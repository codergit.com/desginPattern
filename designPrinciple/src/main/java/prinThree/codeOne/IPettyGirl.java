package prinThree.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-10
 * 描述：接口隔离原则学习
 * 说明：美女接口
 */
public interface IPettyGirl {
	//要有漂亮的脸蛋。
	public void goodLooking();
	//要有好身材
	public void niceFigure();
	//要有气质
	public void greateTemperament();
}
