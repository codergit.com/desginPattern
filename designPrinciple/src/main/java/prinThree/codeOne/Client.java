package prinThree.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-10
 * 描述：场景类
 * 说明：这是演示的第一个版本。因此接口还有可以优化的地方
 */
public class Client {
	public static void main(String[] args) {
		//定义一个美女
		IPettyGirl yanyan = new PettyGirl("艳艳");
		AbstractSearcher searcher = new Searcher(yanyan);
		searcher.show();
	}
}
