package prinThree.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-10
 * 描述：星探类
 * 说明：继承抽象星探类
 */
public class Searcher extends AbstractSearcher{

	public Searcher(IPettyGirl _pettyGirl) {
		super(_pettyGirl);
	}

	@Override
	public void show() {
		System.out.println("------------美女信息如下 ------------");
		super.pettyGirl.goodLooking();
		super.pettyGirl.niceFigure();
		super.pettyGirl.greateTemperament();
	
	}

}
