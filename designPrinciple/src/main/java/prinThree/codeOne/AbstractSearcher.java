package prinThree.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-10
 * 描述：抽象星探类
 * 说明：
 */
public  abstract class AbstractSearcher {
	public IPettyGirl pettyGirl;
	public AbstractSearcher(IPettyGirl _pettyGirl){
		this.pettyGirl = _pettyGirl;
	}
	
	//搜索美女列出美女信息
	public abstract void show();
}
