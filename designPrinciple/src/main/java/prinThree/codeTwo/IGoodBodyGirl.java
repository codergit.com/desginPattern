package prinThree.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-10
 * 描述：两种类型的美女定义
 * 说明：因为第一个版本的接口要三个条件才能算美女，因此需要改一下
 * 比如身材和脸蛋都好的也可以算美女
 * 因此将第一个版本的美女接口拆成两部分
 */
public interface IGoodBodyGirl {
	//要有漂亮的脸蛋
	public void goodLooking();
	//好身材
	public void niceFigure();
}
