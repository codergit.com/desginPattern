package prinThree.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-10
 * 描述：最标准的美女------>面孔，身材，气质
 * 
 * 说明：实现两个接口，通过这样的重构，不管以后要气质美女还是要外形美女都
 * 可以保持接口的稳定。
 * 
 * 这种将一个臃肿的接口变更为两个独立的接口所依赖的原则就是接口隔离原则。
 * 通过分散定义多个接口，可以预防未来变更的扩散，提高系统的灵活性和可维护性
 * 
 */
public class PettyGirl implements IGoodBodyGirl,IGreatTemperamentGirl{
	private String name ;   //美女姓名
	public PettyGirl(String _name){
		this.name = _name;
	}
	public void greateTemperament() {
		System.out.println(this.name+" ----气质很好");
	}

	public void goodLooking() {
		System.out.println(this.name+" ----脸蛋很漂亮");
	}

	public void niceFigure() {
		System.out.println(this.name+" ----身材非常棒");
	}

}
