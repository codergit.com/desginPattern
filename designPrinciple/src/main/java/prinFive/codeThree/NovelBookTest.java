package prinFive.codeThree;

import junit.framework.TestCase;

import com.designPrinciple.prinFive.codeOne.IBook;
import com.designPrinciple.prinFive.codeOne.NovelBook;

/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：小说类的单元测试
 * 说明：Keep the bar green to keep the code clean 
 * Junit运行的两种结果：要么是红条，单元测试失败，
 * 要么是绿条，单元测试通过。
 * 扩展，一个方法的测试方法一般不少于三种，
 * 1.正常的业务逻辑要保证测试到，
 * 2.边界条件要测试到
 * 3.异常测试到。
 * 
 * 单元测试是对类的测试。类中的方法耦合是允许的。
 * 在实际的项目中，一个类一般只有一个测试类，其中可以有很多的测试方法；。
 * 
 * 
 */
public class NovelBookTest extends TestCase{
    private String name = "平凡的世界";
    private int    price = 6000;
    private String author = "路遥";
    private IBook novelBook = new NovelBook(name,price,author);
    //测试getPrice方法
    public void testGetPrice(){
    	//原价销售，根据输入和输出的值是否相等进行断言
    	super.assertEquals(this.price, this.novelBook.getPrice());
    }
    
    
}
