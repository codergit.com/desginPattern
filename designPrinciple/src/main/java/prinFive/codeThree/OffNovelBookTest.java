package prinFive.codeThree;

import junit.framework.TestCase;

import com.designPrinciple.prinFive.codeOne.IBook;
import com.designPrinciple.prinFive.codeTwo.OffNovelBook;

/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：打折销售的小说类单元测试
 * 
 * 说明：
 */
public class OffNovelBookTest extends TestCase{
      private IBook below40NovelBook = new OffNovelBook("平凡的世界", 3000, "路遥");
      private IBook above40NovelBook = new OffNovelBook("平凡的世界", 6000, "路遥");
      //测试低于40源的数据是否打8折
      public void testGetPriceBelow40(){
    	  super.assertEquals(2400, this.below40NovelBook.getPrice());
      }
      //测试大于40元的是否打9折
      public void testGetPriceAbove40(){
    	  super.assertEquals(5400, this.above40NovelBook.getPrice());
      }
      

}
