package prinFive.codeTwo;

import java.text.NumberFormat;
import java.util.ArrayList;

import com.designPrinciple.prinFive.codeOne.IBook;

/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：书店打折销售类
 * 通过重写getPrice方法，扩展完成了新增加的业务，
 * 说明；开闭原则对扩展开放，对修改关闭，并不意味着不做任何修改，底层模块的变更，必然要有高层
 * 模块进行耦合，否则就是一个孤立无意义的代码片段。
 */
public class BookStore {
	private final static ArrayList<IBook> bookList = new ArrayList<IBook>();
	
	static{
		bookList.add(new OffNovelBook("天龙八部", 220, "金庸"));
		bookList.add(new OffNovelBook("巴黎圣母院", 240, "雨果"));
		bookList.add(new OffNovelBook("悲惨世界", 260, "雨果"));
		bookList.add(new OffNovelBook("金瓶梅", 230, "兰陵笑笑生"));
	}
	//模拟书店买书
	public static void main(String[] args) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		formatter.setMaximumFractionDigits(2);
		System.out.println("-----------书店卖出去的书籍记录----------");
		for(IBook book: bookList){
			System.out.println("书籍名称： "+book.getName()+"\t" +
					"书籍作者： "+book.getAuthor()+"\t书籍价格： "+formatter.format(book.getPrice() / 100.0)+"元");
		}
	
	}
}
