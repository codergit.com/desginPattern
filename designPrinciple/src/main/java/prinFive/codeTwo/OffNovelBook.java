package prinFive.codeTwo;

import com.designPrinciple.prinFive.codeOne.NovelBook;

/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：打折销售的小说类
 * 说明：当业务发送变化需要修改价格时
 * 1.修改接口，违反接口作为规范的契约，KO！！！
 * 2.修改实现类，直接在getPrice()中实现打折处理。但是有些信息会缺少，KO !!!
 * 3.通过扩展实现变化，增加子类，重写getPrice()方法。
 * 
 */
public class OffNovelBook extends NovelBook{

	public OffNovelBook(String name, int price, String author) {
		super(name, price, author);
	}
	@Override
	public int getPrice(){
		int selfPrice = super.getPrice();
		int offPrice = 0;
		if(selfPrice > 4000){  //原价大于40元，则打9折
			offPrice = selfPrice * 90 / 100;
		}else{
			offPrice = selfPrice * 80 / 100;
		}
		return offPrice;
	}
}
