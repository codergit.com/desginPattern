package prinFive.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：计算机书籍类
 * 说明：
 */
public class ComputerBook implements IComputerBook{
	private String name;
	private String scope;
	private String author;
	private int price;
	
	
	
	public ComputerBook(String name, String scope, String author, int price) {
		this.name = name;
		this.scope = scope;
		this.author = author;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public int getPrice() {
		return this.price;
	}

	public String getAuthor() {
		return this.author;
	}

	public String geScope() {
		return this.scope;
	}

}
