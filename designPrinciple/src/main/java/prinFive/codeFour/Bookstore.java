package prinFive.codeFour;

import java.text.NumberFormat;
import java.util.ArrayList;

import com.designPrinciple.prinFive.codeOne.IBook;
import com.designPrinciple.prinFive.codeOne.NovelBook;

/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：书店销售计算机书籍
 * 说明：
 */
public class Bookstore {
	private final static ArrayList<IBook> bookList = new ArrayList<IBook>();
	static{
		bookList.add(new NovelBook("天龙八部", 220, "金庸"));
		bookList.add(new NovelBook("巴黎圣母院", 240, "雨果"));
		bookList.add(new NovelBook("悲惨世界", 260, "雨果"));
		bookList.add(new NovelBook("金瓶梅", 230, "兰陵笑笑生"));
		bookList.add(new ComputerBook("汇编语言", "汇编", "大神", 4300));
		
	
	}
	//模拟书店买书
	public static void main(String[] args) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		formatter.setMaximumFractionDigits(2);
		System.out.println("-----------书店卖出去的书籍记录----------");
		for(IBook book: bookList){
			System.out.println("书籍名称： "+book.getName()+"\t" +
					"书籍作者： "+book.getAuthor()+"\t书籍价格： "+formatter.format(book.getPrice() / 100.0)+"元");
		}
	
	}
}
