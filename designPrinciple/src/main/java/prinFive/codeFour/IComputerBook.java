package prinFive.codeFour;

import com.designPrinciple.prinFive.codeOne.IBook;

/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 业务扩展。
 * 描述：计算机书籍接口
 * 说明：获得计算机书籍的范围，同时继承IBook接口。
 */
public interface IComputerBook extends IBook {
	//计算机书籍是有一个范围
	public String geScope();
}
