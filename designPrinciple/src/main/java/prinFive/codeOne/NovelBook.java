package prinFive.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：小说实现类
 * 说明：实现IBook接口
 * 注意数据价格是int型，在非金融类项目中对货币处理时，一般取2位精度。
 * 通常的设计方法是在运行过程中扩大100倍，在需要时再缩小100倍，减少精度带来的损失。
 *
 */
public class NovelBook implements IBook{
	//名称
	private String name;
	private int price;
	private String author;
	
	public NovelBook(String name, int price, String author) {
		this.name = name;
		this.price = price;
		this.author = author;
	}

	public String getName() {
		return this.name;
	}

	public int getPrice() {
		return this.price;
	}

	public String getAuthor() {
		return this.author;
	}

}
