package prinFive.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-22
 * 描述：书籍接口
 * 说明：以书店销售讲解开闭原则。
 */
public interface IBook {
	//名称
	public  String getName();
	//价格
	public  int   getPrice();
	//作者
	public  String getAuthor();
	
}
