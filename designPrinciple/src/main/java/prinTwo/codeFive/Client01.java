package prinTwo.codeFive;

import java.util.HashMap;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：场景类二：
 * 说明：子类在没有覆写父类的方法前提下，子类方法被执行了，这会引起业务逻辑混乱
 * 注意：子类中方法的前置条件必须与超类中被覆写的方法的前置条件相同或者更宽松。
 *
 */
public class Client01 {
	public static void invoker() {
		//父类存在的地方子类就应该能存在
		Son  f = new Son();
		HashMap map = new HashMap();
		f.doSomething(map);
	}

	public static void main(String[] args) {
		invoker();
	}
}
