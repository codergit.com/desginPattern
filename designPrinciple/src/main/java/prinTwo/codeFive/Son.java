package prinTwo.codeFive;

import java.util.Collection;
import java.util.HashMap;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：子类的前置条件较小
 *
 */
public class Son extends Father{
	public Collection doSomething(HashMap map){
		System.out.println("子类被执行。。。。");
		return map.values();
	}
}
