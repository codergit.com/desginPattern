package prinTwo.codeFive;

import java.util.Collection;
import java.util.Map;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：里氏替换原则的第三个含义（反例）
 * 说明：父类的前置条件较大
 *
 */
public class Father {
	public Collection doSomething(Map map){
		System.out.println("父类被执行。。。。");
		return map.values();
	
	}
}
