package prinTwo.codeOne;

import com.designPrinciple.prinTwo.AbstractGun;


/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：士兵实现类
 * 说明：注意：在类中调用其他类时务必要使用父类或者接口，如果不能使用父类或者接口，则说明
 * 类的设计已经违背了LSP的原则。
 *
 */
public class Soldier {
	//定义枪支
	private AbstractGun gun;
	//给士兵一支枪
	public void setGunm(AbstractGun  _gun){
		this.gun = _gun;
	}
	public void killEnemy(){
		System.out.println("士兵开始杀敌------");
		gun.shoot();
	
	}
}
