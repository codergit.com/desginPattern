package prinTwo.codeOne;

import com.designPrinciple.prinTwo.AbstractGun;


/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：机枪实现类
 *
 */
public class MachineGun extends AbstractGun {

	@Override
	public void shoot() {
		System.out.println("机枪扫射 --------");
	}

}
