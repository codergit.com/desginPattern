package prinTwo.codeOne;

import com.designPrinciple.prinTwo.AbstractGun;


/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：手枪实现类
 *
 */
public class HandGun  extends AbstractGun{
	//手枪实现
	public void shoot() {
		System.out.println("手枪射击-------");
	}

}
