package prinTwo.codeOne;

import com.designPrinciple.prinTwo.AbstractGun;


/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：步枪实现类
 *
 */
public class Rifle extends AbstractGun{
	public void shoot() {
		System.out.println("步枪射击---------");
	}

}
