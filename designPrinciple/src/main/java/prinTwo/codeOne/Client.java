package prinTwo.codeOne;


/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：枪支演示场景类
 *
 */
public class Client {
	public static void main(String[] args) {
		//定义一个士兵
		Soldier sold = new Soldier();
		//给士兵一把步枪
		sold.setGunm(new Rifle());
		//杀敌
		sold.killEnemy();
	}
}
