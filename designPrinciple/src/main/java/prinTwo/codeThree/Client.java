package prinTwo.codeThree;

import com.designPrinciple.prinTwo.codeOne.Rifle;

/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：场景类  里氏替换原则的第二个含义：子类可以有自己的个性。
 * 说明：子类继承了父类，并且拥有自己的属性和方法
 * 
 */
public class Client {
	public static void main(String[] args) {
		Snipper snipper = new Snipper();
		//使用子类作为参数
		snipper.killEnemy(new AUG());
		//使用父类作为参数  对象下转型，不安全，从里氏替换原则来看，就是有子类出现的地方父类不一定可以出现
		//snipper.killEnemy( (AUG)(new Rifle()));
	}
}
