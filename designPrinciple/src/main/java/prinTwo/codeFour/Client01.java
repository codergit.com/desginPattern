package prinTwo.codeFour;

import java.util.HashMap;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：场景类二：
 * 说明：输出结果和Client 一样   也就是说： 子类代替父类传递到调用者中，子类的方法永远也不会被执行。
 *
 */
public class Client01 {
	public static void invoker() {
		//父类存在的地方子类就应该能存在
		Son  f = new Son();
		HashMap map = new HashMap();
		f.doSomething(map);
	}

	public static void main(String[] args) {
		invoker();
	}
}
