package prinTwo.codeFour;

import java.util.Collection;
import java.util.Map;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：子类重载父类的方法：方法名相同，输入参数类型不同。
 *
 */
public class Son extends Father{
	public Collection doSomething(Map map){
		System.out.println("子类被执行。。。。");
		return map.values();
	}
}
