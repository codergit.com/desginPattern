package prinTwo.codeFour;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-4 
 * 描述：场景类
 * 说明：在这个场景类中父类的doSomething方法的参数为HashMap，子类的参数为Map，
 * 也就是说子类的方法输入参数被放大了，或者放宽了，
 */
public class Client {
	public static void invoker() {
		//父类存在的地方子类就应该能存在
		Father f = new Father();
		HashMap map = new HashMap();
		f.doSomething(map);
	}

	public static void main(String[] args) {
		invoker();
	}
}
