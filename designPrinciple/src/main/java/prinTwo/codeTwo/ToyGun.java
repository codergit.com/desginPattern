package prinTwo.codeTwo;
import com.designPrinciple.prinTwo.AbstractGun;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：玩具枪实现类
 *
 */
public class ToyGun extends AbstractGun{

	public void shoot() {
		//玩具枪不能射击
	}

}
