package prinTwo.codeTwo;

import com.designPrinciple.prinTwo.codeOne.Soldier;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：里氏替换原则的第一个含义：子类必须完全实现父类的方法。
 *    
 *    
 * 说明：在添加了一个玩具枪后，出现了一个问题
 * 玩具枪不符合射击杀敌的标准，因此应该进行修改。
 * 也就是说子类是否能够完整地实现父类的业务
 * 因此解决方法是：
 * 将ToyGun脱离继承AbstractGun类，然后建立一个独立的父类让ToyGun继承。为了实现代码服用，可以与
 * AbstractGun建立关联委托关系
 *
 */
public class Client {
	public static void main(String[] args) {
		//定义一个士兵
		Soldier sold = new Soldier();
		//给士兵一把步枪
		sold.setGunm(new ToyGun());
		//杀敌
		sold.killEnemy();
	}

}
