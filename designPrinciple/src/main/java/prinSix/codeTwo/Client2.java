package prinSix.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：张三驾驶宝马
 * 说明：在新增加底层模块时，只修改了业务场景类，也就是高层模块。
 * 底层模块不用修改，业务可以正常运行，把“变更”引起的风险扩散降到最小。
 */
public class Client2 {
   public static void main(String[] args) {
	IDriver  zhangsan = new Driver();
	ICar   bmw = new BMW();
	zhangsan.drive(bmw);
	    
    } 
}
