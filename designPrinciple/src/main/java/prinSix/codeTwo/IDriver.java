package prinSix.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：司机接口
 * 说明：
 */
public interface IDriver {
      public void drive(ICar car);
}
