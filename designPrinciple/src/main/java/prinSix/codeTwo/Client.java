package prinSix.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：业务场景
 * 说明：抽象不应该依赖实现细节 
 * client 属于高层业务逻辑，对底层模块的依赖都建立在抽象上。
 */
public class Client {
    public static void main(String[] args) {
		IDriver  zhangSan = new Driver();
		ICar benz = new Benz();
		//张三开奔驰
		zhangSan.drive(benz);
	}
}
