package prinSix.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：司机接口
 * 说明：接口只是一个抽象化的概念，是对一类事物的抽象描述，具体的实现代码由
 * 响应的实现类完成。
 */
public interface ICar {
       public void run();
}
