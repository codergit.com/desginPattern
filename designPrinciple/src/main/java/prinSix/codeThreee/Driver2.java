package prinSix.codeThreee;

import com.designPrinciple.prinSix.codeTwo.ICar;

public class Driver2 implements IDriver2{
    private ICar car;
	public void setCar(ICar car) {
		this.car = car;
	}

	public void driver() {
		this.car.run();
	}

}
