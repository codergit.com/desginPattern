package prinSix.codeThreee;

import com.designPrinciple.prinSix.codeTwo.ICar;

/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：setter方式传递依赖对象
 * 说明：在抽象中设置setter方法声明依赖关系一直闹依赖注入的说法，这是
 * setter依赖注入，
 */
public interface IDriver2 {
    //车辆型号
	public void setCar(ICar car);
     public void driver();

}



