package prinSix.codeThreee;

import com.designPrinciple.prinSix.codeTwo.ICar;

public class Driver implements IDriver{
    private ICar car;
    public Driver(ICar car){
    	this.car = car;
    }
	public void drive() {
		this.car.run();
	}

}
