package prinSix.codeThreee;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：构造函数传递依赖对象
 * 说明：
 */
public interface IDriver {
   public void drive();
}
