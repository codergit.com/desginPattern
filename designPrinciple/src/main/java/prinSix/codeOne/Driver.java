package prinSix.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：论证
 * 说明：
 */
//采用依赖倒置原则可以减少类间的耦合性，提高系统的稳定性，
//降低并行开发引起的风险，提高代码的可读性，和可维护性，。

public class Driver {
     //开车
	public void drive(Benz benz){
		benz.run();
	}
}
