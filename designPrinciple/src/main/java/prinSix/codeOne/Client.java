package prinSix.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-9-3
 * 描述：场景类   ，没有使用依赖倒置原则
 * 说明：当业务变更的时候这种方式就不适用了。，
 */
public class Client {
   public static void main(String[] args) {
	Driver zhangsan = new Driver();
	Benz benz = new Benz();
	zhangsan.drive(benz);
	
	
	
}
} 
