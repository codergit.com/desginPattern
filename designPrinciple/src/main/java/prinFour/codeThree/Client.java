package prinFour.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：模拟人工选择操作。
 * 说明：InstallSoftWare和Wizard的关系耦合性太高了
 * Wizard里的方法暴露太多。codeFour进行重构。
 */
public class Client {
	public static void main(String[] args) {
		InstallSoftWare invoker = new InstallSoftWare();
		invoker.installWizard(new Wizard());
	}
}
