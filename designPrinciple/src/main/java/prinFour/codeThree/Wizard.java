package prinFour.codeThree;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：软件安装的导向类
 * 说明：在该类中分别定义了三个步骤方法每个步骤中都有相关的业务处理逻辑完成指定的任务。
 */
public class Wizard {
	private Random rand = new Random(System.currentTimeMillis());
	//第一步
	public int first(){
		System.out.println("执行第一个方法");
		return rand.nextInt(100);
	}
	
	//第二步
	public int second(){
		System.out.println("执行第二个方法");
		return rand.nextInt(100);
	}
	//第三步
	public int third(){
		System.out.println("执行第三个方法");
		return rand.nextInt(100);
	}
	
	
	
	
}
