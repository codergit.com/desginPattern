package prinFour.codeOne;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：老师类
 * 说明：包含了对GroupLeader和Girl的引用。耦合关系太大
 * Girls 类不属于Teacher的朋友类，但是却与Teacher有交流，违反了迪米特法则
 */
public class Teacher {
	//老师对学生发布命令，清点女学生。
	public void command(GroupLeader groupLeader){
		List<Girls> listGirls = new ArrayList<Girls>();
		GroupLeader gl = new GroupLeader();
		for(int i =0;i<20;i++)
		{
			listGirls.add(new Girls())	;
		}
		//告诉体育委员开始执行清查任务。
		groupLeader.countGirls(listGirls);
	}
	
	
	
}
