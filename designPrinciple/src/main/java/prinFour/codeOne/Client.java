package prinFour.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		Teacher teacher = new Teacher();
		//老师发布命令
		GroupLeader gl = new GroupLeader();
		teacher.command(gl);
		
	}
}
