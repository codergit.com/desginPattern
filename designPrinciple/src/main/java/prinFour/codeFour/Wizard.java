package prinFour.codeFour;

import java.util.Random;


/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：重构后的软件安装的导向类
 * 说明：将三个步骤的访问权限改为private，同时把InstallSoftware的方法installWizard移动到
 * Wizard方法中。只对外公布这个public方法。显示了类的高内聚特性。
 */
public class Wizard {
	private Random rand = new Random(System.currentTimeMillis());
	//第一步
	private  int first(){
		System.out.println("执行第一个方法");
		return rand.nextInt(100);
	}
	
	//第二步
	private int second(){
		System.out.println("执行第二个方法");
		return rand.nextInt(100);
	}
	//第三步
	private int third(){
		System.out.println("执行第三个方法");
		return rand.nextInt(100);
	}
	//软件安装过程
	public void installWizard(){
		int first = this.first();
		if(first > 50){
			int second = this.second();
			System.out.println("软件安装第一步完成。。。。");
			if(second  > 50){
				int third = this.third();
				System.out.println("软件安装第二步完成。。。。");

				if(third > 50){
					System.out.println("软件安装第三步完成。。。。");
				}
			}
		}
	}
	
	
}
