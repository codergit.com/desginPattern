package prinFour.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：模拟人工选择操作。
 * 说明：InstallSoftWare和Wizard的关系耦合性太高了
 * Wizard里的方法暴露太多。codeFour进行重构。
 * 
 * 一个类的公开的public属性或者方法越多，修改时涉及的面也越大，变更引起的风险扩散也也越大
 * 因此，为了保持朋友类之间的距离，在设计时，需要反复衡量：是否还可以再减少public方法和属性
 * ,是否可以修改为private package-private(友好的)，protected等访问权限，是否可以加上final关键字等，
 * 
 */
public class Client {
	public static void main(String[] args) {
		InstallSoftWare invoker = new InstallSoftWare();
		invoker.installWizard(new Wizard());
	}
}
