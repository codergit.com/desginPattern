package prinFour.codeTwo;

import java.util.ArrayList;
import java.util.List;

import com.designPrinciple.prinFour.codeOne.Girls;
/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：体育委员类实现过程
 * 说明：
 */
public class GroupLeader {
	private List<Girls> listGirls ;
	
	public GroupLeader(List<Girls> _listGirls){
		this.listGirls = _listGirls;
	}
	
	public void countGirls(){
		
		System.out.println("女生人数为： "+listGirls.size());
	}
}
