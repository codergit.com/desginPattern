package prinFour.codeTwo;

import java.util.ArrayList;
import java.util.List;

import com.designPrinciple.prinFour.codeOne.Girls;
import com.designPrinciple.prinFour.codeOne.GroupLeader;
/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：改进后的Teacher类
 * 说明：
 */
public class Teacher {
	//老师对学生发布命令，清点女学生。
	public void command(GroupLeader groupLeader){
		List<Girls> listGirls = new ArrayList<Girls>();
		for(int i =0;i<20;i++)
		{
		listGirls.add(new Girls())	;
		}
		//告诉体育委员开始执行清查任务。
		groupLeader.countGirls(listGirls);
	}
	
}
