package prinFour.codeTwo;

import java.util.ArrayList;
import java.util.List;

import com.designPrinciple.prinFour.codeOne.Girls;

/**
 * 
 * @author fcs
 * @date 2014-8-17
 * 描述：场景类
 * 说明：把Teacher中对List<Girl>的初始化移动到了场景类中，同时
 * 在GroupLeader中增加了Grill的注入，避开了、Teacher类对陌生类
 * Girls的访问，降低了系统间的耦合，提高了系统的健壮性。
 *
 * 注意：一个类只和朋友交流，不与陌生类交流，不要出现getA().getB()....
 * 这样的方法链，(在极端情况下可以使用，即每个点号后面的返回类型都相同)，类与类之间的关系是建立在类
 * 之间的，而不是方法间的，因此一个方法尽量不要引入一个类中不存在的对象，
 * ，JDK API提供的类除外。
 *
 */
public class Client {
	public static void main(String[] args) {
		Teacher teacher = new Teacher();
		//老师发布命令
		List<Girls> listGirls = new ArrayList<Girls>();
		GroupLeader gl = new GroupLeader(listGirls);
		for(int i =0;i<20;i++)
		{
			listGirls.add(new Girls())	;
		}
		gl.countGirls();
		
	}
}
