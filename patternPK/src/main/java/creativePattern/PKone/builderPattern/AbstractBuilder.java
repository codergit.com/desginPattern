package creativePattern.PKone.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：抽象建造者
 * 说明：使用模板方法模式创建建造者
 */
public abstract class AbstractBuilder {
   public final SuperMan superMan = new SuperMan();
   public void setBody(String body){
	   this.superMan.setBody(body);
   }
   public void setSpecialTalent(String st){
	   this.superMan.setSpecialTalent(st);
   }
   public void setSpecialSymbol(String ss){
	   this.superMan.setSpecialSymbol(ss);
   }
   //构建出完整的超人
   public abstract SuperMan getSuperMan();
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}
