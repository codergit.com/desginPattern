package creativePattern.PKone.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：成年超人建造者
 * 说明：
 */
public class AdultSuperManBuilder  extends AbstractBuilder{

	@Override
	public SuperMan getSuperMan() {
         super.setBody("强壮的身体");
         super.setSpecialSymbol("胸前带S标志");
         super.setSpecialTalent("会飞行");
         return super.superMan;
	}
	
	
	
	

}
