package creativePattern.PKone.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：导演类
 * 说明：相当于工厂类
 */
public class Director {
   //两个建造者的应用
	private static AbstractBuilder adultBuilder = new AdultSuperManBuilder();
	public static AbstractBuilder  childBuilder = new ChildSupermanBuilder();
	public static SuperMan getAdultSuperMan(){
		return adultBuilder.getSuperMan();
	}
	public static SuperMan getChildSuperMan(){
		return childBuilder.getSuperMan();
	}
	
	
	
	
	
	
	
	
}
