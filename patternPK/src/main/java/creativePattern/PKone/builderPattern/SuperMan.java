package creativePattern.PKone.builderPattern;

/**
 * 
 * @author fcs
 * @date 2014-9-4 
 * 描述：按建造者模式构造的超人
 *  说明：
 */
public class SuperMan {
	private String body;     //躯体
	private String specialTalent; //技能
	private String specialSymbol;//标志

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSpecialTalent() {
		return specialTalent;
	}

	public void setSpecialTalent(String specialTalent) {
		this.specialTalent = specialTalent;
	}

	public String getSpecialSymbol() {
		return specialSymbol;
	}

	public void setSpecialSymbol(String specialSymbol) {
		this.specialSymbol = specialSymbol;
	}

}
