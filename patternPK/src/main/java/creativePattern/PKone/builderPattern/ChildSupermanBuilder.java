package creativePattern.PKone.builderPattern;
//未成年超人
/**
 * 建造者通过不同的部件，不同的装配产生不同的复杂对象。
 */
public class ChildSupermanBuilder  extends AbstractBuilder{

	@Override
	public SuperMan getSuperMan() {
		super.setBody("强壮的身体");
		super.setSpecialSymbol("胸前带小S标志");
	    super.setSpecialTalent("刀枪不入");
	    return super.superMan;
	}

}
