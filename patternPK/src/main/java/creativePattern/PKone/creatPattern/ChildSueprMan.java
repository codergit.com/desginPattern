package creativePattern.PKone.creatPattern;


/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：未成年超人
 * 说明：
 */
public class ChildSueprMan  implements ISuperman{

	public void specialTalent() {
			System.out.println("小超人的能力是刀枪不入，快速运动");
	}
   
}
