/**
 * 作者：fcs
 * 时间：2014-9-4
 * 说明：
 */
package creativePattern.PKone.creatPattern;


/**
 * @author fcs
 * @date 2014-9-4
 * 描述：成年超人
 * 说明：
 */
public class AdultSuperman implements ISuperman {
    
	public void specialTalent() {
		System.out.println("超人力大无穷");
	}

}
