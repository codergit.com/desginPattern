package creativePattern.PKone.creatPattern;


/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：超人制造工厂
 * 说明：
 */
public class SuperManFactory {
	//根据参数返回不同类型的超人。
     public static ISuperman  createSueprMan(String type){
    	 if(type.equalsIgnoreCase("adult")){
    		 return new AdultSuperman();
    	 }else if(type.equalsIgnoreCase("child")){
    		 return new ChildSueprMan();
    	 }else{
    		 return null;
    	 }
     }
}
