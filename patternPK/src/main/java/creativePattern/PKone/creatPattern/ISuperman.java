package creativePattern.PKone.creatPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：使用工厂方法模式创建的超人接口
 * 说明：
 */
public interface ISuperman {
    public void specialTalent();
}
