package creativePattern.PKone.creatPattern;


/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：场景类
 * 说明：通过工厂方法模式生产出的对象，然后由客户端进行对象的其他操作，
 * 但是并不代表所有生产出的对象都必须具有相同的状态和行为，是由产品决定的。
 * 
 */
public class Client {
    //模拟生产超人
	public static void main(String[] args) {
		ISuperman adultMan = SuperManFactory.createSueprMan("adult");
	    adultMan.specialTalent();	
	
	}
}
