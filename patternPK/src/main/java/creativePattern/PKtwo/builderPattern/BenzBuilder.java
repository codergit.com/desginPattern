package creativePattern.PKtwo.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：奔驰建造车间
 * 说明：
 */
public class BenzBuilder  extends CarBuilder {
	@Override
	protected String buildWheel() {
		return super.getBluePrint().getEngine();
	}
	@Override
	protected String buildEngine() {
		return super.getBluePrint().getWheel();
	}

}
