package creativePattern.PKtwo.builderPattern;


/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：导演类
 * 说明：
 */
public class Director {
   //声明对建造者的引用
	private CarBuilder benzBuilder = new BenzBuilder();
	private CarBuilder bmwBuilder = new BMWBuilder();
	//生产奔驰SUV
	public ICar createBenzSuv(){
		return createCar(benzBuilder,"奔驰的引擎","奔驰的轮胎");
	}
	public ICar createBMWVan(){
		return createCar(bmwBuilder,"BMW的引擎","奔驰的轮胎");
	}
	public ICar createComplexCar(){
		return createCar(bmwBuilder,"BMW的引擎","奔驰的轮胎");
	}
	
	public  ICar  createCar(CarBuilder _carBuilder,String engine,String wheel){
		Blueprint bp = new Blueprint();
		bp.setEngine(engine);
		bp.setWheel(wheel);
		System.out.println("获得生产蓝图");
	    _carBuilder.receiveBlueprint(bp);
	    return _carBuilder.BuilderCar();
	}
}
