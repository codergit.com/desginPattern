package creativePattern.PKtwo.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：按建造者模式生产汽车
 * 说明：对汽车各个部分进行组装
 */
public interface ICar {
    public String  getWheel();
    public String  getEngine();
    
}
