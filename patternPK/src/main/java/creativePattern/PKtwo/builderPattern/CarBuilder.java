package creativePattern.PKtwo.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：抽象建造者
 * 说明：
 */
public abstract  class CarBuilder {
    //待建造的汽车
	private ICar car;
    //设计蓝图
	private Blueprint bp;
	
	public void receiveBlueprint(Blueprint _bp){
		this.bp = _bp;
	}
	public Blueprint getBluePrint(){
		return bp;
	}
	public Car BuilderCar(){
		return new Car(buildWheel(),buildEngine());
	}
	protected abstract String buildWheel();
	protected abstract String buildEngine();
	
	
	
	
	
	
	
}
