package creativePattern.PKtwo.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：场景类
 * 说明：
 */
public class Client {
    public static void main(String[] args) {
	   //定义导演类
    	Director director = new Director();
	   System.out.println("生产奔驰。。。");
	   ICar benzSuv = director.createBenzSuv();
	   System.out.println(benzSuv);
	   ICar bmw = director.createBMWVan();
	   System.out.println(bmw);
    
    
    }
}
