package creativePattern.PKtwo.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：具体车辆
 * 说明：
 */
public class Car  implements ICar{
    private String engine;
    private String wheel;
    
	public Car(String engine, String wheel) {
		this.engine = engine;
		this.wheel = wheel;
	}
	public String getWheel() {
		return wheel;
	}
	public String getEngine() {
		return engine;
	}
}
