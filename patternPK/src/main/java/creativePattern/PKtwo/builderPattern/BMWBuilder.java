package creativePattern.PKtwo.builderPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-5
 * 描述：宝马建造车间
 * 说明：
 */
public class BMWBuilder extends CarBuilder {

	@Override
	protected String buildWheel() {
		return super.getBluePrint().getEngine();
	}
	@Override
	protected String buildEngine() {
		return super.getBluePrint().getWheel();
	}

}
