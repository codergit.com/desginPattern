package creativePattern.PKtwo.abstractFactoryPatten;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：奔驰车工厂
 * 说明：
 */
public class BenzFactory  implements CarFactory{

	public ICar createSuv() {
		return new BenzSuv();
	}

	public ICar createVan() {
		return new BenzVan();
	}

}
