package creativePattern.PKtwo.abstractFactoryPatten;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：宝马SUv
 * 说明：
 */
public class BMWSuv extends AbsBMW{
    private final static String X_SEARIES = "X系列车型SUV";
    public String getModel(){
    	return X_SEARIES;
    }
  

}
