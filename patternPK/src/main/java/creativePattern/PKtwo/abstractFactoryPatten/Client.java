package creativePattern.PKtwo.abstractFactoryPatten;

import com.designPrinciple.prinSix.codeOne.Benz;

/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：场景类
 * 说明：
 */
public class Client {
  public static void main(String[] args) {
	System.out.println("====生产奔驰。。。。");
    System.out.println("A.找到奔驰车工厂 ");
    CarFactory carFactory = new  BenzFactory();
    System.out.println("B.开始生产奔驰SUV");
    ICar benzSuv = carFactory.createSuv();
    System.out.println("生产汽车如下： ");
    System.out.println("汽车品牌： "+benzSuv.getBand());
    System.out.println("汽车型号："+benzSuv.getModel());
  
  
  
  
  
  
  }
}
