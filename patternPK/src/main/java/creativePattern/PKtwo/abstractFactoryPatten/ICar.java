package creativePattern.PKtwo.abstractFactoryPatten;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：使用抽象方法模式建造汽车
 * 说明：汽车接口
 */
public interface ICar {
     public String getBand();   //品牌
     public String getModel();  //型号
     
}
