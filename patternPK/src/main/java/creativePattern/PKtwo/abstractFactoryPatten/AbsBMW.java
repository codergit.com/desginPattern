package creativePattern.PKtwo.abstractFactoryPatten;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：抽象宝马汽车
 * 说明：
 */
public  abstract class  AbsBMW implements ICar {
    private final String bmw_band = "宝马汽车";
	public String getBand() {
		return bmw_band;
	}
	//由具体汽车实现
    public abstract String getModel();
	
	
	

}
