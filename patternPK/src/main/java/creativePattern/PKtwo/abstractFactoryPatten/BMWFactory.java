package creativePattern.PKtwo.abstractFactoryPatten;
/**
 * 
 * @author fcs
 * @date 2014-9-4
 * 描述：宝马汽车工厂
 * 说明：
 */
public class BMWFactory implements CarFactory {

	public ICar createSuv() {
		return new BMWSuv();
	}

	public ICar createVan() {
		return new BMVan();
	}

}
