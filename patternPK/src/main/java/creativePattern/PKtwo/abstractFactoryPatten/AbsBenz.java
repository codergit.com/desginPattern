package creativePattern.PKtwo.abstractFactoryPatten;

public abstract class AbsBenz implements ICar {
    private final static String BENZ_BAND = "��������";
	public String getBand() {
		return BENZ_BAND;
	}

	public abstract String getModel() ;

}
