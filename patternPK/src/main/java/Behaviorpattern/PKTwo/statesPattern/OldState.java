package Behaviorpattern.PKTwo.statesPattern;

public class OldState extends HumanState{

	@Override
	public void work() {
		System.out.println("老人的工作就是享受天伦之乐");
	}

}
