package Behaviorpattern.PKTwo.statesPattern;
/**\
 *
 *
 * @author fcs
 * @date 2014-9-9
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {
	  Human human = new Human();
	  human.setState(new ChildState());
	  System.out.println("儿童的主要工作。。。。");
      human.work();
      human.setState(new AdultState());
      System.out.println("\n====成年人的工作。。。。");
      human.work();
      System.out.println("老年人的工作");
      human.setState(new OldState());
      human.work();
   
   }
}
