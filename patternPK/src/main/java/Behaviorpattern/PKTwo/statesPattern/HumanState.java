package Behaviorpattern.PKTwo.statesPattern;

public abstract class HumanState {
  protected Human human;
  public void setHuman(Human _human){
	  this.human = _human;
  }
  public abstract void work();
}
