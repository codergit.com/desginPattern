package Behaviorpattern.PKTwo.statesPattern;

/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：环境角色
 * 说明：
 */
public class Human {
     public static final HumanState CHILD_STATE = new ChildState();
     public static final HumanState ADULT_STATE = new AdultState();
     public static final HumanState OLD_STATE = new OldState();
     
     private HumanState state;
     public void setState(HumanState _state){
    	this.state = _state;
    	
     }
     
     public void work(){
    	 this.state.work();
     }
     
}


