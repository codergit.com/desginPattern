package Behaviorpattern.PKTwo.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {
	Context context  = new Context();
    System.out.println("儿童的工作----");
    context.setWorkMethod( new ChildWork());
    context.work();
    System.out.println("\n成年人的工作----");
    context.setWorkMethod(new AdultWork());
    System.out.println("\n老年人的工作----");
    context.setWorkMethod(new OldWork());
    context.work();
   }
}
