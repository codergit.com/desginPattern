package Behaviorpattern.PKTwo.strategyPattern;

/**
 * 
 * @author fcs
 * @date 2014-9-9 描述：环境角色 说明：
 */
public class Context {
	private WorkAlgorithm workMethod;

	public WorkAlgorithm getWorkMethod() {
		return workMethod;
	}

	public void setWorkMethod(WorkAlgorithm workMethod) {
		this.workMethod = workMethod;
	}	
	public void work(){
		workMethod.work();
	}
}
