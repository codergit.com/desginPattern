package Behaviorpattern.PKTwo.strategyPattern;
/*
 * 成人工作
 */
public class AdultWork extends WorkAlgorithm{

	@Override
	public void work() {
     System.out.println("成年人的工作是工作，赚钱，为社会做贡献。");		
	}

}
