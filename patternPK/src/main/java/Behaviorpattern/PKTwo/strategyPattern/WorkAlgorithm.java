package Behaviorpattern.PKTwo.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：抽象工作算法
 * 说明：
 */
public abstract class WorkAlgorithm {
    public abstract void work();
    
}
