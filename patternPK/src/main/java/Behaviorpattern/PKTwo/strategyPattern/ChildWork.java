package Behaviorpattern.PKTwo.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：孩童工作
 * 说明：
 */
public class ChildWork extends WorkAlgorithm {

	@Override
	public void work() {
		System.out.println("儿童的工作就是玩耍，学习");
	}
}
