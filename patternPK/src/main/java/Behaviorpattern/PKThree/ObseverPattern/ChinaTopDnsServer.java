package Behaviorpattern.PKThree.ObseverPattern;

import com.patternPK.Behaviorpattern.PKThree.ChainResponsePattern.Recorder;

/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：中国顶级DNS服务器
 * 说明：
 */
public class ChinaTopDnsServer extends DnsServer{

	@Override
	protected boolean isLocal(Recorder recorder) {
		return recorder.getDomain().endsWith(".cn");
	}

	@Override
	protected void sign(Recorder recorder) {
		recorder.setOwner("中国顶级DNS服务器");
	}

       
}
