package Behaviorpattern.PKThree.ObseverPattern;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import com.patternPK.Behaviorpattern.PKThree.ChainResponsePattern.Recorder;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：抽象DNS服务器
 * 说明：responseFromUpperServer方法，只允许设置一个观察者。
 */
public  abstract class DnsServer extends Observable implements Observer{

	public void update(Observable o, Object arg) {
		Recorder recorder = (Recorder)arg;
	    if(isLocal(recorder)){
	    	recorder.setIp(genIpAddress());
	    }else{
	    	resonsFromUpperServer(recorder);
	    }
	}
	public void setUpperServer(DnsServer _upperServer){
		//先清空，再增加
		super.deleteObservers();
		super.addObserver(_upperServer);
	}
	//每个DNS都有一个数据处理区，检查域名是否在本区中
	protected abstract boolean isLocal(Recorder  recorder);
	
	//每个服务器都有自己的名字
	protected abstract void sign(Recorder recorder);
	
	//向父DNs请求解析，也就是通知观察者
	private void resonsFromUpperServer(Recorder recorder){
		super.setChanged();
		super.notifyObservers(recorder);
	}
	
	//每个DNS服务器都必须实现解析任务
	protected Recorder echo(String domain){
		Recorder recorder = new Recorder();
		//获取IP地址
		recorder.setDomain(domain);
		recorder.setIp(genIpAddress());
		return recorder;
	}
	//随机产生一个IP地址，工具类
	private String genIpAddress(){
		Random rand = new Random();
		String address = rand.nextInt(255) + "."+rand.nextInt(255)+"."+
		rand.nextInt(255)+"."+rand.nextInt(255);
		return address;
	}
}
