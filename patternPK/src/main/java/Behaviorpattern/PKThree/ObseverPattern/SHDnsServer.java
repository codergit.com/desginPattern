package Behaviorpattern.PKThree.ObseverPattern;

import com.patternPK.Behaviorpattern.PKThree.ChainResponsePattern.Recorder;

/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：上海DNS服务器
 * 说明：
 */
public class SHDnsServer  extends DnsServer {

	@Override
	protected boolean isLocal(Recorder recorder) {
		return recorder.getDomain().endsWith(".sh.cn");
	}

	@Override
	protected void sign(Recorder recorder) {
		recorder.setOwner("上海DNS服务器");
	}

}
