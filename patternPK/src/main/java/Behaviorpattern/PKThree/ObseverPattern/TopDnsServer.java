package Behaviorpattern.PKThree.ObseverPattern;

import com.patternPK.Behaviorpattern.PKThree.ChainResponsePattern.Recorder;

/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：全球顶级DNS服务器
 * 说明：
 */
public class TopDnsServer  extends DnsServer {

	@Override
	protected boolean isLocal(Recorder recorder) {
	    //所有域名最终的解析地点。
		return true;
	}

	@Override
	protected void sign(Recorder recorder) {
		recorder.setOwner("全球顶级DNS服务器");
	}
	
	
	
	
	
	
	
	
	

}
