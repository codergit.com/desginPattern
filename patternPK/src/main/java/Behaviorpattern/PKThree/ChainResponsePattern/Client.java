package Behaviorpattern.PKThree.ChainResponsePattern;

import java.util.Scanner;

/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：场景类
 * 说明：
 */
public class Client {
    public static void main(String[] args) {
	DnsServer sh = new SHDnsServer();
	DnsServer china = new ChinaTopDnsServer();
	DnsServer top = new TopDnsServer();
	china.setUpperServer(top);
	sh.setUpperServer(china);
    System.out.println("====域名解析模拟器=====");
    while(true){
    	System.out.println("\n请输入域名（输入N退出）");
      Scanner scan = new Scanner(System.in);
    	String domain = scan.next();
    	if(domain.equalsIgnoreCase("n")){
    		return ;
    	}
        Recorder recorder = sh.resolve(domain);
        System.out.println("----DNS服务器解析结果");
        System.out.println(recorder);
    }
    	
    	
    	
    	
    	
    	
    	
	}
}
