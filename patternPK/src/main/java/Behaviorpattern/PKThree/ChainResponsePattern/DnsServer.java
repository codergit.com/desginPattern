package Behaviorpattern.PKThree.ChainResponsePattern;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：抽象域名服务器
 * 说明：
 */
public  abstract class DnsServer {
    //上级DNS是谁
	private DnsServer upperServer;
    //解析域名
	public final Recorder resolve(String domain){
		Recorder recorder= null;
		if(isLocal(domain)){
			recorder = echo( domain);
			
		}else{//本服务器不能解析，提交上级
			recorder = upperServer.resolve(domain);
			
		}
		
		return recorder;
	}
	public void setUpperServer(DnsServer _upperServer){
		this.upperServer = _upperServer;
	}
	//每个DNS都有一个数据处理区，检查域名是否在本区中
	protected abstract boolean isLocal(String domain);
	
	//每个DNS服务器都必须实现解析任务
	protected Recorder echo(String domain){
		Recorder recorder = new Recorder();
		//获取IP地址
		recorder.setDomain(domain);
		recorder.setIp(genIpAddress());
		return recorder;
	}
	//随机产生一个IP地址，工具类
	private String genIpAddress(){
		Random rand = new Random();
		String address = rand.nextInt(255) + "."+rand.nextInt(255)+"."+
		rand.nextInt(255)+"."+rand.nextInt(255);
		return address;
	}
	
	
	
	
	
	
	
}
