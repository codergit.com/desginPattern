package Behaviorpattern.PKThree.ChainResponsePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：上海服务器解析
 * 说明：重写echo方法，各个具体的DNS服务器实现自己的解析过程，
 * 属于个性化处理，代表的是每个DNS服务器的不同处理逻辑
 */
public class SHDnsServer  extends DnsServer{
	@Override
	protected Recorder echo(String domain){
		Recorder recorder = super.echo(domain);
		recorder.setOwner("上海DNS服务器");
		return recorder;
	}
	//定义上海服务器能处理的级别
	@Override
	protected boolean isLocal(String domain) {
		return domain.endsWith(".sh.cn");
	}
	

}
