package Behaviorpattern.PKone.strategyPattern;





/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：环境角色
 * 说明：使用上下文对策略进行封装
 */
public class Context {
   private Algorithm al;
   public Context (Algorithm _al){
	   this.al = _al;
   }
   public boolean compress(String source,String to){
	   return al.compress(source, to);
   }
   public boolean uncompress(String source,String to){
	   return al.uncompress(source, to);
	   
   }
}
