package Behaviorpattern.PKone.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：策略模式实现压缩和解压缩算法
 * 说明：每个算法实现两个功能，压缩和解压缩，传递Source是绝对路径，
 * compress则是压缩到to目录下。uncompress则是反向操作
 */
public interface Algorithm {
    //压缩算法
 	public boolean compress(String source, String to);
    
 	//解压缩算法
 	public boolean uncompress(String source, String to);

}
