package Behaviorpattern.PKone.strategyPattern;





public class GZip implements Algorithm{

	//zip格式的压缩算法
	public boolean compress(String source, String to) {
		System.out.println(source + " --->"+ to+"Zip压缩成功" );
	    return true;
	}

	public boolean uncompress(String source, String to) {
		System.out.println(source + " --->"+ to+"Zip解压缩成功" );		
		return false;
	}

}
