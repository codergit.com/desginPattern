package Behaviorpattern.PKone.strategyPattern;





/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：遵循依赖倒置原则
 * 说明：实现算法接口
 */
public class Zip  implements Algorithm{

	//zip格式的压缩算法
	public boolean compress(String source, String to) {
		System.out.println(source + " --->"+ to+"Zip压缩成功" );
	    return true;
	}

	public boolean uncompress(String source, String to) {
		System.out.println(source + " --->"+ to+"Zip解压缩成功" );		
		return false;
	}

}
