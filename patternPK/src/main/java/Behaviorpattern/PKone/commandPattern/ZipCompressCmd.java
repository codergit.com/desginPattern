package Behaviorpattern.PKone.commandPattern;

public class ZipCompressCmd extends AbstractCmd{
    public boolean execute(String source,String to ){
    	return super.zip.compress(source, to);
    }
}
