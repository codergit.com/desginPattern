package Behaviorpattern.PKone.commandPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：zip接收者
 * 说明：
 */
public class ZipReceiver  implements IReceiver{
   //zip格式的压缩算法
	public boolean compress(String source, String to) {
		System.out.println(source+"----->"+"zip压缩成功");
		return true;
	}
   //zip格式的解压缩算法
	public boolean uncompress(String source, String to) {
		
		System.out.println(source+"----->" + to+"zip解压缩成功。。。");
		return true;
	}

}
