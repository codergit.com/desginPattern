package Behaviorpattern.PKone.commandPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：抽象接收者
 * 说明：使用委托的方式实现执行命令
 */
public interface IReceiver {
	 //压缩
	public boolean compress(String source, String to);
    //解压缩
	public boolean uncompress(String source, String to);

}
