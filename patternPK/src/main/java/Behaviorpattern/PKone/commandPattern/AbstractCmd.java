package Behaviorpattern.PKone.commandPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：抽象压缩命令
 * 说明：
 */
public abstract class AbstractCmd {
   //对接受者的引用
	protected IReceiver  zip = new ZipReceiver();
	protected IReceiver gzip = new GzipReceiver();
	public abstract boolean execute(String source,String to);
}
