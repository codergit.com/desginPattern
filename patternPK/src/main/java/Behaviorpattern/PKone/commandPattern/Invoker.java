package Behaviorpattern.PKone.commandPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：调用者
 * 说明：
 */
public class Invoker {
	private AbstractCmd cmd;
	public Invoker(AbstractCmd _cmd){
		this.cmd = _cmd;
	}
	//执行命令
	public boolean execute(String source,String to){
		return cmd.execute(source, to);
		
	}
	
	
	
	
	
	
	
	
	
	
	

}
