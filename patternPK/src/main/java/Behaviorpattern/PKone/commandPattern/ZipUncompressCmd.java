package Behaviorpattern.PKone.commandPattern;

public class ZipUncompressCmd extends AbstractCmd{

	@Override
	public boolean execute(String source, String to) {
		return super.zip.compress(source, to);
	}

}
