package Behaviorpattern.PKone.commandPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-9
 * 描述：gzip接收者
 * 说明：
 */
public class GzipReceiver  implements IReceiver{

	public boolean compress(String source, String to) {
		System.out.println(source+"----->"+"gzip压缩成功");

		return true;
	}

	public boolean uncompress(String source, String to) {
		System.out.println(source+"----->"+"gzip压缩成功");

		return true;
	}
  
}
