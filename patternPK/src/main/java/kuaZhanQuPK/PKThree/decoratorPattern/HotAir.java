package kuaZhanQuPK.PKThree.decoratorPattern;

public class HotAir extends Decorator{

	public HotAir(IStar _star) {
		super(_star);
	}
	
	public void act(){
		System.out.println("演戏前： 夸夸其谈，没有自己不能演的角色");
	    super.act();
	}

}
