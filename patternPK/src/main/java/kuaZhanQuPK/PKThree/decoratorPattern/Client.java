package kuaZhanQuPK.PKThree.decoratorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {
	   IStar freakStar = new FreakStar();
	   freakStar = new HotAir(freakStar);
	   freakStar = new Deny(freakStar);
	   System.out.println("----看看一些虚假明星的形象");
	   freakStar.act();
	   
   }
}
