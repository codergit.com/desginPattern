package kuaZhanQuPK.PKThree.brigePattern;

public class FilmStar extends AbsStar {

	public FilmStar(AbsAction _action) {
		super(_action);
	}
    //默认的电影明星的主要工作是拍电影
	public FilmStar(){
		super(new ActFilm());
	}
	
	public void doJob(){
		System.out.println("\n----影视明星的工作。。。。");
	    super.doJob();
	}
}
