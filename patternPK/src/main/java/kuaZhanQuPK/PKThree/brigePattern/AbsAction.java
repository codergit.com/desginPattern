package kuaZhanQuPK.PKThree.brigePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：抽象活动
 * 说明：
 */
public abstract  class AbsAction {
    public abstract void desc();
}
