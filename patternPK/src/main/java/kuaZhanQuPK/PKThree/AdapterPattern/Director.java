package kuaZhanQuPK.PKThree.AdapterPattern;

import org.omg.IOP.CodecFactoryPackage.UnknownEncoding;

/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：导演类
 * 说明：
 */
public class Director {
     public static void main(String[] args) {
		System.out.println("=====演戏过程模拟======");
	    IStar star = new FilmStar();
	    star.act("前十五分钟，明星本人演戏");
	    IActor actor = new UnknowActor();
	    IStar standin = new Standin(actor);
	    standin.act("中间5分钟替身在演戏");
        star.act("后十五分钟，明星本人演戏");
     }
}
