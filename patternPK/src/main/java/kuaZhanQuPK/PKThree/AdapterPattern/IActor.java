package kuaZhanQuPK.PKThree.AdapterPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：普通演员接口
 * 说明：
 */
public interface IActor {
    public void playact(String context);
}
