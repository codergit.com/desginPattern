package kuaZhanQuPK.PKThree.AdapterPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：明星接口
 * 说明：
 */
public interface IStar {
   public void act(String context);
}
