package kuaZhanQuPK.PKThree.proxyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：追星族
 * 说明：
 */
public class Idolater {
    public static void main(String[] args) {
		IStar star = new Singer();
	   	IStar agent = new Agent(star);
	   	System.out.println("追星族场景类。。。。");
	    agent.sign();
    }
}
