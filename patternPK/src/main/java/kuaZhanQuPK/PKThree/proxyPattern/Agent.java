package kuaZhanQuPK.PKThree.proxyPattern;

public class Agent implements IStar {
	private IStar star;
    //定义是谁的经纪人
	public Agent(IStar _star){
		this.star = _star;
	}
	public void sign() {
		this.star.sign();
	}

}
