package kuaZhanQuPK.PKTwo.facadePattern;

import java.util.Date;

/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：场景类
 * 说明：使用门面模式对薪水计算子系统进行封装，避免子系统的内部复杂逻辑外泄，
 * 确保子系统的业务逻辑的单纯性，即使业务流程需要变更，影响的也是子系统内部功能，
 * 比如奖金需要与基本工资挂钩，这样的修改对外系统来说是透明的，只需要子系统内部变更即可。
 */
public class Client {
     public static void main(String[] args) {
		HRFacade facade = new HRFacade();
		System.out.println("外系统查询总收入。。。。");
		int  salary = facade.querySalary("张三", new Date(System.currentTimeMillis()));
		System.out.println("张三收入为："+salary);
		System.out.println("外系统查询工资天数");
	    int workDays = facade.queryWorkDays("李斯");
	    System.out.println("李斯本月出勤： "+workDays);
		
	}


}
