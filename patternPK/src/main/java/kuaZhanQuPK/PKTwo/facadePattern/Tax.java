package kuaZhanQuPK.PKTwo.facadePattern;

import java.util.Random;

/**\
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：税收
 * 说明：
 */
public class Tax {
    public int getTax(){
    	//缴纳一个随机量的税金
    	return (new Random()).nextInt(100);
    	
    }
}
