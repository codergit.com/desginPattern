package kuaZhanQuPK.PKTwo.facadePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：奖金计算
 * 说明：
 */
public class Bonus {
	//考勤情况
	private Attendance att = new  Attendance();
	
    //奖金
	public int getBonus(){
		//获得出勤情况
		int workDays = (int)att.getWorkDays();
		int bonus = workDays * 100 / 30;
		return bonus;
	}
}
