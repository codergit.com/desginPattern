package kuaZhanQuPK.PKTwo.facadePattern;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：绩效
 * 说明：
 */
public class Performance {
     //基本工资
	private BasicSalary salary = new BasicSalary();
	//绩效奖励
	public int getPerformance(){
		//随机绩效
		int perf = (new Random()).nextInt(100);
	    return salary.getBasicSalary() * perf / 100;
	}
}
