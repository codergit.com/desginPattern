package kuaZhanQuPK.PKTwo.facadePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：HR门面
 * 说明：
 */
public class HRFacade {
     //工资情况
	private SalaryProvider  salaryProvider = new  SalaryProvider();
	//考勤情况
	private Attendance attendance = new Attendance();
	
	//查询总收入
	public int querySalary(String name,java.util.Date date){
		return salaryProvider.totalSalary();
	}
	//查询工作天数
    public  int queryWorkDays(String name){
    	return attendance.getWorkDays();
    }

}
