package kuaZhanQuPK.PKTwo.facadePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：总工资计算
 * 说明：对其他相关类进行组合
 */
public class SalaryProvider {
   //基本工资
	private BasicSalary basicSalary = new BasicSalary();
	private Bonus bonus = new Bonus();
	private Performance  perf = new Performance();
	private Tax   tax = new Tax();
	public int totalSalary(){
		return basicSalary.getBasicSalary() + bonus.getBonus()+
		perf.getPerformance() - tax.getTax();
	}
	
	
	
}
