package kuaZhanQuPK.PKTwo.mediatorPattern;
/**
 * @author fcs
 * @date 2014-9-13
 * 描述：中介者
 * 说明：里面的方法分为两类：
 * 1.每个类的业务流程，
 * 2.实现抽象中介者定义的方法
 */
public class Mediator extends AbsMediator {
	
	@Override
	public void up(ISalary _salary) {
		upSalary();
		upTax();
	}
	@Override
	public void up(IPosition _position) {
		upPosition();
		upSalary();
		upTax();
	}
	@Override
	public void up(ITax _tax) {
		upTax();
		downSalary();
	}
	@Override
	public void down(ISalary _salary) {
		
	}
	@Override
	public void down(IPosition _Position) {
		
	}
	@Override
	public void down(ITax _tax) {
		
	}
	 
	public void upTax(){
		System.out.println("税收上升，为国家做贡献");
		
	}
	public void upSalary(){
		System.out.println("工资翻倍，乐翻天");
	}
	public void upPosition(){
		System.out.println("职位上升一级，狂喜");
	}
	
	public void downSalary(){
		System.out.println("经济不景气，降低工资");
	}
	public void downTax(){
		System.out.println("税收剪支，国家收入减少");
	}
	public void downPosition(){
		System.out.println("官降三级，比自杀还痛苦。。");
	}
	
	
}
