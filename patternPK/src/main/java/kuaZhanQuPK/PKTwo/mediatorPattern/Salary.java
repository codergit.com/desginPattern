package kuaZhanQuPK.PKTwo.mediatorPattern;

public class Salary extends AbsColleague implements ISalary {

	public Salary(AbsMediator mediator) {
		super(mediator);
	}

	public void increaseSalary() {
		super.mediator.up(this);

	}

	public void decreaseSalary() {
		super.mediator.down(this);

	}
     
}
