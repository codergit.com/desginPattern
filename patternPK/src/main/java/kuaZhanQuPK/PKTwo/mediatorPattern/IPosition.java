package kuaZhanQuPK.PKTwo.mediatorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-13
 * 描述：职位接口
 * 说明：
 */
public interface IPosition {
   //升职
	public void promote();
	//降职
	public void demote();
}
