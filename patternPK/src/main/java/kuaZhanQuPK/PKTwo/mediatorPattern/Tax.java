package kuaZhanQuPK.PKTwo.mediatorPattern;

public class Tax extends AbsColleague implements ITax{
	
	public Tax(AbsMediator mediator) {
		super(mediator);
	}
	public void raise() {
		super.mediator.up(this);
	}
	public void drop() {
		super.mediator.down(this);
	}
}
