package kuaZhanQuPK.PKTwo.mediatorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-13
 * 描述：工资接口
 * 说明：
 */
public interface ISalary {
     //加薪
	public void increaseSalary();
	//降薪
	public void decreaseSalary();
}
