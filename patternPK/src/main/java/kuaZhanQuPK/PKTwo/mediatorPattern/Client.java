package kuaZhanQuPK.PKTwo.mediatorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {
	   //定义中介者
	   Mediator mediator = new Mediator();
       //定义各个同事类
       IPosition position = new Position(mediator);
       ISalary salary = new Salary(mediator);
       ITax tax = new Tax(mediator);
       System.out.println("===职位提升了。。。");
       position.promote();
   
   }
}
