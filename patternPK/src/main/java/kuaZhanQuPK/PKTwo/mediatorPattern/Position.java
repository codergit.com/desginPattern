package kuaZhanQuPK.PKTwo.mediatorPattern;

public class Position extends AbsColleague implements IPosition{

	public Position(AbsMediator mediator) {
		super(mediator);
	}
	public void promote() {
		super.mediator.up(this);
	}
	public void demote() {
		super.mediator.down(this);
	}

}
