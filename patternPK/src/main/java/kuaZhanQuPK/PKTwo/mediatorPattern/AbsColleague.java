package kuaZhanQuPK.PKTwo.mediatorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-13
 * 描述：抽象同事类
 * 说明：
 */
public class AbsColleague {
   //每个同事类都对中介者非常了解
   public AbsMediator mediator;
   public AbsColleague(AbsMediator mediator) {
	this.mediator = mediator;
  } 
}
