package kuaZhanQuPK.PKOne.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：超文本邮件
 * 说明：
 */
public class HtmlMAil extends MailTemplete{

	public HtmlMAil(String from, String to, String subject, String context) {
		super(from, to, subject, context);
	}
	public String getContext(){
		String context = "\nContent-Type:Multipart/mixed;charset=GB2312\n"+super.getContext();
	    context = context + "\n邮件格式为：超文本格式";
	    return context;
	}

}
