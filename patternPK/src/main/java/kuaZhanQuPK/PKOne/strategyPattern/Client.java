package kuaZhanQuPK.PKOne.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：场景类
 * 说明：可以方便的改为TextMail,发送文本邮件
 */
public class Client {
    public static void main(String[] args) {
		MailTemplete m = new HtmlMAil("a@b.com", "b@a.com", "外星人攻占地球", "结局是外星人被地球人打败了");
		MailServer ms = new MailServer(m);
		ms.sendMail();
	}
}
