package kuaZhanQuPK.PKOne.strategyPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：文本邮件
 * 说明：
 */
public class TextMail  extends MailTemplete{

	public TextMail(String from, String to, String subject, String context) {
		super(from, to, subject, context);
	}
	public String getContext()
	{
		String context= "\nContet-Type; text/plain;charset-GB2312\n"+super.getContext();
		//同时对邮件进行base64编码处理
		context = context + "\n邮件格式： 文本格式";
		return context;
	}
}
