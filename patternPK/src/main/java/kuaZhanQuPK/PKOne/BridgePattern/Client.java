package kuaZhanQuPK.PKOne.BridgePattern;


/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {
		MailTemplete m = new HtmlMAil("a@b.com", "b@a.com", "外星人攻占地球", "结局是外星人被地球人打败了");
        MailServer mail = new Postfix(m);
        mail.sendMail();
   }
}
