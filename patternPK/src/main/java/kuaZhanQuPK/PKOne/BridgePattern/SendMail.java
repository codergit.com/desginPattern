package kuaZhanQuPK.PKOne.BridgePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：sendMail邮件服务器
 * 说明：
 */
public class SendMail extends MailServer{
   
	public SendMail(MailTemplete m) {
		super(m);
	}
	public void sendMail(){
		//增加邮件服务器洗洗脑
		super.m.add("received:(sendmail); 7 Nov 2009 40:14:23 + 0100");
		super.sendMail();
	}

}
