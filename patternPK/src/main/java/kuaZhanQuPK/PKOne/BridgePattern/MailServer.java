package kuaZhanQuPK.PKOne.BridgePattern;
/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：邮件服务器
 * 说明：
 */
public abstract  class MailServer {
   //发送的是哪封邮件
	protected final  MailTemplete m;
	public MailServer(MailTemplete m2) {
	   this.m = m2;
    }
	public void sendMail(){
		System.out.println("===正在发送的邮件信息-===");
	    System.out.println("发件人："+m.getFrom() );
	    System.out.println("收件人："+m.getTo());
	    System.out.println("标题："+m.getSubject());
	    System.out.println("邮件内容："+m.getContext());
	}
}
