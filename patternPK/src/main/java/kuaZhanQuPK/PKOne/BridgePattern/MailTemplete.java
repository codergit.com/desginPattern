package kuaZhanQuPK.PKOne.BridgePattern;

public  abstract  class MailTemplete {
	 //邮件发件人
	private String from;
	//收件人
	private String to;
	//主题
	private String subject;
	//内容
	private String context;
	public MailTemplete(String from, String to, String subject, String context) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.context = context;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	//允许增加邮件发送标志
	public void add(String sendInfo){
		context = sendInfo+context;
	}
}
