package kuaZhanQuPK.PKOne.BridgePattern;


/**
 * 
 * @author fcs
 * @date 2014-9-11
 * 描述：Postfix邮件服务器
 * 说明：
 */
public class Postfix  extends MailServer{

	public Postfix(MailTemplete m) {
		super(m);
	}
  
	
	public void sendMail(){
    	//增加邮件服务器的信息
    	String context = "Received: from XXX(unknown [xxx.xxx.xxx.xxx]) by aaa.aaa.com(Postfix) with ESMTP id 8DBCD172B8\n";
        super.m.add(context);
        super.sendMail();
    
    }
}
