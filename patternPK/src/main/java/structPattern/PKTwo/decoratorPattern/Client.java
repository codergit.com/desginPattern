package structPattern.PKTwo.decoratorPattern;


/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {  
	   	System.out.println("很久很久以前，这里有一只丑小鸭。。。。。");
	    SWan ducking  = new UglyDucking();
	    ducking.desAppearence();
	    ducking.cry();
	    ducking.fly();
	    System.out.println("\n小鸭子终于发现自己是一只小天鹅。。。。");
        //进行强化
	    ducking  = new BeautifyAppearance(ducking);
	    ducking = new StrongBehavior(ducking);
        ducking.desAppearence();
        ducking.cry();
        ducking.fly();
   }
}
