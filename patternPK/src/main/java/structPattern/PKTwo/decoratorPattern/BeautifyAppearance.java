package structPattern.PKTwo.decoratorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：外形美化
 * 说明：继承装饰类，实现装饰作用
 */
public class BeautifyAppearance extends Decorator{

	public BeautifyAppearance(SWan _swan) {
		super(_swan);
	}
	@Override
    public void desAppearence(){
    	System.out.println("外表是纯白的，。。。。。");
    }
}
