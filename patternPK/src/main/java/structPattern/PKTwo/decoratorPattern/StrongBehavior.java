package structPattern.PKTwo.decoratorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：强化行为
 * 说明：
 */
public class StrongBehavior   extends Decorator{

	public StrongBehavior(SWan _swan) {
		super(_swan);
	}

	//会飞
	public void fly(){
		System.out.println("会飞行了，。。。。");
	}
	
	
}
