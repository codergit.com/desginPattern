package structPattern.PKTwo.decoratorPattern;
/**]
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：天鹅接口
 * 说明：
 */
public interface SWan {
   public void fly();
   public void cry();
   public void desAppearence();
   
}
