package structPattern.PKTwo.decoratorPattern;

/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：抽象装饰类
 * 说明：
 */
public class Decorator implements SWan {
   private SWan swan;
   //装饰的是谁
   public Decorator(SWan _swan){
	   this.swan = _swan;
   }
	public void fly() {
		swan.fly();
	}

	public void cry() {
		swan.cry();
	}

	public void desAppearence() {
		swan.desAppearence();
	}

	

}
