package structPattern.PKTwo.decoratorPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：丑小鸭，是一只天鹅
 * 说明：
 */
public class UglyDucking implements SWan {

	public void fly() {
		System.out.println("叫声是克鲁----克鲁----克鲁");
	}

	public void cry() {
		System.out.println("外形是脏兮兮的白色，毛茸茸的大头");
	}

	public void desAppearence() {
		System.out.println("不能飞翔");
	}

}
