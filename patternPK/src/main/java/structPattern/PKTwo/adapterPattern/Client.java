package structPattern.PKTwo.adapterPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：场景类
 * 说明：
 */
public class Client {
    public static void main(String[] args) {
		System.out.println("----妈妈有五个孩子，其中四个是这样的，，");
	    Duck duck = new Ducking();
	    duck.cry(); 
	    duck.desAppearance();
	    duck.desBehavior();
	    System.out.println("一只特殊的小鸭子。。。。");
        Duck uglyDucking = new UglyDucking();
        uglyDucking.cry();
        uglyDucking.desAppearance();
        uglyDucking.desBehavior();
	    
	    
	    
    
    
    }
}
