package structPattern.PKTwo.adapterPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：丑小鸭，是鸭子，但是有天鹅的属性。
 * 说明：
 */
public class UglyDucking extends WhiteSwan implements Duck{
   
	public void cry(){
		super.cry();
	}
	public void desAppearance() {
		super.desAppearence();
	}

	public void desBehavior() {
		System.out.println("会游泳。。。");
	    //还会飞行
		super.fly();
	
	}
    
}
