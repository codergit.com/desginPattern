package structPattern.PKTwo.adapterPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：鸭类接口
 * 说明：从鸭子类进行与天鹅的适配
 */
public interface Duck {
 public void cry();
 public void desAppearance();
 public void desBehavior();
 
}
