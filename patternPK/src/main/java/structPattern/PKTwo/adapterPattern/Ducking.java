package structPattern.PKTwo.adapterPattern;
/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：小鸭子
 * 说明：
 */
public class Ducking implements Duck{

	public void cry() {
       System.out.println("叫声是嘎嘎---嘎嘎---嘎嘎");		
	}

	public void desAppearance() {
		System.out.println("外形是黄白相间的，嘴长");
		
	}

	public void desBehavior() {
		System.out.println("会游泳。。。。。");
		
	}
    
}
