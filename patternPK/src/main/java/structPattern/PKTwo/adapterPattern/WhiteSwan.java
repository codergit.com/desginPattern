package structPattern.PKTwo.adapterPattern;
import com.patternPK.structPattern.PKTwo.decoratorPattern.SWan;

/**
 * 
 * @author fcs
 * @date 2014-9-8
 * 描述：白天鹅实现类
 * 说明：
 */
public class WhiteSwan implements SWan{
	public void fly() {
		System.out.println("叫声是克鲁----克鲁----克鲁");
	}

	public void cry() {
		System.out.println("外形是纯白色，惹人喜爱");
	}

	public void desAppearence() {
		System.out.println("能够飞翔");
	}


}
