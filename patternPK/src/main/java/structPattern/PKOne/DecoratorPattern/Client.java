package structPattern.PKOne.DecoratorPattern;

import com.patternPK.structPattern.PKOne.agentPattern.IRunner;
import com.patternPK.structPattern.PKOne.agentPattern.Runner;

/**
 * 
 * @author fcs
 * @date 2014-9-7
 * 描述：场景类
 * 说明：
 */
public class Client {
   public static void main(String[] args) {
	IRunner liu = new Runner();
	liu = new RunnerWithJet(liu);
    System.out.println("---增强后的运动员"); 
    liu.run();
   }
} 
