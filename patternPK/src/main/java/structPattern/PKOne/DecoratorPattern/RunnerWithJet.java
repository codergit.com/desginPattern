package structPattern.PKOne.DecoratorPattern;

import com.patternPK.structPattern.PKOne.agentPattern.IRunner;

public class RunnerWithJet  implements IRunner{
	private IRunner runner;
	public RunnerWithJet(IRunner _runner){
		this.runner = _runner;
		
	}
	public void run() {
		System.out.println("加快运动员的速度，加喷气装置");
	    runner.run();
	}
}
