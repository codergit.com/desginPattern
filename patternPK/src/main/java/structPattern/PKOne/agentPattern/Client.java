package structPattern.PKOne.agentPattern;


public class Client {
   public static void main(String[] args) {
	  IRunner liu = new Runner();
	  System.out.println("---客人找到运动员的代理要求其去跑步----");
      IRunner agent = new RunnerAgent(liu);
      agent.run();
   }
}
