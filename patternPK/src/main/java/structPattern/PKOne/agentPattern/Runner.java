package structPattern.PKOne.agentPattern;


/**
 * 
 * @author fcs
 * @date 2014-9-7
 * 描述：运动员跑步
 * 说明：
 */
public class Runner implements  IRunner{

	public void run() {
		System.out.println("运动员跑步，动作潇洒");
	}

}
