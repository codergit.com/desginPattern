package togetherThree;

import java.util.Observable;

/**
 * 
 * @author fcs
 * @date 2014-9-17
 * 描述：产品事件
 * 说明：
 */
public class ProductEvent  extends Observable{
   //事件起源
	private Product source;
	//事件类型
	private ProductEventType type;
	//传入事件的源头，默认为新建类型
	public  ProductEvent(Product p )
	{
		this(p,ProductEventType.NEW_RPODUCT);
	}
	
	//事件源头以及事件类型
	public ProductEvent(Product p, ProductEventType pe){
		this.source = p;
		this.type = pe;
		//事件触发
		notifyEventDispatch();
		
	}
	
	//获得事件源头
	public Product getSource(){
		return source;
	}
	
	//获得事件的类型
	public ProductEventType getEventType(){
		return this.type;
	}
	
	//通知事件处理中心
	//作用：明确事件的观察者，并同时在初始化时通知观察者
	//在有参构造方法中被调用，
	private void notifyEventDispatch(){
		super.addObserver(EventDispatch.getEeventDispatch());
		super.setChanged();
		super.notifyObservers(source);
	}
}
