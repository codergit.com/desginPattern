package togetherThree;
/**
 * 
 * @author fcs
 * @date 2014-9-17
 * 描述：事件类型定义
 * 说明：定义了四个事件类型
 */
public enum ProductEventType {
	//新建一个产品
      NEW_RPODUCT(1),
    //删除一个产品
      DEL_PRODUCT(2),
      //修改产品
      EDIT_PRODUCT(3),
      //克隆一个产品
      CLONE_PRODUCT(4);
      private int value = 0;
      private ProductEventType(int _value){
    	  this.value = _value;
      }
      public int getValue(){
    	  return this.value;
      }
      
      
      
}
