package togetherThree;
/**
 * 
 * @author fcs
 * @date 2014-9-17
 * 描述：定义事件处理者
 * 说明：
 */
public class Beggar  extends EventCustomer {

	public Beggar() {
		super(EventCustomType.DELETE);
	}

	@Override
	public void exec(ProductEvent event) {
		//事件的源头
		Product p = event.getSource();
		//事件类型
		ProductEventType type = event.getEventType();
		System.out.println("乞丐处理事件 ： "+p.getName() +"销毁，事件类型 = "+type);
		
		
	}
     
}
