package togetherThree;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;


/**
 * 
 * @author fcs
 * @date 2014-9-17
 * 描述：事件观察者
 * 说明：使用单例模式
 * 避免对象膨胀，但同时也有性能和线程安全的隐患
 * 产品和事件都定义成功，使用桥梁模式将两个独立的对象组合起来
 * 
 * 该类有三个职责
 *  1.事件的观察者
 *    作为观察者模式中的观察者角色，接收被观察期望完成的任务，
 *    在该框架中就是接收ProductEvent事件
 *  2.事件分发者
 *    作为中介者模式的中介者角色，它担当者非常重要的任务----分发事件
 *    同时协调各个同事类（也就是事件的处理者）处理事件
 *  3.事件处理者的管理员角色
 *    不是每个事件的处理者都可以接受事件并进行处理，是需要获得分发者许可
 *    后才可以，也就是说只有事件分发者允许它处理，它才能处理。
 *    
 *    违反了单一原则，但是，拆开的话会增加系统的复杂性，并且代码量会增加
 *    ，因为设计原则只是一个理论，而不是一个带有刻度的标尺，因此在系统
 *    设计中不应该把它视为不可逾越的屏障，而是应该把它看成是一个方向标，
 *    尽量遵守，而不是必须恪守。
 *    
 */
public class EventDispatch implements Observer{
    
	//单例模式
	private final static EventDispatch dispatch = new EventDispatch();
	//事件消费者
	private Vector<EventCustomer> customer =  new Vector<EventCustomer>();
	
	
	//不允许生成多个实例
	private EventDispatch(){}
	
	public static EventDispatch getEeventDispatch(){
		return dispatch;
		
	}
	public void addObserver(Observer observer) {
		
	}
	public void deleteObserver(Observer observer) {
		
	}
	public void notifyObserver(String context) {
		
	}
	//事件触发器
	public void update(Observable o, Object arg) {		
		//事件的源头
		Product product = (Product)arg;
		//事件
		ProductEvent event = (ProductEvent)o;
		//处理者处理，这里是中介者模式的核心，可以上很复杂的业务逻辑
		for(EventCustomer e:customer){
			//处理能力是否匹配
			for(EventCustomType t:e.getCustomType()){
				if(t.getValue() ==event.getEventType().getValue()){
					e.exec(event);
				}
			}
		}	
	}
	//注册事件处理者
	public void registerCustomer(EventCustomer _customer){
	     customer.add(_customer);
	}
}
