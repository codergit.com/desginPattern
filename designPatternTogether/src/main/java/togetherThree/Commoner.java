package togetherThree;
/**
 * 
 * @author fcs
 * @date 2014-9-17
 * 描述：平民
 * 说明：
 */
public class Commoner extends EventCustomer{

	public Commoner() {
		super(EventCustomType.NEW);
	}

	@Override
	public void exec(ProductEvent event) {
		//事件的源头
		Product p = event.getSource();
		//事件类型
		ProductEventType type = event.getEventType();
		System.out.println("平民处理事件 ： "+p.getName() +"诞生记事件类型 =  "+type);
		
	}
}
