package togetherThree;
/**
 * 
 * @author fcs
 * @date 2014-9-17
 * 描述：产品类
 * 说明：product 与ProductManager形成单来源调用
 */
public class Product  implements Cloneable {
   private String name;
   //是否可以属性变更
   private boolean canChanged = false;
   //产生一个新的产品
   public Product(ProductManager pm,String name){
	   //允许建立 产品
	   if(pm.isCreateProduct()){
		   canChanged = true;
		   this.name = name;
	   }
   }
   
   public String  getName(){
	   return name;
   }
   public void setName(String name){
	   if(canChanged){
		   this.name = name;
	   }
   }
   //重写clone方法
   public Product chlone(){
	   Product p = null;
	   try {
		p = (Product)super.clone();
	} catch (CloneNotSupportedException e) {
		e.printStackTrace();
	}
	   return p;
   }
   
}
