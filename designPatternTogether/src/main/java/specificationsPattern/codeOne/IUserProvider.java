package specificationsPattern.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：用户操作对象接口
 * 说明：实现两个基本查询
 */
public interface IUserProvider {
    //根据用户名查找用户
	public ArrayList<User> findUserByNameEqual(String name);
	//年龄大于指定年龄的用户
	public ArrayList<User> findUserByAgeThan(int age);
}
