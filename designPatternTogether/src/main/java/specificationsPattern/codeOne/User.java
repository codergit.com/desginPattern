package specificationsPattern.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：用户类
 * 说明：
 */
public class User {
	//姓名
	private String name;
	//年龄
	private int age;
	public User(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString(){
		return "用户名： "+name+"\t"+"年龄："+age;
	}
	
}
