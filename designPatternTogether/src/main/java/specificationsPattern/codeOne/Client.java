package specificationsPattern.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：场景类
 * 说明：可以进一步进行抽取共同点
 */
public class Client {
     public static void main(String[] args) {
    	 ArrayList<User> userList = new ArrayList<User>();
    	 userList.add(new User("苏大",3));
    	 userList.add(new User("牛儿",4));
    	 userList.add(new User("李斯",32));
    	 userList.add(new User("张三",22));
    	 userList.add(new User("王五",34));
    	 userList.add(new User("阿九",40));
    	 userList.add(new User("张为",22));
    	 userList.add(new User("大神",38));
    	 IUserProvider userProvider = new UserProvider(userList);
    	 for(User u: userProvider.findUserByAgeThan(20)){
    		 System.out.println(u);
    	 }
     }	 
}
