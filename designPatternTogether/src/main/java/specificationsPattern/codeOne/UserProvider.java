package specificationsPattern.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：用户操作类
 * 说明：
 */
public class UserProvider  implements IUserProvider {
	
	//用户列表
	private ArrayList<User> userList;
	
	//构造函数传递用户列表
	public UserProvider(ArrayList<User> _userList){
		this.userList = _userList;
	}
	
	public ArrayList<User> findUserByNameEqual(String name) {
		ArrayList<User> result = new ArrayList<User>();
		for(User u :userList){
		   if(u.getName().equals(name)){	
			   result.add(u);
			   
		   }
		}
		return result;
	}
	//年龄大于指定年龄的用户
	public ArrayList<User> findUserByAgeThan(int age) {
		ArrayList<User> result = new ArrayList<User>();
		for(User u : userList){
			if(u.getAge() > age){
				result.add(u);
			}
		}
		return result;
	}

}
