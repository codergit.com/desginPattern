package specificationsPattern.codeThree;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：非规格说明书
 * 说明：
 */
public class NotSpecification extends CompositeSpecification {
  
	//传递一个规格书
	private IUserSpecification spec;
	
	public NotSpecification(IUserSpecification spec) {
		this.spec = spec;
	}

	@Override
	public boolean isSatusfiedBy(User user) {
		return !spec.isSatusfiedBy(user);
	}

}
