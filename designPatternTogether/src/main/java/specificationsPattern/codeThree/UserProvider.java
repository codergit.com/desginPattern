package specificationsPattern.codeThree;

import java.util.ArrayList;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：用户操作
 * 说明：
 */
public class UserProvider  implements IuserProvider {
    //用户列表
	public ArrayList<User> userList;
	//传递用户列表
	public UserProvider(ArrayList<User> _userList){
		this.userList = _userList;
	}
	public ArrayList<User> findUser(IUserSpecification userSpec) {
		ArrayList<User> result = new ArrayList<User>();
		for(User u : userList){
			if(userSpec.isSatusfiedBy(u)){
				result.add(u);
			}
		}
		return result;
	}
}
