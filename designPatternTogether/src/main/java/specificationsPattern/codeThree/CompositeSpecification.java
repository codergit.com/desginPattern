package specificationsPattern.codeThree;
import com.designPatternTogether.specificationsPattern.codeOne.User;
/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：组合规格说明书
 * 说明：
 */
public abstract class CompositeSpecification implements IUserSpecification{
   //是否满足条件由实现类实现
	public abstract  boolean isSatusfiedBy(User user);
	
    
	public IUserSpecification and(IUserSpecification spec) {
		return new AndSpecification(this, spec);
	}

	public IUserSpecification or(IUserSpecification spec) {
		return new OrSpecifaction(this,spec);
	}

	public IUserSpecification not() {
		return new NotSpecification(this);
	}

}
