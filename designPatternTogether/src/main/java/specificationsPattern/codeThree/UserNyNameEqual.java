package specificationsPattern.codeThree;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：姓名相同规格书
 * 说明：
 */
public class UserNyNameEqual  extends CompositeSpecification{
    
	private String name;
    
	public UserNyNameEqual(String name) {
		this.name = name;
	}

	//校验用户是否满足条件
	@Override
	public boolean isSatusfiedBy(User user) {
		return user.getName().equals(name);
	}

}
