package specificationsPattern.codeThree;

import java.util.ArrayList;

import com.designPatternTogether.specificationsPattern.codeOne.User;


/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：场景类
 * 说明：
 */
public class Client {
  public static void main(String[] args) {
	   //初始化用户
	  ArrayList<User> userList = new ArrayList<User>();
      userList.add(new User("苏国庆",10));
	  userList.add(new User("国庆牛",76));
      userList.add(new User("李斯",45));
      userList.add(new User("张国庆",49));
      //定义一个用户查询类
      IuserProvider userProvider = new UserProvider(userList);
      System.out.println("名字包含国庆的人员");
      IUserSpecification spec = new UserByAgeThan(24);
      IUserSpecification spec1 = new UserByNameLike("%国庆%");
   
    for(User u : userProvider.findUser(spec.and(spec1))){
   	     System.out.println(u);
    }
      
      
  }
  
}
