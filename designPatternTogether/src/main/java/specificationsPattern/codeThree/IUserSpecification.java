package specificationsPattern.codeThree;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：带与或非的规格说明书接口
 * 说明：此时需要基类依赖父类的情况
 * 注意：父类依赖子类的情景只有在非常明确不会发生变化的场景中存在，
 * 它不具备扩展性，是一种固化而不可变的结构。
 */
public interface IUserSpecification {
   //候选者是否满足条件
	public boolean isSatusfiedBy(User user);
   //and操作
	public IUserSpecification and(IUserSpecification spec);
   //or操作
	public IUserSpecification or(IUserSpecification spec);
   //not操作
	public IUserSpecification not();

}
