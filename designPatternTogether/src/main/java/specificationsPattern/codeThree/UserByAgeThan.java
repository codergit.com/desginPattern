package specificationsPattern.codeThree;

import com.designPatternTogether.specificationsPattern.codeOne.User;

public class UserByAgeThan extends CompositeSpecification{
	private int age;
	
	public UserByAgeThan(int age) {
		this.age = age;
	}

	@Override
	public boolean isSatusfiedBy(User user) {
		return user.getAge() == age;
	}

}
