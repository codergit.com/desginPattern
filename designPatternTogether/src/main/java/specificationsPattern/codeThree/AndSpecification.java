package specificationsPattern.codeThree;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：与规格书
 * 说明：
 */
public class AndSpecification extends CompositeSpecification {
    
	//传递两个规格书进行and操作
	private IUserSpecification left;
	private IUserSpecification right;
	
	
	public AndSpecification(IUserSpecification left, IUserSpecification right) {
		this.left = left;
		this.right = right;
	}
	@Override
	public boolean isSatusfiedBy(User user) {
		return left.isSatusfiedBy(user) &&right.isSatusfiedBy(user);
	}

}
