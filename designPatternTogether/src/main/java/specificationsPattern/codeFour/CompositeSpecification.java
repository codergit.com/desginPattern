package specificationsPattern.codeFour;


/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：组合规格书
 * 说明：
 */
public abstract   class CompositeSpecification implements ISpecification {
     //是否满足条件由实现类实现
	public abstract boolean isSatisfiedBy(Object candidate);

	public ISpecification and(ISpecification spec) {
		return new AndSpecification(this, spec);
	}

	public ISpecification or(ISpecification spec) {
		return new OrSpecifaction(this, spec);
	}

	public ISpecification not() {
		return new NotSpecification(this);
	}

}
