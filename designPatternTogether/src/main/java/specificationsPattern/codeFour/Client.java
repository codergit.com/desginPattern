package specificationsPattern.codeFour;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：场景类
 * 说明：
 */
public class Client {
     public static void main(String[] args) {
		   ArrayList<Object> list = new ArrayList<Object>();
		   //定义两个业务规格书
		   ISpecification spec1 = new BizSpecification(new Object());
	       ISpecification  spec2 = new BizSpecification(new Object());
           for(Object obj :list){
        	   if(spec1.and(spec2).isSatisfiedBy(obj)){
        		   System.out.println(obj);
        	   }
           }
	         
	       
	       
     }
}
