package specificationsPattern.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：抽象规格书
 * 说明：
 */
public interface ISpecification {
    //候选者是否满足要求
	public boolean isSatisfiedBy(Object candidate);
   //and操作
	public ISpecification and(ISpecification spec);
   //or操作
	public ISpecification or(ISpecification spec);
   //not操作
	public ISpecification not();



}
