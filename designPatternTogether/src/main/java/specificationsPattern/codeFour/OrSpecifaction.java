package specificationsPattern.codeFour;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：or操作规格说明书
 * 说明：
 */
public class OrSpecifaction extends CompositeSpecification{
	 //传递两个规格书进行and操作
	private ISpecification left;
	private ISpecification right;
	
	public OrSpecifaction(ISpecification left, ISpecification right) {
		this.left = left;
		this.right = right;
	}
	@Override
	public boolean isSatisfiedBy(Object candidate) {
		return left.isSatisfiedBy(candidate) || right.isSatisfiedBy(candidate);
	}
}
