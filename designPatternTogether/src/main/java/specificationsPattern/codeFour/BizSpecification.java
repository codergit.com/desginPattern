package specificationsPattern.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：业务规格说明书
 * 说明：
 */
public class BizSpecification extends CompositeSpecification{
    private Object obj;
	public BizSpecification(Object obj) {
		this.obj = obj;
	}
	@Override
	public boolean isSatisfiedBy(Object candidate) {
		//根据基准对象和候选对象，进行业务判断，返回Boolean
		return false;
	}
} 
