package specificationsPattern.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：非规格说明书
 * 说明：
 */
public class NotSpecification extends CompositeSpecification {
	//传递一个规格书
	private ISpecification spec;
	public NotSpecification(ISpecification _spec){
		this.spec = _spec;
		
	}
	
	@Override
	public boolean isSatisfiedBy(Object candidate) {
		return !spec.isSatisfiedBy(candidate);
	}
  

}
