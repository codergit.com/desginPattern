package specificationsPattern.codeTwo;

import com.designPatternTogether.specificationsPattern.codeOne.User;

public class UserByAgeThan implements IUserSpecification {
	//��׼����
	private int age;
	public UserByAgeThan(int age) {
		this.age = age;
	}
	public boolean isSatusfiedBy(User user) {
		return user.getAge() > age;
	}
}
