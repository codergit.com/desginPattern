package specificationsPattern.codeTwo;

import java.util.ArrayList;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：用户操作接口
 * 说明：
 */
public interface IuserProvider {
    //根据条件查找用户
	public ArrayList<User> findUser(IUserSpecification userSpec);



}
