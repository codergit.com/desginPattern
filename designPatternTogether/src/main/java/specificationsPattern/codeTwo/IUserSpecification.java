package specificationsPattern.codeTwo;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：规格说明书接口
 * 说明：
 */
public interface IUserSpecification {
   //候选者是否满足条件
	public boolean isSatusfiedBy(User user);
}
