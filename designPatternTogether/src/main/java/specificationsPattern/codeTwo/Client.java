package specificationsPattern.codeTwo;

import java.util.ArrayList;

import com.designPatternTogether.specificationsPattern.codeOne.User;

public class Client {
    public static void main(String[] args) {
    	 ArrayList<User> userList = new ArrayList<User>();
    	 userList.add(new User("苏大",3));
    	 userList.add(new User("牛儿",4));
    	 userList.add(new User("李斯",32));
    	 userList.add(new User("张三",22));
    	 userList.add(new User("王五",34));
    	 userList.add(new User("阿九",40));
    	 userList.add(new User("张为",22));
    	 userList.add(new User("大神",38));
    	 //定义一个用户查询类
    	 IuserProvider userProvider = new UserProvider(userList);
	     System.out.println("年龄大于20岁的用户");
	     //定义一个规格说明书
	     IUserSpecification userSpec = new UserByAgeThan(20);
	     
         for(User u : userProvider.findUser(userSpec)){
        	 System.out.println(u);
         }
    
    
    }
}
