package specificationsPattern.codeTwo;

import com.designPatternTogether.specificationsPattern.codeOne.User;

/**
 * 
 * @author fcs
 * @date 2014-9-18
 * 描述：姓名相同的规格书
 * 说明：
 */
public class UserByNameEqual  implements IUserSpecification {

	//基准姓名
	private String name;
	
	
	public UserByNameEqual(String name) {
		super();
		this.name = name;
	}
	public boolean isSatusfiedBy(User user) {
		return user.getName().equals(name);
	}
}
