package togetherOne;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：文件管理类
 * 说明：
 */
public class FileManager {
   //ls命令
	public static String ls(String path){
		return "file1\nfile2\nfile3\nfile4\n";
	}
	
	public static String ls_l(String path){
		String str = "drw-rw-rw root system 1024 2009-8-20 10:23 file1\n";
	    str = str + "drw-rw-rw root system 1024 2009-8-20 10:23 file2\n";
	    str = str + "drw-rw-rw root system 1024 2009-8-20 10:23 file３\n";
	     return str;
	}
	// ls -a命令
	public static String ls_a(String path){
		String str = ".\n..\nfile1\nfile2\nfile3\n";
		return str;
	}
	
	
}
