package togetherOne;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：抽象命令类
 * 说明：
 */
public abstract  class CommandName {
      private CommandName nextOperator;
      public final String handleMessage(CommandVO comvo){
    	  String result = "";
    	  //判断是否是自己处理的参数
    	  
    	  if(comvo.getParam().size() == 0 || comvo.getParam().contains(this.getOperatorParam())){
    		  result = this.echo(comvo);
    	  }else{
    		  if(this.nextOperator != null){
    			   result = this.nextOperator.handleMessage(comvo);
    		  }else{
    			  result = "命令无法执行";
    		  }
    	  }
    	  return result;
      }
      //设置剩余参数由谁来处理
      public void setNext(CommandName _operator){
    	  this.nextOperator = _operator;
      }
      //每个处理者都要处理一个后缀参数
      protected abstract String getOperatorParam();
      
      //每个处理者都必须实现处理任务
      protected abstract String echo(CommandVO vo);
      
      
      
}
