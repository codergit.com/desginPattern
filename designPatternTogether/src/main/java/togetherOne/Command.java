package togetherOne;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：抽象命令
 * 说明：
 */
public  abstract class Command {
    public abstract String execute(CommandVO vo);
    //建立链表
    protected final List<? extends CommandName> buildChain(Class<? extends CommandName> abstractClass){
    	List<Class> classes = ClassUtils.getSonClass(abstractClass);
    	//存放命令的实例，并建立链表的关系
    	List<CommandName> commandNameList = new ArrayList<CommandName>();
    	   for(Class c: classes){
    		CommandName commandName = null;
    		try{
    			//产生实例
    			commandName = (CommandName)Class.forName(c.getName()).newInstance();		
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		if(commandNameList.size() > 0){
    			commandNameList.get(commandNameList.size() - 1).setNext(commandName);
    			
    		}
    		commandNameList.add(commandName);
    	}
    	return commandNameList;    	
    }
} 
