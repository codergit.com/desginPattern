package togetherOne;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：
 * 说明：空的抽象类，有两个职责
 * 1.标记ls命令族
 * 2.个性化处理
 */
public  abstract class AbstractLS extends CommandName {
    //默认参数
	public final static String DEFAULT_PARAM = "";
	
	public final static String A_PARAM = "a";

    public final static String L_PARAM = "l";
}
