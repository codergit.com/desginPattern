package togetherOne;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：命令分发
 * 说明：
 */
public class Invoker {
    public String exec(String _commandStr){
    	//定义返回值
    	String result = "";
    	//首先解析命令
    	CommandVO vo = new CommandVO(_commandStr);
    	if(CommandEnum.getNames().contains(vo.getCommandName())){
    		String className = CommandEnum.valueOf(vo.getCommandName()).getValue();
    	    Command command;
    	    try{
    	    	command = (Command)Class.forName(className).newInstance();
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	}else{
    		result = "无法执行命令，请检查命令格式";
    	}
    	return result;
    	
    }
}
