package togetherOne;
/**
 * 
 * @author fcs
 * @date 2014-9-15
 * 描述：具体的ls命令
 * 说明：
 */
public class LSCommand extends Command {
	@Override
	public String execute(CommandVO vo) {
		//返回链表的首节点
		CommandName firstNode = super.buildChain(AbstractLS.class).get(0);
		return firstNode.handleMessage(vo);
	}
}
