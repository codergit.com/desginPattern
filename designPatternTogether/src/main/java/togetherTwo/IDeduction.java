package togetherTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-16
 * 描述：扣款策略接口
 * 说明：固定扣款，和自由扣款
 */
public interface IDeduction {
   //扣款，提供交易和卡信息，进行扣款，并返回扣款是否成功
	public boolean exec(Card card, Trade trade);
}
