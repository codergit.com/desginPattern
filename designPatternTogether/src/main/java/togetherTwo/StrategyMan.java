package togetherTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-16
 * 描述：负责对具体策略的映射
 * 说明：使用枚举对策略类进行映射处理，避免高层模块直接访问策略类
 * 同时由工厂方法模式根据映射产生策略对象
 * 相当于一个登记容器，所有的具体策略都在这里登记，然后提供给工厂方法模式。
 */
public enum StrategyMan {
      SteadyDeduction("com.designPatternTogether.togetherTwo.SteadyDeduction");
	//有问题
      // FreeDeduction("com.designPatternTogether.togetherTwo.FreeDeduction");

   String value = "";
   private StrategyMan(String _value){
	   this.value = _value;
	   
   }
   public  String getValue(){
	   return this.value;
   }

}
