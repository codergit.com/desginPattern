package togetherTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-16
 * 描述：扣款模块封装，使用门面模式
 * 说明：相当于一个子系统
 */
public class DeductionFacade {
    //对外公布的扣款信息
	public static Card deduct(Card card ,Trade trade){
		//获得消费策略
		StrategyMan reg = getDeduction(trade);
		//初始化一个消费策略
		IDeduction deduction = StrategyFactory.getDeduction(reg);
		//产生一个策略上下文
		DeductionContext  context = new DeductionContext(deduction);
		context.exec(card, trade);
		//返回扣款处理后完毕的数据
		return card;
		
		
	}
    //获得对应的商户消费策略
	public static StrategyMan getDeduction(Trade trade){
		//模拟操作
		if(trade.getTradeNo().contains("abc")){
			return StrategyMan.SteadyDeduction;
		}else{
			//需要进行更改
			return StrategyMan.SteadyDeduction;
		}
	}
	
}
