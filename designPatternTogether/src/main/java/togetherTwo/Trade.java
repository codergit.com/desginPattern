package togetherTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-16
 * 描述：交易类
 * 说明：负责记录每一笔交易，
 */
public class Trade {
     //交易编码
	private String tradeNo = "";
	//交易金额
	private int amoun = 0;
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public int getAmoun() {
		return amoun;
	}
	public void setAmoun(int amoun) {
		this.amoun = amoun;
	}
	
}
