package togetherTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-16
 * 描述：扣款策略的封装
 * 说明：策略的上下文角色
 */
public class DeductionContext {
   //扣款策略
	private IDeduction deduction  = null;
	//构造函数传递策略
	public DeductionContext(IDeduction _deduction){
		this.deduction = _deduction;
	}
	//执行扣款
	public boolean exec(Card card,Trade trade){
		return this.deduction.exec(card, trade);
	}
}
