package togetherTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-16
 * 描述：策略工厂
 * 说明：
 */
public class StrategyFactory {
     public static IDeduction getDeduction(StrategyMan strategy){
    	 IDeduction deduction = null;
    	try {
			deduction = (IDeduction)Class.forName(strategy.getValue()).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return deduction;
    	
     }
}
