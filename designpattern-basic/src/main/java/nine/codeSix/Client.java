package nine.codeSix;


/**
 * 
 * @author fcs
 * @date 2014-8-14
 * 描述：将Receiver封装了后的场景类
 * 说明：高层模块不需要知道执行者是谁
 */
public class Client {
	public static void main(String[] args) {
		//首先声明调用者Invoker
		Invoker invoker = new Invoker();
		//定义一个发送给接收者的命令
		Command command = new ConcreteCommand();
		//把命令交给接收者去执行
		invoker.setCommand(command);
		invoker.action();
	}
}
