package nine.codeSix;

import com.design.nine.codeThree.Receiver;

/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：抽象命令，完美的command类
 * 说明：接收者可以有多个，命令角色是命令模式的核心，
 */
public abstract class Command {
	protected final Receiver receiver;
	//实现类必须定义一个接受者
	public Command(Receiver _receiver){
		this.receiver = _receiver;
	}
	//每个命令都必须有一个执行命令的方法
	public abstract void    execute();

}
