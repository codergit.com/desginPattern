package nine.codeSix;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：执行者，封装具体执行命令的模块
 * 说明：
 */
public class Invoker {
	private Command command;
	public void setCommand(Command command){
		this.command = command;
	}
	public void action(){
		this.command.execute();
	}
}
