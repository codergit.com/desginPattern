package nine.codeSix;

import com.design.nine.codeThree.Receiver;
/**
 * 
 * @author fcs
 * @date 2014-8-14
 * 描述：具体的命令
 * 说明：简化了许多，每个命令完成单一的职责，而不是根据接收者的不同完成
 * 不同的职责，在高层模块的调用时就不用考虑接收者是谁的问题。
 */
public class ConcreteCommand2 extends Command{
	
	public ConcreteCommand2(Receiver _receiver) {
		super(_receiver);
	}

	//声明自己的默认接收者
	public ConcreteCommand2(){
		super(new ConcreateReceiver2());
	}
	//必须实现一个命令
	@Override
	public void execute() {
		//业务处理
		this.receiver.dosomething();
	}

}

