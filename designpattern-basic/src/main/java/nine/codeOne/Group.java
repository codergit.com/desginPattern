package nine.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：命令模式的根类
 * 说明：抽象类，抽象出某个部门模块的共有方法。
 */
public  abstract class Group {
    	//客户找项目组
	 	public abstract void find();
	 	//客户要求添加
		public  abstract void add();
		//客户要求更改
		public  abstract void update();
		//客户要求删除
		public abstract void delete();
		//客户提交计划变更
		public abstract void plan();
		
}
