package nine.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：需求组实现类
 * 说明：
 */
public class RequestGroup extends Group {

	@Override
	public void find() {
		System.out.println("客户寻找需求组。。。。");
	}

	@Override
	public void add() {
		System.out.println("客户要求添加新的需求。。。");
	}

	@Override
	public void update() {
		System.out.println("客户要求添加新的需求。。。。");
	}

	@Override
	public void delete() {
		System.out.println("客户要求删除需求。。。");
	}

	@Override
	public void plan() {
		System.out.println("客户要求更改计划 需求。。。");
	}

}
