package nine.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：命令模式场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		//客户找到需求组,要求更改一项需求
		Group group = new RequestGroup();
		group.find();
		group.update();
		group.plan();
	}
}
