package nine.codeThree;

public class Client {
	public static void main(String[] args) {
		//定义执行者
		Invoker invoker = new Invoker();
		//定义接收者
		Receiver receiver = new ConcreateReceiver();
		//定义命令
		Command command = new ConcreteCommand(receiver);
		invoker.setCommand(command);
		invoker.action();
	}
}	
