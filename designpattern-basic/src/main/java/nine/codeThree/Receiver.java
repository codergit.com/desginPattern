package nine.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：抽象的接收者，
 * 说明：因为接收者不止一个，因此可以使用继承实现具体执行命令的任务。
 * 有多个就需要制定一个所有特性的抽象集合。
 */
public abstract class Receiver {
	//执行任务
	public abstract void dosomething();
}
