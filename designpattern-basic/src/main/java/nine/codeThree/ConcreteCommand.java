package nine.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：具体的命令者类
 * 说明：可以在实际应用中扩展该命令类，在每个命令类中通过构造函数定义
 * 了该命令是针对哪一个接收者发送的，定义一个命令接收的主体，调用者非常简单
 * 仅实现命令的传递。
 */
public class ConcreteCommand extends Command{
	private Receiver  receiver;
	public ConcreteCommand(Receiver receiver){
		this.receiver = receiver;
	}
	//必须实现一个命令
	@Override
	public void execute() {
		//业务处理
		this.receiver.dosomething();
	}

}
