package nine.codeThree;

public class ConcreteCommand2 extends Command{
	private Receiver  receiver;
	public ConcreteCommand2(Receiver receiver){
		this.receiver = receiver;
	}
	//必须实现一个命令
	@Override
	public void execute() {
		//业务处理
		this.receiver.dosomething();
	}

}

