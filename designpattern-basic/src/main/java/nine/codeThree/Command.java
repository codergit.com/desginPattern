package nine.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：抽象命令
 * 说明：接收者可以有多个，这要依赖叶无道额具体执行，命令角色是命令模式
 * 核心，
 */
public abstract class Command {
	public abstract void    execute();

}
