package nine.codeFour;

import com.design.nine.codeTwo.Command;

/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：增加需求的命令
 * 说明：继承Command类，并实现execute方法具体调用执行方法。
 */
public class AddRequireCommand extends Command{
	//增加一项需求的命令
	@Override
	public void execute() {
		//找到需求组
		super.reqGroup.find();
		//增加一份需求
		super.reqGroup.add();
		//页面也要增加
		super.pageGroup.add();
		//功能也要增加
		super.codeGroup.add();
		//给出计划
		super.reqGroup.plan();
	}

}
