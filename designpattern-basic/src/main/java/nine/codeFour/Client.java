package nine.codeFour;

import com.design.nine.codeTwo.Command;
import com.design.nine.codeTwo.Invoker;

/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：增强版的命令模式
 * 说明：客户端只要发出命令，至于如何执行这个命令，是协调一个对象
 * 还是两个对象都不需要关心，命令模式做了一层非常好的封装。
 */
public class Client {
	public static void main(String[] args) {
		//定义负责人
		Invoker xiao = new Invoker();
		//客户要求增加一项需求
		Command command = new AddRequireCommand();
		//接头人接收命令
		xiao.setCommand(command);
		//执行命令
		xiao.action();

	}
}
