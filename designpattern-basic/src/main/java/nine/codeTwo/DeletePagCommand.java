package nine.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：删除界面的命令
 * 说明：
 */
public class DeletePagCommand extends Command{

	@Override
	public void execute() {
		super.pageGroup.find();
		super.pageGroup.delete();
		super.pageGroup.plan();
	}

}
