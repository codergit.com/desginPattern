package nine.codeTwo;

/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：增加需求的命令
 * 说明：继承Command类，并实现execute方法具体调用执行方法。
 */
public class AddRequireCommand extends Command{

	@Override
	public void execute() {
		super.reqGroup.find();
		super.reqGroup.add();
		super.reqGroup.plan();
	}

}
