package nine.codeTwo;

import com.design.nine.codeOne.CodeGroup;
import com.design.nine.codeOne.PageGroup;
import com.design.nine.codeOne.RequestGroup;

/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：抽象命令
 * 说明：command类是整个扩展的核心,具体的实现类只需要执行execute方法即可。
 * 可以有N个子类，如增加一个功能命令等。。。。
 */
public abstract class Command {	
	   //三个组都定义好，子类可以直接使用。
		protected RequestGroup reqGroup = new RequestGroup();
		protected PageGroup pageGroup = new PageGroup();
		protected CodeGroup codeGroup = new CodeGroup();
		
		//具体执行方法
		public abstract void execute();
}
