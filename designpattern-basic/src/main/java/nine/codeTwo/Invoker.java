package nine.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：具体负责人
 * 说明：
 */
public class Invoker {
	//什么命令
	private Command command;
	//客户发出命令
	public  void setCommand(Command command){
		this.command = command;
	}	
	//执行客户发出的命令
	public void action(){
		this.command.execute();
	}


}
