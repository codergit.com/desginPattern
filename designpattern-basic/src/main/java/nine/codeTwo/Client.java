package nine.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：增强版的命令模式
 * 说明：实现了高内聚的特性，因为只需要知道执行结果，不需要知道是谁执行的
 * 执行过程怎么样。。。
 */
public class Client {
	public static void main(String[] args) {
		//定义负责人
		Invoker xiao = new Invoker();
		//客户要求增加一项需求
		Command command = new AddRequireCommand();
		//接头人接收命令
		xiao.setCommand(command);
		//执行命令
		xiao.action();

	}
}
