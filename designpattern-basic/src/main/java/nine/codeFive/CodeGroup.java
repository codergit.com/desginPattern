package nine.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：命令模式
 * 说明：项目组第三个
 */
public class CodeGroup extends Group{

	@Override
	public void find() {
		System.out.println("客户寻找代码组。。。。。。");
	}

	@Override
	public void add() {
		System.out.println("客户要求添加功能。。。。。");
	}

	@Override
	public void update() {
		System.out.println("客户要求更改功能。。。。。");
	}

	@Override
	public void delete() {
		System.out.println("客户要求删除功能。。。。。");
	}

	@Override
	public void plan() {
		System.out.println("客户要求更改计划。。。。。。");
	}

}
