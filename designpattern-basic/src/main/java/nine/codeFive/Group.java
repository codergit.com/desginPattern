package nine.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：命令模式的根类
 * 说明：抽象类，抽象出某个部门模块的共有方法。
 * 增加了一个回滚的方法，每个接收者都可以对自己实现的任务进行回滚。
 * 可以根据事务日志进行回滚
 */
public  abstract class Group {
    	//客户找项目组
	 	public abstract void find();
	 	//客户要求添加
		public  abstract void add();
		//客户要求更改
		public  abstract void update();
		//客户要求删除
		public abstract void delete();
		//客户提交计划变更
		public abstract void plan();
		//每个接受者都要对直接执行的任务可以进行回滚。
		public void roolBack(){
			//根据日志进行回滚
		}
		
}
