package nine.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-13
 * 描述：美工组实现类
 * 说明：命令模式下的三个项目组
 */
public class PageGroup extends Group{

	@Override
	public void find() {
		System.out.println("客户找到美工组。。。。");
	}

	@Override
	public void add() {
		System.out.println("客户要求添加界面。。。。");
	}

	@Override
	public void update() {
		System.out.println("客户要求更改界面。。。。");
	}

	@Override
	public void delete() {
		System.out.println("客户要求删除界面。。。。。");
	}

	@Override
	public void plan() {
		System.out.println("客户提交计划变更。。。。。");
	}

}
