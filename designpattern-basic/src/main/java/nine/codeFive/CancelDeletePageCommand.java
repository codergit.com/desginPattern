package nine.codeFive;

/**
 * 
 * @author fcs
 * @date 2014-8-14
 * 描述：撤销删除命令
 * 说明：然后就是invoker进行调用，
 */
public class CancelDeletePageCommand extends Command{

	//撤销删除一个页面的命令
	@Override
	public void execute() {
		super.pageGroup.roolBack();
	}

}
