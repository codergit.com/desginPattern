package six.codeTwo;

import com.design.six.codeOne.SchoolReport;

/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：老爸查看修饰后的成绩单
 * 说明：通过两层继承对成绩单进行修饰，但是很多情况是不能多重继承的，否则维护成本会很大
 * 而且代码非常臃肿
 * 
 * 所以在面向对象的设计中，如果超过两层继承，就应该想想是不是出设计问题了。
 */
public class Father {
	public static void main(String[] args) {
		SchoolReport sr = new SugarFourthGradeSchoolReport();
		sr.report(); //看成绩单
		
		sr.sign("老三");  //老爸签名
	}
}	
