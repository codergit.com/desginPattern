package six.codeTwo;

import com.design.six.codeOne.FourthGradeSchoolReport;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：修饰成绩单，
 * 说明：继承fouthschoolReport
 *
 */
public class SugarFourthGradeSchoolReport extends FourthGradeSchoolReport {
    //修饰最高成绩
	private void reportHighScore(){   
		System.out.println("这次语文最高是  75 ，数学是78  自然是 80");
	}
	//先汇报成绩单，再汇报排名情况
	private void reprotSort(){
		System.out.println("我是排名第38名。。。。");
	}
	@Override
	public void report(){
		this.reportHighScore();    //先说最高成绩
		super.report();      //然后再看成绩单
		this.reprotSort();   //最后看排名
	}
}
