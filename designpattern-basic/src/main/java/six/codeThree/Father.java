package six.codeThree;

import com.design.six.codeOne.FourthGradeSchoolReport;
import com.design.six.codeOne.SchoolReport;

/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：老爸查看修饰后的成绩单
 * 改了一点，实现了，因为有两个装饰的类，所以应该生成两个装饰的类的对象
 */
public class Father {
	public static void main(String[] args) {
		//把成绩单拿过来
		SchoolReport sr;
		Decorator    sr1;
		Decorator    sr2;
		//原装的成绩单
		sr = new FourthGradeSchoolReport();
		//加了最高分说明的成绩单
	    sr1 = new HighScoreDecorator(sr);
	    //加了排名的成绩单
	    sr2 = new SortDecorator(sr);
	    
	    sr1.report();
	    sr2.sign("老三");
		
	}
}
