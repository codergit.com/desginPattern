package six.codeThree;

import com.design.six.codeOne.SchoolReport;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：最高成绩修饰
 * 说明：先调用具体的装饰类的装饰方法，reportHighScore,然后再调用
 * 具体构建的方法
 */
public class HighScoreDecorator extends Decorator{

	public HighScoreDecorator(SchoolReport sr) {
		super(sr);
		
	}
	//汇报最高成绩
	public void reportHighScore(){
		System.out.println(" 这次考试语文最高是74 数学是78 自然是 80  ");
	}
	//先说最高成绩
	@Override
	public void report(){
		this.reportHighScore();
		super.report();
	}
}
