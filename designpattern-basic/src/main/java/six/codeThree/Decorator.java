package six.codeThree;

import com.design.six.codeOne.SchoolReport;

/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：修饰的抽象类（装饰模式的核心类） 
 * 作用：相当于一个特殊的代理类，真实的执执行者还是被代理的角色、FouthgradeSchoolReport
 * 该类的作用是： 封装SchoolReport类
 * 
 * 说明：装饰类要把动作的执行者委托给需要装饰的对象，
 * 该类的意义：让子类来封装SchoolReport的子类。重写report方法
 * 
 *
 */
public  abstract class Decorator {
	//是哪一个成绩单（相当于封装SchoolReport ）
	private SchoolReport sr ;
	//构造函数，传递成绩
	public Decorator(SchoolReport sr){
		this.sr = sr;
	}
	//成绩单还是要被看到的
	public void report(){
		this.sr.report();
	}
	//家长签名
	public void sign(String name){
		this.sr.sign(name);
	}

	


}
