package six.codeThree;

import com.design.six.codeOne.SchoolReport;

/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：排名情况修饰
 *
 */
public class SortDecorator  extends Decorator{

	public SortDecorator(SchoolReport sr) {
		super(sr);
	}
	//告诉排名
	private void reportSort(){
		System.out.println("我是排名第  38 。。。。");
	}
	
	//先看成绩单再说排名
	@Override
	public void report(){
		super.report();
		this.reportSort();
	}
}
