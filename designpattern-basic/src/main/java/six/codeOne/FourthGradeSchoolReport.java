package six.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：四年级成绩单
 *
 */
public class FourthGradeSchoolReport  extends SchoolReport{
	//具体成绩单
	@Override
	public void report() {
		System.out.println("尊敬的  ＸＸＸ家长：");
		System.out.println("。。。。。。");
		System.out.println("语文 62 数学 65 体育  98 自然 63");
		System.out.println(".........");
		System.out.println("家长签名： ");
	}
	//家长签名
	@Override
	public void sign(String name) {
		System.out.println("家长签名： "+name);
	}
	
}
