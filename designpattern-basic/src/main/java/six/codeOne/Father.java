package six.codeOne;
//老爸查看成绩单
/**
 * 说明这是最基本的成绩单，但是不令人满意
 * 因此需要修饰一下
 * 1.汇报最高成绩
 * 2.汇报排名情况
 */
public class Father {
	public static void main(String[] args) {
		SchoolReport sr = new FourthGradeSchoolReport();
		//查看成绩单
		sr.report();
	}
	
}
