package six.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：抽象成绩单
 *
 */
public abstract  class SchoolReport {
		//成绩报告
	public abstract void report();
		//家长签字
	public abstract void sign(String name);
	
}
