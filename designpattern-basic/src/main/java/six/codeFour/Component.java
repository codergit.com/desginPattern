package six.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：抽象构件类
 * 该包描述类图中的构件和角色
 */
public  abstract class Component {
	//抽象方法
	public abstract void operate();
}
