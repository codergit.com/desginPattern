package six.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：具体的装饰类
 *
 */
public class ConcreteDecorator1 extends Decorator{

	public ConcreteDecorator1(Component _component) {
		super(_component);
	}
	//定义自己的修饰方法
	private void method1(){
		System.out.println("method1 修饰");
	}
	//重写父类的operate方法
	@Override
	public void operate(){
		this.method1();
		super.operate();
	}
}
