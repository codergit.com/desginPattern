package six.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：装饰角色-----通常是一个抽象类
 *
 */
public class Decorator extends Component{
	private Component  component = null;
	public Decorator(Component _component){
		this.component = _component;
	}
	@Override
	public void operate() {
		this.component.operate();
	}

}
