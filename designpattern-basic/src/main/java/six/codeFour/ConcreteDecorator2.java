package six.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：具体的装饰角色二
 * 说明：如果只有一个装饰类，则可以没有抽象装饰角色。直接实现具体的装饰角色即可
 * 注意： 原始方法和装饰方法的执行顺序在具体的装饰类是固定的，可以通过方法重载实现
 * 多种执行顺序。
 */
public class ConcreteDecorator2 extends Decorator {

	//定义被修饰者
	public ConcreteDecorator2(Component _component) {
		super(_component);
	}
	//自定义修饰方法
	private void method2(){
		System.out.println("method 2修饰");
	}
	//重写父类的operate的方法
	public void operate(){
		super.operate();
		this.method2();
	}
}
