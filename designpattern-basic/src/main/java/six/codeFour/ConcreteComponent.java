package six.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-9
 * 描述：具体构件类
 * Component：组成的，构成的，成分 ，组件
 * Concrete：具体的，实在的
 */
public class ConcreteComponent extends Component{

	@Override
	public void operate() {
		System.out.println("do something ....");
	}

}
