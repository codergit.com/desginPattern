package four.codeTwo;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：多例模式演示
 *
 */
public class Emperor {
	//定义最多能产生的实例数量
	private static int maxNumEmp = 02;
	//皇帝姓名属性用ArrayList容纳。
	private static ArrayList<String> nameList = new ArrayList<String>();
	//定义一个列表，容纳所有的皇帝实例
	private static ArrayList<Emperor> emperorList = new ArrayList<Emperor>();
	//当前皇帝的序列号
	private static int countNumEmp = 0;
	//产生所有对象
	static{
		for(int i =0 ;i<maxNumEmp;i++){
			emperorList.add(new Emperor("皇帝-->"+(i+1)));
		}
	}
	
	private Emperor(){
		//不允许有第二个皇帝
		
	}
	//传入一个皇帝名称，建立皇帝对象
	private Emperor(String name){
		nameList.add(name);
	}
	//随机拉出一个皇帝
	public static Emperor getInstance(){
		Random random = new Random();
		countNumEmp =  random.nextInt(maxNumEmp);
		return emperorList.get(countNumEmp);
		
	}
	public static void say(){
		System.out.println("我是： "+nameList.get(countNumEmp));
	}
	
	
}
