package four.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：多例模式演示场景类
 *
 */
public class Minister {
	public static void main(String[] args) {
		int ministerNum = 5;
		for (int i = 0; i < ministerNum; i++) {
			Emperor emperor = Emperor.getInstance();
			System.out.println("第"+(i+1)+"个大臣参拜的是：");
			emperor.say();
		}
	}
}
