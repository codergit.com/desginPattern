package four.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-4
 * 描述：线程不安全的单例类
 * 说明；该代码在低并发的情况下不会出现什么问题，在高并发的情形下
 * 多个线程可能会产生多个单例对象。
 * 解决方法：在getSingleton()方法里加synchronized代码块
 */
public class Singleton02 {
	private static Singleton02 singleton = null;
	//限制产生多个对象
	private Singleton02(){}
	//通过该方法获得实例对象
	public static Singleton02 getSingleton(){
		if(singleton == null){
			singleton = new Singleton02();
		}
		return singleton;
	
	}
}
