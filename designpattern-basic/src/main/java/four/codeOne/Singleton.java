package four.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-3
 * 描述：单利模式的通用代码
 * 说明：Singleton类称为单例类，通过使用private 的构造函数确保了在图个应用
 * 中指产生一个并且是自行实例化的。
 * 该模式称为：饿汉式单例，防止线程出现不同步的问题。
 */
public class Singleton {
	private static final Singleton single = new Singleton();
	private Singleton(){
		
	}
	public  static Singleton getSingleton(){
		return single;
	}
	//类中的其他方法，尽量是static
	public static void goSomething(){
		
	}
	
}
