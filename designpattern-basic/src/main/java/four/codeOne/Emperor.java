package four.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-3
 * 描述：单例模式：皇帝只能有一个，一个类只能有一个对象
 * 说明：通过定义一个私有访问权限的构造啊后脑勺，避免被其他类new出来一个
 * 对象，而Emperor自己则可以new出来一个对象，其他类对该类的访问，可以通过getInstance获得
 * 同一个对象
 */

public class Emperor {
	//初始化一个皇帝
  private static final Emperor emperor = new Emperor();
  private Emperor (){
	  
  }
  public static Emperor getInstance(){
	  return emperor;
  }
  public void say(){
	  System.out.println("我是秦始皇。。。。");
  }
}
