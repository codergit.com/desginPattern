package five.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：场景类
 *
 */
public class Client {
	public static void main(String[] args) {
		//原有的业务逻辑
		Target target = new ConcreteTarget();
		target.request();
		
		//增加适配器角色后的业务逻辑
		Target target2 = new Adapter();
		
		target2.request();
		
		
		
	}
}
