package five.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：目标角色
 * 说明：通常情况下目标角色是一个接口或者是抽象类
 * 一般不会是一个实现类
 */
public interface Target {
	//目标角色有自己的方法
	public void request();
}
