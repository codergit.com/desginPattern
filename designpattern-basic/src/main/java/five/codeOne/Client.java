package five.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：场景类
 * 说明：通过较小的修改得到最好的业务处理方式.
 */
public class Client {
	public static void main(String[] args) {
		//没有与外系统连接的时候，是这样写的,查看自己员工的信息
		IUserInfo youngGirl = new UserInfo();
		//模拟从数据库查找
		for(int  i=0;i<101;i++){
			youngGirl.getMobileNumber();
			
		}
		
		//查找其他外包员工的信息
		IUserInfo youngOutGirl = new OuterUserInfo();
		for(int i = 0 ;i < 100;i++){
			youngOutGirl.getMobileNumber();
		}
		
	}
}
