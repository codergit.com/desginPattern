package five.codeOne;

import java.util.HashMap;
import java.util.Map;

public class OuterUser implements IOuterUser{

	/**
	 * 员工的基本信息
	 */
	public Map getUserBaseInfo() {
		HashMap baseInfoMap = new HashMap();
		
		baseInfoMap.put("userName", "这个员工是混世魔王");
		
		baseInfoMap.put("mobileNumber", "这个员工的电话号码是 ：   ");
		return baseInfoMap;
		
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.design.five.codeOne.IOuterUser#getUserOfficeInfo()
	 * 员工的家庭信息
	 */
	public Map getUserOfficeInfo() {
		HashMap officeInfo = new HashMap();
		officeInfo.put("jobposition","这个人的职位是boss...");
		officeInfo.put("officeTelNumber", "员工的办公电话");
		return officeInfo;
	}
	/**
	 * 员工的工作信息
	 */
	public Map getUserHomeInfo() {
		
		HashMap  homeInfo = new HashMap();
		homeInfo.put("homeTelNumber","员工的家庭电话是。。。");
		homeInfo.put("homeAddress", "员工的家庭地址是 。。。");
		return homeInfo;
		
		
	}

}
