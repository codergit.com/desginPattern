package five.codeOne;

import java.util.Map;
/**
 * 
 * @author fcs
 * @date 2014-8-5
 * 描述：劳务公司的人员信息接口
 *
 */
public interface IOuterUser {
	//基本信息，比如名称，性别，手机号码等。
	public Map getUserBaseInfo();
	//工作区域信息
	public Map getUserOfficeInfo();
	//用于的家庭信息
	public Map getUserHomeInfo();
	
	
}
