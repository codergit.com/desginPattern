package five.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-5
 * 描述：人力资源的员工管理系统
 *      员工信息接口
 *
 */
public interface IUserInfo {
	  //获得用户名
      public String getUserName();
      //获得家庭地址
      public String getHomeAddress();
      //获得手机号码
      public String getMobileNumber();
      //办公电话
      public String getJobPosition();
      //获得家庭电话
      public String getHomeTelNumber();
      
}
