package five.codeOne;

import java.util.Map;

/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：中转角色
 * 说明:增加中转角色的原因是两个系统要实现交互，因此需要一个中转的类
 * 进行转换处理，先拿到对方的数据对象，然后转化为我们自己的数据对象
 * 要严格尊师依赖倒转原则，和里氏替换原则，否则即使增加了中转类也无法解决问题
 *
 * 可以使用泛型进行转化而不是String强制转化
 */
public class OuterUserInfo extends OuterUser implements IUserInfo{
	private Map baseInfo = super.getUserBaseInfo();  //员工的基本信息
	private Map homeInfo = super.getUserHomeInfo();  //员工的家庭信息
	private Map  officeInfo = super.getUserOfficeInfo();  //工作信息
	

	public String getUserName() {
		String  userName = (String) this.baseInfo.get("userName");
		System.out.println(userName);
		return userName;
	
	
	}

	public String getHomeAddress() {
		String  homeAddress = (String) this.homeInfo.get("homeAddress");
		System.out.println(homeAddress);
		return homeAddress;
	}

	public String getMobileNumber() {
		String  mobileNumber = (String) this.baseInfo.get("mobileNumber");
		System.out.println(mobileNumber);
		return mobileNumber;
	}

	public String getJobPosition() {
		String  jobPostion = (String) this.officeInfo.get("jobPosistion");
		System.out.println(jobPostion);
		return jobPostion;
	}

	public String getHomeTelNumber() {
		String  homeTel = (String) this.homeInfo.get("homeTelNumber");
		System.out.println(homeTel);
		return homeTel;
	}
}
