package five.codeThree;

import java.util.Map;

/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：用户基本信息接口
 * 说明：外包公司接口
 */
public interface IOuterUserBaseInfo {
	public Map getUserBaseInfo();
}
