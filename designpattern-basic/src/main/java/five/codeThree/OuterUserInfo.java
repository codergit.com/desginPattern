package five.codeThree;

import java.util.Map;

import com.design.five.codeOne.IUserInfo;
/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：适配器
 *
 */
public class OuterUserInfo implements IUserInfo{
	//源目标对象
	private IOuterUserBaseInfo baseInfo = null;
	private IOuterUserHomeInfo homeInfo = null;
	private IOuterUserOfficeInfo officeInfo = null;
	//数据处理
	private Map baseMap = null;
	private Map homeMap = null;
	private Map officeMap = null;
	//构造函数传递对象
	public OuterUserInfo(IOuterUserBaseInfo _baseInfo,IOuterUserHomeInfo _homeInfo,IOuterUserOfficeInfo _officeInfo){
		this.baseInfo = _baseInfo;
		this.homeInfo = _homeInfo;
		this.officeInfo = _officeInfo;
		this.baseMap = this.baseInfo.getUserBaseInfo();
		this.homeMap = this.homeInfo.getUserHomeInfo();
		this.officeMap = this.officeInfo.getUserOfficeInfo();
	}
	
	
	public String getUserName() {
		String  userName = (String) this.baseMap.get("userName");
		System.out.println(userName);
		return userName;
	
	
	}

	public String getHomeAddress() {
		String  homeAddress = (String) this.homeMap.get("homeAddress");
		System.out.println(homeAddress);
		return homeAddress;
	}

	public String getMobileNumber() {
		String  mobileNumber = (String) this.baseMap.get("mobileNumber");
		System.out.println(mobileNumber);
		return mobileNumber;
	}

	public String getJobPosition() {
		String  jobPostion = (String) this.officeMap.get("jobPosistion");
		System.out.println(jobPostion);
		return jobPostion;
	}

	public String getHomeTelNumber() {
		String  homeTel = (String) this.homeMap.get("homeTelNumber");
		System.out.println(homeTel);
		return homeTel;
	}

}
