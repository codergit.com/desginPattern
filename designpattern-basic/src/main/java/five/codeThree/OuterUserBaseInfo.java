package five.codeThree;

import java.util.HashMap;
import java.util.Map;
/**
 * 外包公司员工信息
 * @author fcs
 * @date 2014-8-6
 * 描述：用户基本信息实现类
 *
 */
public class OuterUserBaseInfo  implements IOuterUserBaseInfo{

	public Map getUserBaseInfo() {
		HashMap baseInfoMap = new HashMap();
		
		baseInfoMap.put("userName", "这个员工是混世魔王");
		
		baseInfoMap.put("mobileNumber", "这个员工的电话号码是 ：   ");
		return baseInfoMap;
	}

}
