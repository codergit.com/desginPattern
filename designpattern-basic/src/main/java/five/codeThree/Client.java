package five.codeThree;

import com.design.five.codeOne.IUserInfo;


/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：场景类。
 *
 */
public class Client {
	public static void main(String[] args) {
		IOuterUserBaseInfo baseInfo = new OuterUserBaseInfo();
		IOuterUserHomeInfo homeInfo = new OuterUserHomeInfo();
		IOuterUserOfficeInfo officeInfo = new OuterUserOfficeInfo();
	
		IUserInfo youngGirl = new OuterUserInfo(baseInfo,homeInfo,officeInfo);
		//模拟数据库查询
		for(int  i =0;i<100;i++){
			youngGirl.getMobileNumber();
		}
		
		
	}
}
