package five.codeThree;

import java.util.HashMap;
import java.util.Map;
/**
 * 外包公司员工信息
 * @author fcs
 * @date 2014-8-6
 * 描述：用户工作信息实现类
 *
 */
public class OuterUserOfficeInfo implements IOuterUserOfficeInfo{

	public Map getUserOfficeInfo() {
		HashMap officeInfo = new HashMap();
		officeInfo.put("jobposition","这个人的职位是boss...");
		officeInfo.put("officeTelNumber", "员工的办公电话");
		return officeInfo;
	}

}
