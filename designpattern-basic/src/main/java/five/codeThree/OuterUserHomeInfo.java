package five.codeThree;

import java.util.HashMap;
import java.util.Map;
/**
 * 外包公司员工信息
 * @author fcs
 * @date 2014-8-6
 * 描述：用户家庭信息实现类
 *
 */
public class OuterUserHomeInfo implements IOuterUserHomeInfo{

	public Map getUserHomeInfo() {
		HashMap  homeInfo = new HashMap();
		homeInfo.put("homeTelNumber","员工的家庭电话是。。。");
		homeInfo.put("homeAddress", "员工的家庭地址是 。。。");
		return homeInfo;
	}

}
