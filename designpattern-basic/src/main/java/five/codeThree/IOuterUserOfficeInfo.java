package five.codeThree;

import java.util.Map;

/**
 * 
 * @author fcs
 * @date 2014-8-6
 * 描述：用户基本信息接口
 * 说明：外包公司接口
 * 该系统设计的比较合理因为符合单一原则。接口和类要保持职责单一。
 *
 */
public interface IOuterUserOfficeInfo {
	public Map getUserOfficeInfo();
}
