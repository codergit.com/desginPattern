package eight.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：锦囊
 * 说明：对三个具体策略进行封装,相当于策略执行者
 */
public class Context {
	//构造函数，传入锦囊妙计
	private IStrategy strategy;
	public Context(IStrategy strategy){
		this.strategy = strategy;
	}
	//执行策略
	public void operate(){
		this.strategy.operate();
	}
}
