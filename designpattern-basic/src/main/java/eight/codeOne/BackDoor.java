package eight.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：乔国老开后门
 * 说明：实现策略接口
 */
public class BackDoor implements IStrategy{
	public void operate() {
		System.out.println("找乔国老，帮忙，让吴国太给孙权施压");
	}
}
