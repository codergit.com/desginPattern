package eight.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：吴国太开绿灯
 * 说明：实现策略接口
 */
public class GivenGreeLight implements IStrategy{

	public void operate() {
		System.out.println("求吴国太开绿灯，放行。。。。");
	}

}
