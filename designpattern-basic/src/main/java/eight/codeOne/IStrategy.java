package eight.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：妙计接口
 * 说明：策略模式的接口
 */
public interface IStrategy {
	//每个锦囊妙计都是一个可执行的算法
	public void operate();
}
