package eight.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：孙夫人断后
 * 说明：实现策略接口
 */
public class BlockEnemy implements IStrategy{

	public void operate() {
		System.out.println("孙夫人断后，挡住追兵。。。。");
	}
	
}
