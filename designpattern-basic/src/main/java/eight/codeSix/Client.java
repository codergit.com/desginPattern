package eight.codeSix;

import java.util.Scanner;

/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：策略枚举场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int a = s.nextInt();
		int b = s.nextInt();
		String symbol = s.next();
		System.out.println("运行结果为："+a+symbol+b+" = "+ Calculator.ADD.exec(a, b));
	}
}
