package eight.codeSix;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：策略枚举
 * 说明：把原有定义在抽象策略中的方法移植到枚举中，每个枚举成员就是一个具体的策略
 * 也可以用内置类实现，但是内置类比枚举麻烦，代码可读性差。
 *
 */
public enum Calculator {
	//加法运算
	ADD("+"){
		public int exec(int a,int b){
			return a+b;
		}
	},
	//减法运算
	SUB("-"){
		public int exec(int a ,int b){
			return a-b;
		}
	};
	
	
	String value  = "";
	//获得枚举类型的值
	private Calculator(String _value){
		this.value = _value;
	}
	//声明一个抽象函数
	public abstract int exec(int a ,int b );
		
}
