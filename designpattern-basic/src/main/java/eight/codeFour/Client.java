package eight.codeFour;

import java.util.Scanner;


/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int a = s.nextInt();
		int b = s.nextInt();
		String symbol = s.next();
		Calculator cal = new Calculator();
		System.out.println("运行结果： "+a+symbol+b+" = "+cal.exec(a, b, "+"));
		
		
		
	}
}
