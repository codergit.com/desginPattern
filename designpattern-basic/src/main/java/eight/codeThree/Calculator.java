package eight.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：最直接的加法器
 * 说明：
 */
public class Calculator {
	private final static String ADD_SYMBOL = "+";
	private final static String SUB_SYMBOL = "-";
	public int exec(int a ,int b, String symbol){
		int result = 0;
		if(symbol.equals(ADD_SYMBOL)){
			result = this.add(a, b);
		}else if(symbol.equals(SUB_SYMBOL)){
			result = this.sub(a, b);
		}
		return result;
	}
	public int add(int a ,int b){
		return a+b;
	}
	public int  sub(int a ,int b){
		return a - b;
	}
}
