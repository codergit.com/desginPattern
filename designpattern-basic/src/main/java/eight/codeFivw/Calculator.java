package eight.codeFivw;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：引入策略模式的计算器
 * 说明：接口
 */
public interface Calculator {
	public int exec(int a, int b);
}
