package eight.codeFivw;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：上下文
 * 说明：
 */
public class Context {
	private Calculator cal = null;
	public Context(Calculator _cal){
		this.cal = _cal;
	}
	
	public int exec(int a ,int b ,String symbol){
		return this.cal.exec(a, b);
	}
	
}
