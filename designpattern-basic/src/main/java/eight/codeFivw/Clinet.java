package eight.codeFivw;

import java.util.Scanner;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：策略模式的计算器
 * 说明：场景类
 */
public class Clinet {
	private final static String ADD_SYMBOL = "+";
	private final static String SUB_SYMBOL = "-";
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		
		String symbol= sc.next();
	Context  context = null;
	if(symbol.equals(ADD_SYMBOL)){
		context = new Context(new Add());
	}else if(symbol.equals(SUB_SYMBOL)){
		context = new Context(new Sub());
	}
	System.out.println("运行结果为： "+a+symbol+b+" = "+context.exec(a, b, symbol));
	
	
	}
}
