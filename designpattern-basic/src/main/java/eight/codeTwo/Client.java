package eight.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：高层模块
 * 说明：采用了面向对的继承和多态机制
 */
public class Client {
	public static void main(String[] args) {
		//声明一个具体的策略
		Strategy strategy = new ConcreteStrategy1();
		//声明上下文对象
		Context context = new Context(strategy);
		//执行封装后的方法
		context.doAnything();
	
	}
}
