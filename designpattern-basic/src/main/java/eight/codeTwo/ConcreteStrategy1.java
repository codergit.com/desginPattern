package eight.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：具体的策略角色
 * 说明：实现策略接口的方法
 */
public class ConcreteStrategy1 implements Strategy{

	public void doSomething() {
		System.out.println("具体策略1的运算法则");
	}

}
