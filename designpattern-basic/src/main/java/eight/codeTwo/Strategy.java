package eight.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：抽象的策略角色
 * 说明：是一个普通的接口，里面含有抽象的算法。
 */
public interface Strategy {
	//策略模式的运算法则
	public void doSomething();
	
}
