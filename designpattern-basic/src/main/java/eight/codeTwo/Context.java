package eight.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-12
 * 描述：封装角色（策略模式的重点）
 * 说明：借用了代理模式的思路，与代理模式的额差别是策略模式的封装角色和被封装的策略类不用是
 * 同一个接口，如果是同一个接口就成了代理模式
 */
public class Context {
	//抽象策略
   private Strategy strategy = null;
   //构造函数设置具体策略
   public Context(Strategy strategy){
	   this.strategy = strategy;
   }
   //封装后的策略方法
   public void doAnything(){
	   this.strategy.doSomething();
   }
}
