package tweetyOne.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：抽象产品类
 * 说明：
 */
public abstract class Product {
      public abstract void beProducted();
      public abstract void beSelled();
      
}
