package tweetyOne.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：山寨公司生产衣服。
 * 说明：变化对类图来说不会有太大的变化，就是不断的扩展。
 * 增加公司，继承Corp，增加产品类，继承Product，不会对整个应用产生太大的变化，
 * 这就是桥梁模式
 * 
 */
public class Client2 {
	public static void main(String[] args) {
		House house  = new House();
		System.out.println("房地产公司运营。。。。。");
	    HouseCorp houseCorp = new HouseCorp( house);
	    houseCorp.makeMoney();
	    System.out.println("\n");
	    System.out.println("山寨公司运营。。。。。。");
        Ipod ipod = new Ipod();
        ShanZhaiCorp  szCorp = new ShanZhaiCorp(new Cloth());
        szCorp.makeMoney();
	}
}
