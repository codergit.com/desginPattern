package tweetyOne.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：场景类
 * 说明：
 */
public class Client {
     public static void main(String[] args) {
		House house  = new House();
		System.out.println("房地产公司运营。。。。。");
	    HouseCorp houseCorp = new HouseCorp( house);
	    houseCorp.makeMoney();
	    System.out.println("\n");
	    System.out.println("山寨公司运营。。。。。。");
        Ipod ipod = new Ipod();
        ShanZhaiCorp  szCorp = new ShanZhaiCorp(ipod);
        szCorp.makeMoney();
     }
}
