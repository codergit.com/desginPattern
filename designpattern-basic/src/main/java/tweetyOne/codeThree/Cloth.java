package tweetyOne.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：公司重操旧业
 * 说明：继承产品类
 */
public class Cloth extends Product{

	@Override
	public void beProducted() {
		 System.out.println("生产衣服。。。。。");
	}

	@Override
	public void beSelled() {
		System.out.println("销售衣服，，，，，，");
	}

}
