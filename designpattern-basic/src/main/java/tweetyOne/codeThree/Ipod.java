package tweetyOne.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：山寨手机类
 * 说明：继承产品类
 */
public class Ipod extends Product{
	@Override
	public void beProducted() {	
		System.out.println("山寨手机生产。。。。");
	}
	@Override
	public void beSelled() {
		System.out.println("山寨手机销售。。。。。");
	}

}
