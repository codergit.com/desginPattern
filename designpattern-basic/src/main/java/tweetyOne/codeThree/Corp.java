package tweetyOne.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：抽象公司类
 * 说明：
 */
public abstract class Corp {
    //定义一个抽象的产品对象， 
	private Product product;
    //构造函数传递具体的产品
	public Corp(Product product) {
		this.product = product;
	}
	public void makeMoney(){
		this.product.beProducted();
		this.product.beSelled();
	}
     
}
