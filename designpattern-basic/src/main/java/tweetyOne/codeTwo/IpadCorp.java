package tweetyOne.codeTwo;

import com.design.tweetyOne.codeOne.Corp;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：业务扩展，生产山寨搬
 * 说明：
 */
public class IpadCorp extends Corp{

	@Override
	public void produce() {
		System.out.println("生产Ipod.....");
	}
	@Override
	public void sell() {
		System.out.println("销售Ipod畅销。。。。。");
	}
	public void makeMoney(){
		super.makeMoney();
		System.out.println("山寨手机赚钱了。。。。。");
	}
}
