package tweetyOne.codeTwo;

import com.design.tweetyOne.codeOne.HouseCorp;

/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：场景类
 * 说明：产品类和工厂类耦合太大，需要重新设计
 */
public class Client {
    public static void main(String[] args) {
		System.out.println("-----房地产公司的运营------");
	    
		HouseCorp houseCorp = new HouseCorp();
	    houseCorp.makeMoney();
	    System.out.println("\n");
	    System.out.println("-----山寨公司的运营。。。。。");
	    IpadCorp ipodCorp = new IpadCorp();
	    ipodCorp.makeMoney();
	    
	    
	    
    
    }
}
