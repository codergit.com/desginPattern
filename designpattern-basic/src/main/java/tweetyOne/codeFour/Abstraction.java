package tweetyOne.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：抽象化角色
 * 说明：增加构造函数，指明实现者。
 */
public abstract class Abstraction {
     //定义对实现化角色的引用
	private Implementor imp;
	//约束子类必须实现该构造函数 

	public Abstraction(Implementor imp) {
		this.imp = imp;
	}
	//自身的行为和属性
	public void request(){
		this.imp.doAnything();
	}
	//获得实现化角色
	public Implementor getImp(){
		return imp;
	}
}
