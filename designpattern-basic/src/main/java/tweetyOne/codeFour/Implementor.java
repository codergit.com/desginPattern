package tweetyOne.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：实现化角色
 * 说明：
 */
public interface Implementor {
    public void doSomething();
    public void doAnything();
}
