package tweetyOne.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：具体抽象化角色
 * 说明：
 */
public class RefinedAbstraction extends Abstraction {
   //重写构造方法，指明具体实现者
	public RefinedAbstraction(Implementor imp) {
		super(imp);
	}
	//修正父类的行为
	@Override
	public void request(){
		System.out.println("业务处理。。。。。。");
		super.request();
		super.getImp();
		
	}

}
