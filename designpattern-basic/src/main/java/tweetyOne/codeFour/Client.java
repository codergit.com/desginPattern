package tweetyOne.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：场景类
 * 说明：桥梁模式是一个非常简单的模式，只是使用了类间的聚合关系，继承，覆写
 * 等常用功能。提供了一个非常清晰，稳定的架构。
 */
public class Client {
   public static void main(String[] args) {
	    //定义一个实现化角色
	   Implementor imp = new ConcreteImplemor1();
	   //定义一个抽象化角色
	   Abstraction abs = new RefinedAbstraction(imp);
	   //执行
	   abs.request();
	   
	   
   }
}
