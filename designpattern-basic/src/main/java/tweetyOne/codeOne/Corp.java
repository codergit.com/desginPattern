package tweetyOne.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：抽象公司类
 * 说明：
 */
public abstract class Corp {
   //生产
   public abstract void produce();
   //销售
   public abstract void sell();
   //挣钱
   public void makeMoney(){
	   this.produce();
	   this.sell();
   }
}
