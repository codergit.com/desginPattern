package tweetyOne.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：房地产公司
 * 说明：
 */
public class HouseCorp extends Corp {
	@Override
	public void produce() {
		System.out.println("房地产公司盖房子。。。。。");
	}
	@Override
	public void sell() {
		System.out.println("房地产公司出售房子。。。。。");
	}
	//重写父类的方法
	public void makeMoney(){
		super.makeMoney();
		System.out.println("房地产公司赚大钱了。。。。。。");
	}
     
}
