package thirteen.codeTwo;

import java.util.Random;

import com.design.thirteen.codeOne.AdvTemplate;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：修改后的场景类
 * 说明：这种不通过new关键字来产生一个对象，而是通过对象赋值来实现的
 * 模式就叫做原型模式、
 */
public class Client {
	//模拟从数据库中取出数据
	private static int MAX_COUNT = 6;
	public static void main(String[] args) {
		int i = 0;
		Mail mail = new Mail(new AdvTemplate());
		mail.setTail("XX银行版权所有");

		while(i < MAX_COUNT){
			//关键代码处，通过克隆方式，产生对象
			Mail cloneMail = mail.clone();  
			cloneMail.setAppellation(getRandString(5)+" 先生(女士)");
			cloneMail.setReciver(getRandString(5) +"@"+getRandString(8)+".com");
			sendMail(mail);
			i++;
		}
	}
	//发送邮件
	public static void sendMail(Mail mail){
		System.out.println("标志： "+mail.getSubject()+" \t收件人： "+mail.getReciver()+"\t....发送成功");
	}
	public static String getRandString(int maxLength){
		String source = "abcdefghjklmnwqtyuiopzxvABCDEFGHJKLMNWQTYUIOPZXV";
		StringBuffer sb = new StringBuffer();
		Random rand = new Random();
		for(int i = 0;i < maxLength;i++){
			sb.append(source.charAt(rand.nextInt(source.length())));
		}
		return sb.toString();
	}
}
