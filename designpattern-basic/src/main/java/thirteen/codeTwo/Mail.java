package thirteen.codeTwo;

import com.design.thirteen.codeOne.AdvTemplate;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：使用克隆技术的Mail业务对象
 * 说明：实现cloneable接口，重写Clone方法
 */
public class Mail  implements Cloneable{
	//收件人
	private String reciver;
	//邮件名称
	private String subject;
	//邮箱内容
	private String contxt;
	//邮件的尾部，一般都是建设“XXX版权所有”等信息
	private String  tail;
	//称谓
	private String appellation;
	public Mail(AdvTemplate advTem) {
		this.contxt = advTem.getAdvContext();
		this.subject = advTem.getAdvSubject();
	}
	//重写超类的Clone方法，这里是浅克隆
	@Override
	public Mail clone(){
		Mail mail = null;
		try {
		
			mail = (Mail)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return mail;
		
	}
	
	
	public String getReciver() {
		return reciver;
	}
	public void setReciver(String reciver) {
		this.reciver = reciver;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContxt() {
		return contxt;
	}
	public void setContxt(String contxt) {
		this.contxt = contxt;
	}
	public String getTail() {
		return tail;
	}
	public void setTail(String tail) {
		this.tail = tail;
	}
	public String getAppellation() {
		return appellation;
	}
	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}
	
}
