package thirteen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：原型模式通用源码
 * 说明：实现cloneable接口
 */
public class ProtoypeClass  implements Cloneable{
	@Override
	public ProtoypeClass clone(){
		ProtoypeClass prototypeClass = null;
		try {
			prototypeClass  = (ProtoypeClass)super.clone();
		} catch (CloneNotSupportedException e) {
			//异常处理
			e.printStackTrace();
		}
		return prototypeClass;
	}
}
