package thirteen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：Mail邮件类
 * 说明：是一个业务对象
 */
public class Mail {
	//收件人
	private String reciver;
	//邮件名称
	private String subject;
	//邮箱内容
	private String contxt;
	//邮件的尾部，一般都是建设“XXX版权所有”等信息
	private String  tail;
	//称谓
	private String appellation;
	public Mail(AdvTemplate advTem) {
		this.contxt = advTem.getAdvContext();
		this.subject = advTem.getAdvSubject();
	}
	public String getReciver() {
		return reciver;
	}
	public void setReciver(String reciver) {
		this.reciver = reciver;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContxt() {
		return contxt;
	}
	public void setContxt(String contxt) {
		this.contxt = contxt;
	}
	public String getTail() {
		return tail;
	}
	public void setTail(String tail) {
		this.tail = tail;
	}
	public String getAppellation() {
		return appellation;
	}
	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}
	
}
