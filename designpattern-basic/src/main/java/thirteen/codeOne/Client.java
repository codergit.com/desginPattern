package thirteen.codeOne;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：场景类
 * 说明：这是演示的单线程下，进行邮件发送的功能，这种情况下每发送一份邮件会花费大约0.02秒时间，
 * 因此，不能满足业务需求。
 * 解决方法：
 * 1.可以使用多线程。
 * 2.可以使用对象的克隆技术
 */
public class Client {
	//模拟从数据库中取出数据
	private static int MAX_COUNT = 6;
	public static void main(String[] args) {
		int i = 0;
		Mail mail = new Mail(new AdvTemplate());
		mail.setTail("XX银行版权所有");
		while(i < MAX_COUNT){
			mail.setAppellation(getRandString(5)+" 先生(女士)");
			mail.setReciver(getRandString(5) +"@"+getRandString(8)+".com");
			sendMail(mail);
			i++;
		}
	}
	//发送邮件
	public static void sendMail(Mail mail){
		System.out.println("标志： "+mail.getSubject()+" \t收件人： "+mail.getReciver()+"\t....发送成功");
	}
	public static String getRandString(int maxLength){
		String source = "abcdefghjklmnwqtyuiopzxvABCDEFGHJKLMNWQTYUIOPZXV";
		StringBuffer sb = new StringBuffer();
		Random rand = new Random();
		for(int i = 0;i < maxLength;i++){
			sb.append(source.charAt(rand.nextInt(source.length())));
		}
		return sb.toString();
	}
}
