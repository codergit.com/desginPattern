package thirteen.codeFour;

import java.util.ArrayList;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：深拷贝演示
 * 说明：拷贝对象与原来的对象对引用变量或者实例变量各自的修改互不影响，
 * 这种拷贝就是深拷贝。
 * 
 * 注意：深拷贝和浅拷贝建议不要混合使用，特别是在涉及类的继承时，父类有多个引用的情况
 * 就非常负载，建议的方案是深拷贝和浅拷贝分开实现。
 */
public class Thing3 implements Cloneable{
	//定义一个私有变量
	private ArrayList<String> strList = new ArrayList<String>();
	@Override
	public Thing3 clone(){
		
		Thing3 thing  = null;
		try {
			thing = (Thing3)super.clone();
			thing.strList = (ArrayList<String>)this.strList.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return thing;
	}
	//设置HashMap的值
	public void setValue(String value){
		this.strList.add(value);
	}
	//取得ArrayList的值
	public ArrayList<String> getValue(){
		return this.strList;
	}
	
}

