package thirteen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：场景类
 * 说明：进行对象的拷贝
 * 
 * 原理说明：Object类的clone方法的原理是从内存（具体地说就是堆内存）
 * 以二进制流的方式进行拷贝，重新分配一个内存块，所以构造函数没有被执行，
 */
public class Client {
	public static void main(String[] args) {
		//产生一个对象
		Thing thing  = new Thing();
		//拷贝一个对象
		Thing cloneThing = thing.clone();
		
	}
}
