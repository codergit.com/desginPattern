package thirteen.codeFour;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：clone与final的对立
 * 说明：对象的clone与对象内的final关键字是有冲突的，
 * 
 * 增加final关键字的拷贝测试
 * 
 * 结论：要使用clone方法，类的成员变量上不要增加final关键字、
 */
public class Thing4 implements Cloneable{
	//定义一个私有变量，final修饰
	private final ArrayList<String> strList = new ArrayList<String>();
	@Override
	public Thing4 clone(){
		
		Thing4 thing  = null;
		try {
			thing = (Thing4)super.clone();
			//编译不通过，final类型是不能再赋值的！！！！
			//thing.strList = (ArrayList<String>)this.strList.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return thing;
	}
	//设置HashMap的值
	public void setValue(String value){
		this.strList.add(value);
	}
	//取得ArrayList的值
	public ArrayList<String> getValue(){
		return this.strList;
	}
	
}
