package thirteen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：简单的可拷贝的对象
 * 说明：实现cloneable接口
 * 
 */
public class Thing implements Cloneable {
		private String name ;
		//该方法会被默认执行
		public Thing(){
			System.out.println("构造函数被执行了。。。。。");
		}
		//该方法应该显示执行
		public Thing(String name){
			this.name = name;
			System.out.println("name = "+ name);
		}
		@Override
		public Thing clone(){
			Thing thing  = null;
			try {
				thing = (Thing)super.clone();
			} catch (CloneNotSupportedException e) {
					//异常处理
			}
			return thing;
			
		}
}
