package thirteen.codeFour;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：浅拷贝和深拷贝
 * 说明：
 */
public class Thing2 implements Cloneable{
	private ArrayList<String> strList = new ArrayList<String>();
	@Override
	public Thing2 clone(){
		Thing2 thing  = null;
		try {
			thing = (Thing2)super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return thing;
	}
	//设置HashMap的值
	public void setValue(String value){
		this.strList.add(value);
	}
	//取得ArrayList的值
	public ArrayList<String> getValue(){
		return this.strList;
	}
	
}
