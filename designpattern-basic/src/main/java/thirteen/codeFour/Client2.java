package thirteen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-20
 * 描述：	浅拷贝测试
 * 说明：Object类提供的方法clone只是拷贝对象，其对象内部的数组，引用对象等都不拷贝，
 * 还是指向原生对象的内存元素地址，这种拷贝是浅拷贝。
 * 这种方式变量共享，是一种不安全的方式。
 * 
 * 说明：内部的数组和引用对象不会被拷贝，其他的原始类型比如int long char等都会被拷贝，
 * 但是对于String类型，是没有clone方法的，处理机制也必将特殊，
 * 通过字符串池在需要的时候才在内存中创建新的字符串，读者在使用的时候就把String当做基本使用即可。
 * 
 * 
 * 注意：使用原型模式时，引用的成员变量必须满足两个条件才不会被拷贝，一是类的成员变量，
 * 而不是方法内变量，二是必须是一个可变的引用对象，而不是一个而不是一个原始类型，或者不可变类型。
 * 
 */
public class Client2 {
	public static void main(String[] args) {
		Thing2 thing2 = new Thing2();
		thing2.setValue("张三");
		Thing2 thingClone = thing2.clone();
		
		thingClone.setValue("李斯");
		System.out.println(thing2.getValue());
	}
}
