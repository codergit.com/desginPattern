package fourtheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：扩展后的场景类
 * 说明：
 */
public class HummerModel1 extends HummerModel{
	private boolean alarmFlag = true;  //要喇叭响
	@Override
	public void start() {
		System.out.println("悍马h1发动");
	}

	@Override
	public void stop() {
		System.out.println("悍马H1停车。。。");
	}

	@Override
	public void alarm() {
		System.out.println("悍马H1鸣笛。。。。");
	}

	@Override
	public void enginBoom() {
		System.out.println("悍马H2引擎。。。。。");
	}
	//由用户决定要不要喇叭响,
	//该方法由用户操作，影响模板方法的执行结果，称为钩子函数。，
	public void setAlarm(boolean isAlarm){
		this.alarmFlag = isAlarm;
	}

}
