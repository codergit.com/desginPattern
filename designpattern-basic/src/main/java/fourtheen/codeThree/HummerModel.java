package fourtheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：扩展后的抽象模板类
 * 说明：
 */
public abstract class HummerModel {
	//发动
	public abstract void start();
	//停止
	public abstract void stop();
	//鸣笛
	public abstract void alarm();
	//引擎
	public abstract void enginBoom();
	//奔驰
	public void run() {
		this.start();
		this.enginBoom();
		//让他叫就叫，不叫也行。
		if(this.isAlarm()){
			this.alarm();

		}
		this.stop();
	}
	//默认喇叭会响
	public boolean isAlarm(){
		return true;
	}
}
