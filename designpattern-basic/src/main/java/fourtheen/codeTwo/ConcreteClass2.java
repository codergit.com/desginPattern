package fourtheen.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：具体方法
 * 说明：
 */
public class ConcreteClass2 extends AbstractClass{

	//实现基本方法
	@Override
	protected void doSomething() {
		System.out.println("class2   实现基本业务逻辑");
	}
	@Override
	protected void doAnything() {
		System.out.println("class2业务逻辑处理....");
	}

}
