package fourtheen.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：具体模板类
 * 说明：继承抽象模板类
 * 
 */
public class ConcreteClass1 extends AbstractClass{
	//实现基本方法
	@Override
	protected void doSomething() {
		System.out.println("class1   实现基本业务逻辑");
	}
	@Override
	protected void doAnything() {
		System.out.println("class1 业务逻辑处理....");
	}
}
