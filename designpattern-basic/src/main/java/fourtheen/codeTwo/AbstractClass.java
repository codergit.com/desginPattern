package fourtheen.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：抽象模板类
 * 说明：抽象模板中的基本方法尽量设计为protected的类型，符合迪米特法则
 * 不需要暴漏的属性或方法尽量不要设置为public。实现类若非必要，尽量不要扩大父类中的访问权限。
 */
public abstract class AbstractClass {
	//基本方法
	protected abstract void doSomething();
	protected abstract void doAnything();
	//模板方法，该方法使用final修饰，不能被重写。
	public  final void templeteMethod1(){
		this.doAnything();
		this.doSomething();
	}
}
