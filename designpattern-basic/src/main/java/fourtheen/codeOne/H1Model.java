package fourtheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：H1型号的悍马模型
 * 说明：继承HummerModel模型类
 */
public class H1Model extends HummerModel{

	@Override
	public void start() {
		System.out.println("悍马h1发动");
	}

	@Override
	public void stop() {
		System.out.println("悍马H1停车。。。");
	}

	@Override
	public void alarm() {
		System.out.println("悍马H1鸣笛。。。。");
	}

	@Override
	public void enginBoom() {
		System.out.println("悍马H1引擎。。。。。");
	}

//	@Override
//	public void run() {
//		this.start();
//		this.enginBoom();
//		this.alarm();
//		this.run();
//		this.stop();
//	}
	
}
