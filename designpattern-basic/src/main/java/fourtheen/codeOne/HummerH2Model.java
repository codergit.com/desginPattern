package fourtheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：H2型悍马模型
 * 说明：这两个模型类，的run方法是同样的代码，因此应该进行优化。
 * 将run方法提取放入抽象模板类中，称为公共方法。
 *
 */
public class HummerH2Model extends HummerModel{

	@Override
	public void start() {
		System.out.println("悍马h2发动");
	}

	@Override
	public void stop() {
		System.out.println("悍马H2停车。。。");
	}

	@Override
	public void alarm() {
		System.out.println("悍马H2鸣笛。。。。");
	}

	@Override
	public void enginBoom() {
		System.out.println("悍马H2引擎。。。。。");
	}

//	@Override
//	public void run() {
//		this.start();
//		this.enginBoom();
//		this.alarm();
//		this.run();
//		this.stop();
//	}
	

}
