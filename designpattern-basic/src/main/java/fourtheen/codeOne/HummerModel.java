package fourtheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-21
 * 描述：抽象悍马模型
 * 说明：
 */
public abstract  class HummerModel {
	//发动
	public abstract void start();
	//停止
	public abstract void stop();
	//鸣笛
	public abstract void alarm();
	//引擎
	public abstract void enginBoom();
	//奔驰
	public void run() {
		this.start();
		this.enginBoom();
		this.alarm();
		this.stop();
	}
	
}
