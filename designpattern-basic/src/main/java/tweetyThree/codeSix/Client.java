package tweetyThree.codeSix;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：场景类
 * 说明：重载在编译器就决定了要调用哪个方法。它是根据Role 的表面类型而决定调用act（Role role）
 * 方法，这是静态绑定。而Actor的执行方法act则是由其实际类型决定的，这是动态绑定。
 *
 *
 */
public class Client {
   public static void main(String[] args) {
	  //定义一个演员
	   AbsActor actor = new OldActor();
	   //定义一个角色
	   Role role = new KungFuRole();
	   //开始演戏
	   actor.act(role);
	   actor.act(new KungFuRole());    //静态绑定
   }
}
