package tweetyThree.codeSix;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：抽象演员
 * 说明：
 */
public   abstract class AbsActor {
     //演员都能够演一个角色
	public void act(Role role){
		System.out.println("演员可以扮演任何角色。。。。。");
	}
	
	public void act(KungFuRole role){
		System.out.println("演员都可以演功夫角色。。。。。。");
	}
	
	
	
}
