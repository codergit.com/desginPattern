package tweetyThree.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：管理层
 * 说明：
 */
public class Manger  extends Employee {
	//这类人物的职责非常明确，业绩：
    private String performance;
    
	public String getPerformance() {
		return performance;
	}
	public void setPerformance(String performance) {
		this.performance = performance;
	}
	//允许访问者访问
	@Override
	public void accept(IVisiter visitor) {
		visitor.visit(this);
	}

}
