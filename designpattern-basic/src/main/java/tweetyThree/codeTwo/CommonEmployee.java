package tweetyThree.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：普通员工
 * 说明：继承抽象员工类
 */
public class CommonEmployee  extends Employee {
	private String job;
	
	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}


	@Override
	public void accept(IVisiter visitor) {
		 visitor.visit(this);
	}

}
