package tweetyThree.codeTwo;

/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：访问者实现
 * 说明：实现Ivisitor接口
 * 其他几个方法做用为产生报表
 */
public class Visitor  implements IVisiter{
    //访问普通员工
	public void visit(CommonEmployee commonEmployee) {
		System.out.println(this.getCommonEmployee(commonEmployee));
	}

	public void visit(Manger manager) {
		System.out.println(this.getManagerInfo(manager));
	}
	
	//组装出基本信息
	private String getBasicInfo(Employee employee){
		String info = "姓名： "+employee.getName()+"\t";
		info = info+"性别： "+(employee.getSex() == employee.FEMALE ?"女":"男")+"\t";
	    info = info+"薪水： "+employee.getSalary()+"\t";
	    return info;
	}
    //组装部门经理的信息
	private String getManagerInfo(Manger manager){
		String baseicInfo =this.getBasicInfo(manager);
		String otherInfo = "业绩"+manager.getPerformance()+"\t";
		return baseicInfo+otherInfo;
	}
	//组装出普通员工的信息
	private String getCommonEmployee(CommonEmployee commonEmployee){
		String basicInfo = this.getBasicInfo(commonEmployee);
	    String otherInfo = "工作： "+commonEmployee.getJob()+"\t";
	    return basicInfo + otherInfo;
	
	}
	
	
	
	
}
