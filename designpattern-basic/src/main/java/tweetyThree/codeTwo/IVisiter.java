package tweetyThree.codeTwo;


/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：访问者接口
 * 说明：该接口可以访问两个对象，一个是普通员工，一个是高层员工
 */
public interface IVisiter {
    //定义可以访问普通员工
	public void visit(CommonEmployee commonEmployee);
    //定义可以访问管理层
	public void visit(Manger manager);
	

}
