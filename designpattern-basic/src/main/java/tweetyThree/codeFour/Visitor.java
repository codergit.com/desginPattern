package tweetyThree.codeFour;

import com.design.tweetyThree.codeTwo.CommonEmployee;
import com.design.tweetyThree.codeTwo.Manger;



/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：集体访问者
 * 说明：
 */
public class Visitor  implements IVisitor {
     //部门经理的工资系数是5
	private final static int MANAGER_COEFFICIENT = 5;
	//员工的工资系数是2
	private final static int COMMMONEMPLOYEE_COEFFICIENT = 2;
	//普通员工的工资总和
	private int commonTotalSalary = 0;
	//部门经理的工资总和
	private int managerTotalSalary = 0;
	//计算部门经理的工资总和
	private void calTotalSalary(int salary){
		this.managerTotalSalary = this.managerTotalSalary+ salary*MANAGER_COEFFICIENT;
		
	}
	//计算普通员工的工资总和
	private void calCommonSalary (int salary){
	    this.commonTotalSalary = this.commonTotalSalary+ salary*COMMMONEMPLOYEE_COEFFICIENT;
	}
	
	public void visit(CommonEmployee commonEmployee) {
		 this.commonTotalSalary = this.commonTotalSalary+ commonEmployee.getSalary()*COMMMONEMPLOYEE_COEFFICIENT;
	}

	public void visit(Manger manager) {
		this.managerTotalSalary = this.managerTotalSalary + manager.getSalary() * MANAGER_COEFFICIENT;
	}

	public int getTotalSalary() {
		return this.commonTotalSalary+ this.managerTotalSalary;
	}
	

}
