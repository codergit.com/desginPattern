package tweetyThree.codeFour;

import com.design.tweetyThree.codeTwo.CommonEmployee;
import com.design.tweetyThree.codeTwo.Manger;


/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：抽象访问者
 * 说明：
 */
public interface IVisitor {
    //定义访问普通员工
	public void visit(CommonEmployee commonEmployee);
	//定义访问管理层
	public void visit(Manger manager);
    //统计所有员工工资总和
	public int getTotalSalary();
}
