package tweetyThree.codeFour;

import java.util.ArrayList;
import java.util.List;

import com.design.tweetyThree.codeTwo.CommonEmployee;
import com.design.tweetyThree.codeTwo.Employee;
import com.design.tweetyThree.codeTwo.Manger;
import com.design.tweetyThree.codeTwo.Visitor;

public class Client {
	 public static void main(String[] args) {
		 IVisitor visitor = (IVisitor) new Visitor();
		   for (Employee empinfo :mockEmployee() ) {
			   empinfo.accept(new Visitor());
		    } 	
		   System.out.println("本公司的月工资总额是： "+visitor.getTotalSalary());
		  
		}

	 //通过持久层传递数据源
	    public static List<Employee> mockEmployee(){
	    	List<Employee> empList = new ArrayList<Employee>();
	    	CommonEmployee zhangsan = new CommonEmployee();
	    	zhangsan.setJob("编写java程序，绝对的蓝领，苦工加搬运工。。。");
	        zhangsan.setName("张三");
	    	zhangsan.setSalary(1800);
	    	zhangsan.setSex(Employee.MALE);
	    	empList.add(zhangsan);
	    	
	    	CommonEmployee liSi = new CommonEmployee();
	    	liSi.setJob("页面美工，审美素质太不流行了。。。");
	        liSi.setName("李四");
	    	liSi.setSalary(1900);
	    	liSi.setSex(Employee.FEMALE);
	     	empList.add(liSi);
	         Manger wangwu = new Manger();
	         wangwu.setName("王五");
	         wangwu.setPerformance("基本上是负值，但是会拍马屁。。。");
	         wangwu.setSalary(18750);
	         wangwu.setSex(Employee.MALE);
	         empList.add(wangwu);
	         return empList;
	      
	    }
}
