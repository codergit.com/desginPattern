package tweetyThree.codeOne;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：场景类
 * 说明：使用模板方法模式展现，访问者模式的基础
 * 需求变更时，需要修改各个模块，因此进行重构，定义出来一个访问者。由访问者进行访问。
 */
public class Client {
    public static void main(String[] args) {
	   for (Employee empinfo :mockEmployee() ) {
		   empinfo.report();
	    } 	
	}
    //通过持久层传递数据源
    public static List<Employee> mockEmployee(){
    	List<Employee> empList = new ArrayList<Employee>();
    	CommonEmployee zhangsan = new CommonEmployee();
    	zhangsan.setJob("编写java程序，绝对的蓝领，苦工加搬运工。。。");
        zhangsan.setName("张三");
    	zhangsan.setSalary(1800);
    	zhangsan.setSex(Employee.MALE);
    	empList.add(zhangsan);
    	
    	CommonEmployee liSi = new CommonEmployee();
    	liSi.setJob("页面美工，审美素质太不流行了。。。");
        liSi.setName("李四");
    	liSi.setSalary(1900);
    	liSi.setSex(Employee.FEMALE);
     	empList.add(liSi);
         Manger wangwu = new Manger();
         wangwu.setName("王五");
         wangwu.setPerformance("基本上是负值，但是会拍马屁。。。");
         wangwu.setSalary(18750);
         wangwu.setSex(Employee.MALE);
         empList.add(wangwu);
         return empList;
         
         
         
         
    
    
    }
}
