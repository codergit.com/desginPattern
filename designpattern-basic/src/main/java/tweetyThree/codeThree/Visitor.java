package tweetyThree.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：具体访问者
 * 说明：
 */
public class Visitor implements IVisitor {

	public void visit(ConcreteElement1 c1) {
		c1.doSomething();
	}

	public void visit(ConcreteElement2 c2) {
		c2.doSomething();
	}

}
