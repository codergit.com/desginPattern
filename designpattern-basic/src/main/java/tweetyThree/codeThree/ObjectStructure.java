package tweetyThree.codeThree;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：结构对象
 * 说明：对象生成器。使用工厂方法模拟
 */
public class ObjectStructure {
     public static Element createElement(){
    	 Random rand = new Random();
    	 if(rand.nextInt(100) > 50){
    		 return new ConcreteElement1();
    	 }else{
    		 return new ConcreteElement2();
    	 }
     }
}
