package tweetyThree.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：抽象访问者
 * 说明：
 */
public interface IVisitor {
   public void visit(ConcreteElement1 c1);
   public void visit(ConcreteElement2 c2);
}
