package tweetyThree.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：抽象元素
 * 说明：
 */
public abstract class Element {
   //定义业务逻辑
	public abstract void doSomething();
   //允许谁访问
	public abstract void accept(IVisitor visitor);
	
	
}
