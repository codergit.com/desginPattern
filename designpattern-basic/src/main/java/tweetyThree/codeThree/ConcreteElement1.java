package tweetyThree.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：具体元素
 * 说明：
 */
public class ConcreteElement1 extends Element{

	@Override
	public void doSomething() {
		System.out.println("完善业务逻辑。。。");
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

}
