package tweetyThree.codeThree;

public class ConcreteElement2 extends Element{

	@Override
	public void doSomething() {
		System.out.println("业务逻辑处理二2");
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

}
