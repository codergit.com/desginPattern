package tweetyThree.codeSeven;


/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：引入访问者模式
 * 说明：
 */
public interface Role {
  //演员要扮演的角色
	public void accept(AbsActor actor);
}
