package tweetyThree.codeSeven;

/**
 * 
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：场景类
 * 说明：双分派：不管演员类和角色类怎么变化，都可以找到期望的方法运行。
 * 双分派意味着得到执行的操作决定于请求的种类和两个接受者的类型。
 */
public class Client {
    public static void main(String[] args) {
		//定义一个演员
    	AbsActor actor = new OldActor();
    	//定义一个角色
    	Role roles = new KungFuRole();
    	//开始演戏
    	roles.accept(actor);
    	
	}
}
