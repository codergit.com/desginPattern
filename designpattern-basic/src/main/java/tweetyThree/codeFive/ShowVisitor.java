package tweetyThree.codeFive;

import com.design.tweetyThree.codeTwo.CommonEmployee;
import com.design.tweetyThree.codeTwo.Employee;
import com.design.tweetyThree.codeTwo.Manger;

/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：具体展示表
 * 说明：
 */
public class ShowVisitor implements IShowVisitor {
    private String info = "";
	public void visit(CommonEmployee commonEmployee) {
		this.info = this.info + this.getBasicInfo(commonEmployee) +"工作： "+commonEmployee.getJob()+ "\t\n";
	}

	public void visit(Manger manager) {
		this.info = this.info + this.getBasicInfo(manager)+" 业绩： "+manager.getPerformance()+"\t\n";
	}
	
	
	

	public void report() {
		System.out.println(info);
	}
	//组装出基本信息
	private String getBasicInfo(Employee employee){
		String info = "姓名： "+employee.getName()+"\t";
		info = info+"性别： "+(employee.getSex() == employee.FEMALE ?"女":"男")+"\t";
	    info = info+"薪水： "+employee.getSalary()+"\t";
	    return info;
	}
    //组装部门经理的信息
	private String getManagerInfo(Manger manager){
		String baseicInfo =this.getBasicInfo(manager);
		String otherInfo = "业绩"+manager.getPerformance()+"\t";
		return baseicInfo+otherInfo;
	}
	//组装出普通员工的信息
	private String getCommonEmployee(CommonEmployee commonEmployee){
		String basicInfo = this.getBasicInfo(commonEmployee);
	    String otherInfo = "工作： "+commonEmployee.getJob()+"\t";
	    return basicInfo + otherInfo;
	
	}

}
