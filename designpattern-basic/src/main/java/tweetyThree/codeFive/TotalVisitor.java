package tweetyThree.codeFive;

import com.design.tweetyThree.codeTwo.CommonEmployee;
import com.design.tweetyThree.codeTwo.Manger;

/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：具体汇总表
 * 说明：
 */
public class TotalVisitor implements ITotalVisitor {
	 //部门经理的工资系数是5
	private final static int MANAGER_COEFFICIENT = 5;
	//员工的工资系数是2
	private final static int COMMMONEMPLOYEE_COEFFICIENT = 2;
	//普通员工的工资总和
	private int commonTotalSalary = 0;
	//部门经理的工资总和
	private int managerTotalSalary = 0;
	public void visit(CommonEmployee commonEmployee) {
		
	}

	public void visit(Manger manager) {
		
	}

	public int getTotalSalary() {

		return 0;
	}

	public void totalSalary() {
     System.out.println("本公司的月工资总额是："+(this.commonTotalSalary+ this.managerTotalSalary));
 		
	}

}
