package tweetyThree.codeFive;

import java.util.ArrayList;
import java.util.List;

import com.design.tweetyThree.codeTwo.CommonEmployee;
import com.design.tweetyThree.codeTwo.Employee;
import com.design.tweetyThree.codeTwo.Manger;

public class Client {
     public static void main(String[] args) {
		//展示报表访问者
    	 IShowVisitor showVisitor = new ShowVisitor();
    	 ITotalVisitor totalVisitor = new TotalVisitor();
    	 for(Employee emp: mockEmployee()){
    		 emp.accept(showVisitor);  //接收展示报表访问者
    	     emp.accept(totalVisitor);
    	 }
    	 //展示报表
    	 showVisitor.report();
    	 //汇总报表
    	 totalVisitor.totalSalary();
	}
     //通过持久层传递数据源
	    public static List<Employee> mockEmployee(){
	    	List<Employee> empList = new ArrayList<Employee>();
	    	CommonEmployee zhangsan = new CommonEmployee();
	    	zhangsan.setJob("编写java程序，绝对的蓝领，苦工加搬运工。。。");
	        zhangsan.setName("张三");
	    	zhangsan.setSalary(1800);
	    	zhangsan.setSex(Employee.MALE);
	    	empList.add(zhangsan);
	    	CommonEmployee liSi = new CommonEmployee();
	    	liSi.setJob("页面美工，审美素质太不流行了。。。");
	        liSi.setName("李四");
	    	liSi.setSalary(1900);
	    	liSi.setSex(Employee.FEMALE);
	     	empList.add(liSi);
	         Manger wangwu = new Manger();
	         wangwu.setName("王五");
	         wangwu.setPerformance("基本上是负值，但是会拍马屁。。。");
	         wangwu.setSalary(18750);
	         wangwu.setSex(Employee.MALE);
	         empList.add(wangwu);
	         return empList;
	      
	    }
}
