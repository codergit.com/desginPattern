package tweetyThree.codeFive;

import com.design.tweetyThree.codeTwo.IVisiter;


/**
 * 
 * @author fcs
 * @date 2014-9-1
 * 描述：汇总表接口
 * 说明：
 */
public interface ITotalVisitor extends IVisiter {
      //统计所有员工的工资
	public void totalSalary();
}
