package tweentytwo.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：
 * 说明：
 */
public class ConcreteColleague1 extends Colleague{
	//通过构造方法传递中介者
	public ConcreteColleague1(Mediator mediator) {
		super(mediator);
	}
	//自有方法
	public void selfMethod1(){
		System.out.println("处理自己的业务逻辑。。。");
	}
    //依赖方法
	public void depMethod2(){
		System.out.println("处理相关的业务逻辑。。。。。");
		//自己不能处理的业务逻辑，委托给中介者处理。
	   super.mediator.doSomething1();
	
	}
	
	
}
