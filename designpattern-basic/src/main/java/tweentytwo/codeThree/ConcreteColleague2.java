package tweentytwo.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：具体同事类
 * 说明：同事类使用构造函数注入中介者，中介者使用get/set方法注入同事类
 * 这是因为同事类必须有中介者，而中介者却可以只有部分同事类。
 */
public class ConcreteColleague2  extends Colleague{
	//通过构造方法传递中介者
	public ConcreteColleague2(Mediator mediator) {
		super(mediator);
	}
	//自有方法
	public void selfMethod1(){
		System.out.println("处理自己的业务逻辑。。。");
	}
    //依赖方法
	public void depMethod2(){
		System.out.println("处理相关的业务逻辑。。。。。");
		//自己不能处理的业务逻辑，委托给中介者处理。
	   super.mediator.doSomething2();
	
	}
	
	
}

