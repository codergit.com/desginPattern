package tweentytwo.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：抽象同事类
 * 说明：
 */
public class Colleague {
  protected Mediator mediator;
  public Colleague(Mediator mediator) {
	super();
	this.mediator = mediator;
  }
}
