package tweentytwo.codeTwo;

import java.util.Random;

import com.design.tweentytwo.codeOne.Purchase;
import com.design.tweentytwo.codeOne.Stock;

/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：修改后的销售
 * 说明：
 */
public class Sale extends AbstractColleague{

	public Sale(AbstractMeditor mediator) {
		super(mediator);
	}
	//销售电脑
	   public void sellComputer(int number){
		   super.mediator.execute("sale.sell", number);
		  
		   System.out.println("销售电脑： "+number+"台");
		  
	   }
		//反馈销售情况
	   public int getSaleStatus(){
		   Random rand = new Random(System.currentTimeMillis());
	       int saleStatues = rand.nextInt(100);
	       System.out.println("电脑销售情况： "+saleStatues);
	      return saleStatues;
	   }
	   //折价销售处理
	   public void offSale(){
		   //库房存货
		  super.mediator.execute("sale.offsell", null);
	   }
	
	
}
