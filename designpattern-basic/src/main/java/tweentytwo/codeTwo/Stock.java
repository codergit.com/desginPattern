package tweentytwo.codeTwo;

import com.design.tweentytwo.codeOne.Purchase;
import com.design.tweentytwo.codeOne.Sale;

/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：修改后的库存管理
 * 说明：
 */
public class Stock extends AbstractColleague{

	public Stock(AbstractMeditor mediator) {
		super(mediator);
	}
	private static int COMPUTER_NUMBER = 100;
	//库存增加
	public void increase(int number){
		COMPUTER_NUMBER = COMPUTER_NUMBER +	number;
		System.out.println("库存数量为："+COMPUTER_NUMBER);
	}
	//库存减少
	public void decrease(int number){
		COMPUTER_NUMBER = COMPUTER_NUMBER- number;
		System.out.println("库存量为： "+number );
	}
	//获得库存数量
	public int getStockNumber(){
		return COMPUTER_NUMBER;
	}
	//清仓
	public void clearStock(){
	    System.out.println("清理存货数量为： "+COMPUTER_NUMBER);
	    super.mediator.execute("stock.clear", null);
	}
	
}
