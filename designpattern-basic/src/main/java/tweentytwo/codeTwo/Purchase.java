package tweentytwo.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：修改后的采购管理
 * 说明：
 */
public class Purchase extends AbstractColleague {

	public Purchase(AbstractMeditor mediator) {
		super(mediator);
	}
	//采购电脑
	public void buyComputer(int number){
		super.mediator.execute("purchase.buy", number);
	}
	//不再采购电脑
	public void refuseBuy(){
		System.out.println("不再采购电脑。。。。。。。");
	}
	
}
