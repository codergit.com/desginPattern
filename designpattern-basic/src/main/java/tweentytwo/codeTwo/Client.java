package tweentytwo.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：修改后的场景类
 * 说明：增加了中介者，然后分别传递到三个同事类中，只负责处理自己的活动
 * ，其他的由中介者处理
 * 
 * 在多个对象依赖的情况下，通过加入中介者角色，取消了多个对象的关联或者依赖关系，
 * 减少了对象的耦合性，
 * 这就是中介者模式
 * 
 */
public class Client {
     public static void main(String[] args) {
		AbstractMeditor mediator = new Mediator();
		System.out.println("采购电脑。。。。。");
	   Purchase  purchase = new Purchase(mediator);
	   purchase.buyComputer(100);
	   System.out.println("销售电脑。。。。。");
	   Sale sale = new Sale(mediator);
	   sale.sellComputer(1);
	   System.out.println("库房管理人员清仓处理。。。。。");
	   Stock stock = new Stock(mediator);
	   stock.clearStock();
	   
     
     }
}
