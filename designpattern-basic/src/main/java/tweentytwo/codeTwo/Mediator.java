package tweentytwo.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：具体中介者
 * 说明：
 */
public class Mediator  extends AbstractMeditor {
    //中介者中最重要的方法
	@Override
	public void execute(String str, Object... objects) {
		if(str.equals("purchase.buy")){
			this.buyComputer((Integer)objects[0]);
		}else if(str.equals("sale.sell")){
			this.saleComputer((Integer)objects[0]);
		}else if(str.equals("stock.clear")){
			this.clearStock();
		}
	}
	private void buyComputer(int number){
		int saleStatus = super.sale.getSaleStatus();
		if(saleStatus > 80){
			System.out.println("采购电脑："+number+" 台");
			super.stock.increase(number);
		}else{
			int buyNumber = number/2;
			System.out.println("采购电脑： "+buyNumber+" 台");
		}
	}
	private void saleComputer(int number){
		if(super.stock.getStockNumber() < number){
			super.purchase.buyComputer(number);
		}
		super.stock.decrease(number);
	}
	private void offsell(){
		System.out.println("折价销售电脑： "+stock.getStockNumber()+" 台");
	}
    private void clearStock(){
    	super.sale.offSale();
    	super.purchase.refuseBuy();
    }
}
