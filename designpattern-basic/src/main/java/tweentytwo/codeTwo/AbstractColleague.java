package tweentytwo.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：抽象同事类
 * 说明：由三个具体的实现类分别继承该抽象类
 */
public class AbstractColleague {
    protected AbstractMeditor  mediator;

	public AbstractColleague(AbstractMeditor mediator) {
		this.mediator = mediator;
	}
    
}
