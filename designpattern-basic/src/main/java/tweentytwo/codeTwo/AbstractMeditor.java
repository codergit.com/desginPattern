package tweentytwo.codeTwo;

/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：抽象中介者
 * 说明：
 */
public abstract class AbstractMeditor {
   protected Purchase purchase;
   protected Sale sale;
   protected Stock stock;
   public AbstractMeditor(){
	   purchase = new Purchase(this);
	   sale = new Sale(this);
       stock = new Stock(this);
       
   }
	
	//中介者最重要的方法叫做事件方法，处理多个对象之间的关系
   public abstract void execute(String str,Object...objects);
	
}
