package tweentytwo.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：采购管理
 * 说明：
 */
public class Purchase {
   //采购电脑
	public void buyComputer(int number){
		//访问库存
		Stock stock = new Stock();
		//访问销售
		Sale sale = new Sale();
		int saleStatus = sale.getSaleStatus();
		if(saleStatus >80){ //销售情况良好
			System.out.println("采购电脑： "+number+" 台");
		}else{
			int buyNumber = number/2;  //折半采购
			System.out.println("采购电脑： "+buyNumber+"台");
		}
	}
	public void refuseBuyCom(){
		System.out.println("不要采购电脑");
	}		
}
