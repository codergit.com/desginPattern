package tweentytwo.codeOne;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：销售管理
 * 说明：
 */
public class Sale {
	//销售电脑
   public void sellComputer(int number){
	   //访问库存
	   Stock stock = new Stock();
	   //访问采购
	   Purchase purchase = new Purchase();
	   if(stock.getStockNumber() < number){
		   purchase.buyComputer(number);
	   }
	   System.out.println("销售电脑： "+number+"台");
	   stock.decrease(number);
   }
	//反馈销售情况
   public int getSaleStatus(){
	   Random rand = new Random(System.currentTimeMillis());
       int saleStatues = rand.nextInt(100);
       System.out.println("电脑销售情况： "+saleStatues);
      return saleStatues;
   }
   //折价销售处理
   public void offSale(){
	   //库房存货
	   Stock stock = new Stock();
	   System.out.println("折价销售电脑： "+stock .getStockNumber()+"台");
   }
	
	
	
}
