package tweentytwo.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-31
 * 描述：场景类
 * 说明：采购，销售，库存，三个类的耦合关系太密集，并且违反迪米特法则。
 * 应该添加一个中介者，与这三个类进行交流。
 *
 *
 */
public class Client {
      public static void main(String[] args) {
		System.out.println("采购人员采购电脑。。。。。。");
	    Purchase purchase = new Purchase();
	    purchase.buyComputer(100);
	    System.out.println("销售人员销售电脑...........");
	    Sale sale = new Sale();
	    sale.sellComputer(1);
	    //库房管理人员管理库存
	    System.out.println("\n库房管理人员清仓处理");
	    Stock stock = new Stock();
	    stock.clearStock();
      }
}
