package sixtheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：抽象享元角色
 * 说明：一般为抽象类，实际项目中一般是一个实现类，是描述一类
 * 事物的方法，
 * 一般需要把外部状态和内部状态（可无）定义出来，避免子类的随意扩展。
 */
public abstract class Flyweight {
	//内部状态
	private String intrinsic;
	/**
	 * 设置为Final关键字，防止修改池中对象的状态
	 */
	protected final String Extrinsic;
	//要求享元角色必须接受外部状态
	public Flyweight(String extrinsic) {
		super();
		Extrinsic = extrinsic;
	}
	
	//定义业务操作
	public abstract void operate();
	public String getIntrinsic() {
		return intrinsic;
	}
	public void setIntrinsic(String intrinsic) {
		this.intrinsic = intrinsic;
	}
	
}
