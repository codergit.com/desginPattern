package sixtheen.codeThree;

public class ConcreteFlyweight extends Flyweight {
	//接收外部状态
	public ConcreteFlyweight(String extrinsic) {
		super(extrinsic);
	}

	//根据外部状态进行逻辑处理
	@Override
	public void operate() {
      
		System.out.println("业务逻辑。。。。。。");
	}

}
