package sixtheen.codeThree;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：享元工厂
 * 说明：
 */
public class FlyweightFactory {
	//对象池
    private static HashMap<String ,Flyweight> pool = new HashMap<String,Flyweight>();
    //工厂
    public static Flyweight getFlyweight(String Extrinsic){
    	Flyweight flyweight = null;
    	//对象池中没有该对象
    	if(pool.containsKey(Extrinsic)){
    		flyweight  = pool.get(Extrinsic);
    	}
    	
    	else{
    		flyweight = new ConcreteFlyweight(Extrinsic);
            pool.put(Extrinsic, flyweight);    	
    	}
    	return flyweight;
       }
}
