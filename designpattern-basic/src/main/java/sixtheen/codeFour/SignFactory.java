package sixtheen.codeFour;

import java.util.HashMap;

import com.design.sixtheen.codeOne.SignInfo;
import com.design.sixtheen.codeTwo.SignInfo4Pool;

/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：报考信息工厂类
 * 说明：
 */
public class SignFactory {
	//池容器
	private static HashMap<String,SignInfo> pool = new HashMap<String,SignInfo>();
	public static SignInfo getSignInfo(String key){
		SignInfo result = null;
		if(! pool.containsKey(key)){
			System.out.println(key+"------建立对象，并放到池中。。。。。");
			result= new SignInfo();
		    pool.put(key, result);
		
		}else{
			result = pool.get(key);
			System.out.println(key+"------直接从池中获得。。。");
		}
		return result;
	}
	
}
