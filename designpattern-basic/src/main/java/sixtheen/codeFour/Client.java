package sixtheen.codeFour;

import com.design.sixtheen.codeOne.SignInfo;
import com.design.sixtheen.codeTwo.SignInfoFactory;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：线程不安全的演示
 * 说明：设置的对象太少，每个线程都到对象池中获得对象，然后修改其属性。就
 * 会出现不和谐的数据。
 * 
 * 说明：要依靠经验在需要的时候考虑一下线程安全的问题，在大部分场景中都不用考虑
 * 使用享元模式时，对象池中的享元对象尽量多，多到足够满足业务为止。
 * 
 * 
 * 
 */

public class Client {
	public static void main(String[] args) {
		//在池中初始化四个对象
		SignInfoFactory.getSignInfo("科目1");
		SignInfoFactory.getSignInfo("科目2");

		SignInfoFactory.getSignInfo("科目3");
		SignInfoFactory.getSignInfo("科目4");

		SignInfo signInfo = SignInfoFactory.getSignInfo("科目2");
		while(true){
			signInfo.setId("zhangSan");
			signInfo.setLocation("zhangsan");
			(new MultiThread(signInfo)).start();
			signInfo.setId("LiSi");
			signInfo.setLocation("LiSi");
			(new MultiThread(signInfo)).start();
			
		}
	}
}
