package sixtheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：报考信息
 * 说明：是一个简单的ＰＯＪＯ对象
 */
public class SignInfo {
	private String id;    //人员 id
	private String location; //考试地点
	private String subject; //科目
	private String postAddress; //邮寄地址
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPostAddress() {
		return postAddress;
	}
	public void setPostAddress(String postAddress) {
		this.postAddress = postAddress;
	}
}
