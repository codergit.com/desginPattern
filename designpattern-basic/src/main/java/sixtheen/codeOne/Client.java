package sixtheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：场景类模拟表现层
 * 说明：这种无限制的创建对象将会造成聂存溢出
 * 可以采用对象池技术
 * 
 */
public class Client {
	public static void main(String[] args) {
		//从工厂中获得一个对象
		SignInfo signInfo = SignInfoFactory.getSignInfo();
		//业务处理
	}
}
