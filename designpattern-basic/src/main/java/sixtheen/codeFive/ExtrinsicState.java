package sixtheen.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：外部状态
 * 说明：性能平衡问题演示
 * 
 * 注意：一定要重写equals和hashCode方法，否则HashMap中的key值
 * 就没有意义了，只有hashCode值相等，并且equals返回的结果为TRUE，
 * 两个对象才能相等
 * 
 * 
 * 
 */
public class ExtrinsicState {
	private String subject;
	private String location;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Override
	public int hashCode() {
		return subject.hashCode() + location.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ExtrinsicState ){
			ExtrinsicState state = (ExtrinsicState)obj;
			return state.getLocation().equals(location) && state.getSubject().equals(subject);
			
		}
		return false;
	}
	
}
