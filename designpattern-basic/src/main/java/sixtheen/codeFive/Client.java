package sixtheen.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：性能平衡问题场景类
 * 说明：以ExtrinsicState类作为外部状态
 * 执行时间是78ms
 */
public class Client {
    public static void main(String[] args) {
		//初始化对象池
    	ExtrinsicState statel = new ExtrinsicState();
    	statel.setLocation("上海");
    	statel.setSubject("科目一");
    	SignInfoFactory.getSignInfo(statel);
    	ExtrinsicState state2 = new ExtrinsicState();
    	state2.setLocation("上海");
    	state2.setSubject("科目一");
    	//计算执行100万次需要的时间
    	long currentTime = System.currentTimeMillis();
    	for(int i = 0;i < 1000000;i++ )
    	{
    		
    		SignInfoFactory.getSignInfo(state2);
    		
    	}
    	long  tailTime = System.currentTimeMillis();
    	System.out.println("执行时间： "+(tailTime - currentTime)+" ms");
   	}
}
