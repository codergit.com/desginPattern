package sixtheen.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：享元模式在API中的应用
 * 说明：String类中的intern方法，：如果是String的对象池中有该类型的值，
 * 则直接返回对象池中的对象。
 * 使用的就是共享技术
 */
public class Test {
	public static void main(String[] args) {
		String str1 = "和谐";
		String str2 = "社会";
		String str3 = "和谐社会";
		String str4;
		str4 = str1+ str2;
		System.out.println(str3 == str4);
		str4 = (str1+ str2).intern();
		System.out.println(str3 == str4);
		
	}
}	
