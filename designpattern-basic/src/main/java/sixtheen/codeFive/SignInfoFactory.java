package sixtheen.codeFive;

import java.util.HashMap;

import com.design.sixtheen.codeOne.SignInfo;

/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：享元工厂
 * 说明：
 */
public class SignInfoFactory {
     //池容器
	private static HashMap<ExtrinsicState,SignInfo> pool = new HashMap<ExtrinsicState,SignInfo>();
	private static HashMap<String,SignInfo> pool1 = new HashMap<String,SignInfo>();

	public static SignInfo getSignInfo(ExtrinsicState key){
		SignInfo result = null;
		if(!pool.containsKey(key)){
			result = new SignInfo();
			pool.put(key, result);
		}else{
			result = pool.get(key);
		}
		return result;
	}

	public static SignInfo getSignInfo(String key) {
		SignInfo result = null;
		if(! pool1.containsKey(key)){
			result= new SignInfo();
		    pool1.put(key, result);
		
		}else{
			result = pool1.get(key);
		}
		return result;
	}
	
	
	
	
	
	
	
}
