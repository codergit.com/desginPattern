package sixtheen.codeTwo;

import com.design.sixtheen.codeOne.SignInfo;

/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：带对象池的报考信息 
 * 
 * 说明：
 * 设计一个共享对象池，需要实现两个功能
 * 1.容器定义： 在这个容器中容纳对象
 * 2.提供客户端访问的接口
 * 池中有对象时，可以直接从池中获得，否则建立一个新的对象，并防放置到池中，
 *  
 *  增加一个key值，继承SignInfo类，
 *  把这些共性提取出来作为所有对象的外部状态，在这个对象池中
 *  一个具体的外部状态只有一个对象。
 *  key: 考试科目+考试地点的复合字符串作为唯一的池对象标准。
 *  
 *  注意：在对象池中，对象一旦产生，必然有一个唯一的，可访问的状态标志该对象，
 *  而且池中的对象声明周期是由池容器决定，而不是由使用者决定。
 *  
 *
 *
 */
public class SignInfo4Pool extends SignInfo {
	private String key;

	public SignInfo4Pool(String _key) {
		this.key = _key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
