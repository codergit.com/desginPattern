package sixtheen.codeTwo;

import com.design.sixtheen.codeOne.SignInfo;

/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：场景类
 * 说明：享元模式的实现场景
 */
public class Client {
	public static void main(String[] args) {
		//初始化对象池
		for (int i = 0; i < 4; i++) {
			String subject = "科目"+i;
			for(int  j =0 ;j < 30;j++){
				String key = subject + "考试地点"+j;
				SignInfoFactory.getSignInfo(key);
			}
		}
		SignInfo signInfo = SignInfoFactory.getSignInfo("科目1考试地点1");
	}
}
