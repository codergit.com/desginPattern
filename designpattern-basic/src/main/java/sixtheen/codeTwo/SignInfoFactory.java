package sixtheen.codeTwo;

import java.util.HashMap;

import com.design.sixtheen.codeOne.SignInfo;

/**
 * 
 * @author fcs
 * @date 2014-8-24
 * 描述：带对象池的工厂类
 * 说明：注意不要有删除原始代码的念头，特别是在产品级的开发中
 */
public class SignInfoFactory {
	//池容器
	private static HashMap<String,SignInfo> pool = new HashMap<String,SignInfo>();
	 //报名信息的对象工厂
	  @Deprecated
	  public static SignInfo getSignInfo(){
		  return new SignInfo();
	  }
	public static SignInfo getSignInfo(String key){
		SignInfo result = null;
		if(! pool.containsKey(key)){
			System.out.println(key+"------建立对象，并放到池中。。。。。");
			result= new SignInfo4Pool(key);
		    pool.put(key, result);
		
		}else{
			result = pool.get(key);
			System.out.println(key+"------直接从池中获得。。。");
		}
		return result;
	}
	
}
