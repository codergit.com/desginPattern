package ten.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：具体处理者
 * 说明：在处理者中涉及三个类，level类负责定义请求和处理级别，request
 * 类负责封装请求，response负责封装链中返回的结果。
 * 这三个类都需要根据业务产生，读者可以在实际应用中完成相关业务填充，
 */
public class ConcreteHandler extends Handler {
	//定义自己的处理逻辑
	@Override
	public Response echo(Request request) {
		//完成处理逻辑
		return null;
	}
	//设置自己的处理级别
	@Override
	public Level getHandlerLevel() {
		return null;
	}

}
