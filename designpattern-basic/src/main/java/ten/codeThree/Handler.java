package ten.codeThree;

import com.design.ten.codeOne.IWoman;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：责任链模式的通用类图
 * 说明：抽象的处理者实现有三个职责：一是定义一个请求的处理方法handlerMessage.
 * 唯一对外开放的方法，二是定义一个连的编排方法setNext(),设置下一个处理者，
 * 三是定义了具体的请求者必先实现的两个方法，定义自己能够处理的级别getHandlerLevel
 * 和具体的处理任务echo;
 * 
 * 注意：在责任链模式中一个请求发送到链中后，前一节点消费部分消息，然后交由后续节点继续处理。
 * 最终可以有处理结果也可以没有处理结果，
 * 
 * 
 */
public abstract class Handler {

	private Handler nexthandler;


	public final Response handleMessage(Request request){
		Response response = null;
		if(this.getHandlerLevel().equals(request.getRequestLevel())){
			response = this.echo(request);
		}else{
			//不属于自己处理的级别
			//判断是否有下一个处理者
			if(this.nexthandler != null){
				//有后续环节才把请求往后递送。
				this.nexthandler.handleMessage(request);
			}else{
				//根据具体业务进行处理
				System.out.println("没地方请示了，按不同意处理。。。。。。");
			}
		}
		return response;
		
	}
	/**
	 * 
	 * 作者：fcs
	 * 描述：如果不属于你处理的请求，你应该让她找下一个环节的人，
	 * 时间：2014-8-15
	 */
	public void setNext(Handler _handler){
		this.nexthandler = _handler;
	}
	//有请求有回应
	public abstract Response echo(Request request);
	public abstract Level getHandlerLevel();
}
