package ten.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：具体处理者
 * 说明：
 */
public class ConcreteHandler1 extends Handler {
	//定义自己的处理逻辑
	@Override
	public Response echo(Request request) {
		//完成处理逻辑
		return null;
	}
	//设置自己的处理级别
	@Override
	public Level getHandlerLevel() {
		return null;
	}

}
