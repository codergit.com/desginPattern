package ten.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：责任链模式类图场景类
 * 说明：在场景类中或者高层模块中对链进行组装，并传递请求，返回结果
 * 在实际应用中，右边会有一个封装类对责任模式进行封装，也就是替代Client类。
 * 直接返回链中第一个处理者，集体链的设置不需要高层次的模块关系。
 * 这样简化高层次模块的调用，减少模块间的耦合，提高系统的灵活性。
 *
 *
 */
public class Client {
	public static void main(String[] args) {
		//声明所有的处理节点
		Handler handler1 = new ConcreteHandler();
		Handler handler2 = new ConcreteHandler1();
		Handler handler3 = new ConcreteHandler2();
		//设置链中的阶段顺序1--->2--->3
		handler1.setNext(handler2);
		handler2.setNext(handler3);
		Response response = handler1.handleMessage(new Request());
		;
	}
}
