package ten.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：有处理权的人员接口
 * 说明：分别有三个实现类，在女儿没有出嫁之前父亲有决定权
 * 出嫁之后夫君有决定权。
 * 夫君死后，夫君的其他男性亲戚有决定权
 */
public interface IHandler {
	//一个女孩，妇女，母亲要求逛街，你的处理
	public void handler(IWoman women);
}
