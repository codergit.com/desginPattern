package ten.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：在女性丧偶后，对目前提出的请求儿子有决定权
 * 说明：实现请求处理类
 * 三个实现类，分别处理女儿，妻子，母亲提出的请求。
 */
public class Son implements IHandler{

	public void handler(IWoman women) {
		System.out.println("母亲的请求是： "+women.getRequest());
		System.out.println("儿子的回答是：可以");
		
	}

}
