package ten.codeOne;

import java.util.ArrayList;
import java.util.Random;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：古代女子三从四德的表现
 * 说明：责任链模式的第一个低级版本
 * 出现以下问题：
 * 1.职责界定不清晰
 * 2.代码臃肿
 * 3.耦合过重。违反开闭原则
 * 4.异常情况欠缺考虑
 */
public class Client {
	public static void main(String[] args) {
		Random rend = new Random();
		ArrayList<IWoman> arrayList = new ArrayList();
		for(int i =0 ;i< 5;i++){
			arrayList.add(new Women(rend.nextInt(4),"我要出去逛街。。。"));
			
			
		}
		//定义三个请示对象
		IHandler father = new Father();
		IHandler husband = new Husband();
		IHandler son = new Son();
		for(IWoman woman: arrayList){
			//未结婚的少女向父亲请示
			if(woman.getType() == 1){
				System.out.println("\n---女儿向父亲请示");
				father.handler(woman);
			}else if(woman.getType()  == 2){
				System.out.println("\n----妻子向丈夫请示。。。。");
				husband.handler(woman);
			}else if(woman.getType() == 3){
				System.out.println("\n ---- 母亲向儿子请示。。。。。");
				son.handler(woman);
			}else{
				System.out.println("请求不被允许，，，，，");
			}
		}
	}
}
