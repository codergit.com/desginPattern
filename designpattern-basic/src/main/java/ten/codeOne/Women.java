package ten.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：古代女性实现类
 * 说明：
 */
public class Women  implements IWoman{
	/**
	 * 通过int类型的描述妇女的状况
	 * 1.未出嫁
	 * 2.已出嫁
	 * 3.夫死
	 */
	private int type= 0;
	//请求活动
	private String request;
	//传递过来的请求
	public Women(int _type,String _request){
		this.type = _type;
		this.request= _request;
	}
	public int getType() {
		return type;         
	}
	//妇女的请示
	public String getRequest() {
		return this.request;
	}

}
