package ten.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：父亲类处理未出嫁的女儿请求
 * 说明：
 */
public class Father implements IHandler{

	public void handler(IWoman women) {
		System.out.println("女儿的请示是： "+women.getRequest());
		System.out.println("父亲的回答是：同意");
	}

}
