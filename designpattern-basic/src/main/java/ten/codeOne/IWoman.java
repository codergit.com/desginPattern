package ten.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：古代女性接口
 * 说明：
 */
public interface IWoman {
	//获取家庭状况
	public int getType();
	//获取活动请求，比如出去逛街啥的，，，
	public String getRequest();
}
