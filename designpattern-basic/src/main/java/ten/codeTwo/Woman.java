package ten.codeTwo;

import com.design.ten.codeOne.IWoman;
/**
 * 通过int类型的描述妇女的状况
 * 1.未出嫁
 * 2.已出嫁
 * 3.夫死
 */
public class Woman implements IWoman{
	private int type= 0;
	private String request = "";
	public Woman(int _type,String _request){
		this.type = _type;
		switch(this.type){
		case 1:
			this.request ="女儿的请求是："+_request;
			break;
		case 2:
			this.request = "妻子的请求是： "+_request;
			break;
		case 3:
			this.request = "母亲的请求是： "+_request;
			break;
		}
	}
	//获得自己的状况
	public int getType() {
		return this.type;
	}
	//获得妇女的请求
	public String getRequest() {
		return this.request;
	}

}
