package ten.codeTwo;

import com.design.ten.codeOne.IWoman;

/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：儿子只能处理母亲的请求
 * 说明：继承Handler类
 */
public class Son extends Handler {

	public Son() {
		super(Handler.SON_LEVEL_REQUEST);
	}

	@Override
	public void response(IWoman women) {
		System.out.println("母亲向儿子请求。。。。");
		System.out.println(women.getRequest());
		System.out.println("儿子的回答是： 同意\n");
	}

}
