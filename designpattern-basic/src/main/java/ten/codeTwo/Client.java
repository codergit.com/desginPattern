package ten.codeTwo;

import java.util.ArrayList;
import java.util.Random;

import com.design.ten.codeOne.IWoman;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：在Client中设置请求的传递顺序，先向父亲请示，，不是父亲解决的问题则由丈夫解决
 * 
 * 说明：业务调用类Client不用去做判断，到底是需要谁去处理
 * 而且Handler抽象类的子类可以继续扩展，该父类处理就父类处理，不该父类处理
 * 就往下传递。
 */
public class Client {
	public static void main(String[] args) {
		Random rend = new Random();
		ArrayList<IWoman> arrayList = new ArrayList();
		for(int i =0 ;i< 5;i++){
			arrayList.add(new Woman(rend.nextInt(4),"我要出去逛街。。。"));
		}
		//定义三个请示对象
		Handler father = new Father();
		Handler husband = new Husband();
		Handler son = new Son();
		//设置请求顺序
		father.setNext(husband);
		husband.setNext(son);
		for(IWoman women :arrayList){
			father.handleMessage(women);
		}
	}
}
