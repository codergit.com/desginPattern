package ten.codeTwo;

import com.design.ten.codeOne.IWoman;
/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：父亲的处理
 * 说明：
 */
public class Father extends Handler {

	public Father() {
		//父亲只处理女儿的请求
		super(Handler.FATHER_LEVEL_REQUEST);
	}

	

	//父亲的答复
	@Override
	public void response(IWoman women) {
		System.out.println("女儿向父亲请示---------");
		System.out.println(women.getRequest());
		System.out.println("父亲的答复是：同意\n");
	
	
	}

}
