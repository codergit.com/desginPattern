package ten.codeTwo;

import com.design.ten.codeOne.IWoman;

/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：修改后的处理类
 * 说明：这里也用到了模板方法模式，
 * 在模板方法中判断请求的级别和当前能够处理的级别，如果相同则调用
 * 基本方法，做出反馈，如果不相同则传递到下一个环节，由下个环节做出回应
 * 如果已经达到环节结尾，则直接做不同意处理，基本方法response需要各个实现类
 * 实现。：每个实现类有两个职责：1
 * 定义自己能够处理的等级级别，二是对请求做出回应
 */
public abstract class Handler {
	public final static int FATHER_LEVEL_REQUEST= 1;
	public final static int HUSBAND_LEVEL_REQUEST = 2;
	public final static int  SON_LEVEL_REQUEST = 3;
	//能处理的级别
	private int level = 0;
	
	//下一个责任人是谁
	private Handler nexthandler;
	//每个类都要说明一下自己能处理哪些请求
	public Handler(int _level){
		this.level = _level;
	}
	//一个女性（女儿，气质或者是母亲）要求逛街，你要处理这些请求

	public final void handleMessage(IWoman women){
		if(women.getType() == this.level){
			this.response(women);
		}else{
			if(this.nexthandler != null){
				//有后续环节才把请求往后递送。
				this.nexthandler.handleMessage(women);
			}else{
				//已经没有后续处理人了，不用处理了
				System.out.println("没地方请示了，按不同意处理。。。。。。");
			}
		}
	}
	/**
	 * 
	 * 作者：fcs
	 * 描述：如果不属于你处理的请求，你应该让她找下一个环节的人，
	 * 时间：2014-8-15
	 */
	public void setNext(Handler _handler){
		this.nexthandler = _handler;
	}
	//有请求有回应
	public abstract void response(IWoman women);

	public void handler(IWoman woman) {		
	}
}
