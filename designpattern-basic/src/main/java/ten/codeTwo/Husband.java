package ten.codeTwo;

import com.design.ten.codeOne.IWoman;

/**
 * 
 * @author fcs
 * @date 2014-8-15
 * 描述：丈夫处理妻子的请求
 * 说明：继承Handler类
 */
public class Husband extends Handler{

	public Husband() {
		super(Handler.HUSBAND_LEVEL_REQUEST);
	}

	@Override
	public void response(IWoman women) {
		System.out.println("妻子向丈夫请示。。。。。");
		System.out.println(women.getRequest());
		System.out.println("丈夫的回答是：同意\n");
	}

}
