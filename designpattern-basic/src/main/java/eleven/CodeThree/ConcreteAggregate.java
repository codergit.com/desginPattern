package eleven.CodeThree;

import java.util.Vector;

/**
 * 
 * @author fcs
 * @date 2014-8-18
 * ��������������
 * ˵����
 */
public class ConcreteAggregate implements Aggregate{
	private Vector vector ;
	public ConcreteAggregate(){}
	public ConcreteAggregate(Vector _vector) {
		super();
		this.vector = _vector;
	}

	public void add(Object object) {
		this.vector.add(object);
	}

	public void remove(Object object) {
		this.remove(object);
	}

	public Iterator iterator() {
		return new ConcreteIterator(this.vector);
	}

}
