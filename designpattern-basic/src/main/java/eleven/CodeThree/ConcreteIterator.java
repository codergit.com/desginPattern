package eleven.CodeThree;

import java.util.Vector;


/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：具体迭代器
 * 说明：实现迭代器接口
 * 注意：开发系统时，迭代器的删除方法应该完成两个逻辑，一是删除当前元素，
 * 二是指向下一个元素。
 */
public class ConcreteIterator implements Iterator{
	private Vector vector ;
	public int cursor = 0;   //定义当前游标

	
	public ConcreteIterator(Vector vector) {

		this.vector = vector;
	}
	public boolean hasNext() {
		if(this.cursor == this.vector.size()){
			return false;
		}
		else{
			return true;
		}
	}
	public Object next() {
		Object result = null;
		if(this.hasNext()){
			result =  this.vector.get(this.cursor++);
		}
		return null;
	}
	public boolean remove() {
		this.vector.remove(this.cursor);
		return true;
	}



	
	
}
