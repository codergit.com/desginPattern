package eleven.CodeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：抽象迭代器接口
 * 说明：迭代器通用源码
 */
public interface Iterator {
	//遍历下一个元素
	public Object next();
	//是否已经遍历到尾部
	public boolean hasNext();
	//删除当前指向的元素
	public boolean remove();
}
