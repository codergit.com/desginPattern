package eleven.CodeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		//声明出容器	
		Aggregate agg = new ConcreteAggregate();
		//产生式对象数据放入
		agg.add("adfae");
		agg.add("2wr");
		agg.add("2242");
		Iterator iterator = agg.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
	}
}
