package eleven.CodeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：抽象接口
 * 说明：
 */
public interface Aggregate {
	//在容器中添加
	public void add(Object object);
	//减少元素
	public void remove(Object object);
	//迭代器遍历
	public Iterator iterator();
	
	
}
