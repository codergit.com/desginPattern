package eleven.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：场景类
 * 说明：
 */
public class Boss {
	public static void main(String[] args) {
		ArrayList<IProject> projectList = new ArrayList<IProject>();
		projectList.add(new Project("星球大战项目",10,1000000));
		projectList.add(new Project("扭转时空项目",101,1000003435));
		projectList.add(new Project("超人改造项目",10,1000000));
		
		for(int i = 4;i<104;i++){
			projectList.add(new Project("第"+i+"个项目",1*5,i*1000));
		}
		
		for(IProject project :projectList){
				System.out.println(project.getProjectInfo());
		}
		
	}
}
