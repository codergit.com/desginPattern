package eleven.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：项目实现类
 * 说明：
 */
public class Project implements IProject{
	private String name = "";  //项目名称
	private int num = 0;      //人员数目
	private int cost = 0;     //费用
	
	
	
	public Project(String name, int num, int cost) {
		this.name = name;
		this.num = num;
		this.cost = cost;
	}


	//得到项目信息
	public String getProjectInfo() {
		String info = "";
		info = info+"项目名称是： "+this.name;
		info = info+"\t项目人数"+this.num;
		info = info+"\t项目费用"+this.cost;
		
		return info;
		
	}

}
