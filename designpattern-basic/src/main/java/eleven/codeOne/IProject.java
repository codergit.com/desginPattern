package eleven.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：项目信息接口
 * 说明：迭代器模式与面向接口编程
 */
public interface IProject {
	public String  getProjectInfo();
}
