package eleven.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：项目信息接口
 * 说明：直接使用add的方法增加项目信息
 */
public interface IProject {
	//增加项目
	public void add(String name, int num, int cost);
	//从老板这里看到的就是项目信息
	public String getProjectInfo();
	//获得一个可以遍历的对象
	public IProjectIterater interator();
	
}
