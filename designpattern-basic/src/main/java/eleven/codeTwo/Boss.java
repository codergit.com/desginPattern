package eleven.codeTwo;



/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：场景类
 * 说明：
 */
public class Boss {
	public static void main(String[] args) {
	    IProject project = new Project();
		project.add("星球大战项目",10,1000000);
		project.add("扭转时空项目",101,1000003435);
		project.add("超人改造项目",10,1000000);
		
		for(int i = 4;i<104;i++){
			project.add("第"+i+"个项目",1*5,i*1000);
		}
		IProjectIterater projectIterator = project.interator();
		while(projectIterator.hasNext()){
			IProject p = (IProject)projectIterator.next();
			System.out.println(p.getProjectInfo());
		
		}
		
	}
}
