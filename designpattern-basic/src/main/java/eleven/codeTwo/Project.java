package eleven.codeTwo;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：项目信息
 * 说明：实现IProject
 */
public class Project implements IProject{
	//定义项目列表
	private ArrayList<IProject> projectList = new ArrayList<IProject>();
	private String name = "";
	private int num =  0;
	private int cost = 0;
	
	//构造函数存储信息
	private Project( String name, int num,
			int cost) {
		this.name = name;
		this.num = num;
		this.cost = cost;
	}
	public Project(){}

	public void add(String name, int num, int cost) {
		this.projectList.add(new Project(name,num,cost));
	}

	public String getProjectInfo() {
		String info = "";
		info = info+"项目名称是： "+this.name;
		info = info+"\t项目人数"+this.num;
		info = info+"\t项目费用"+this.cost;
		
		return info;	}

	//产生一个遍历时对象
	public IProjectIterater interator() {
		return new ProjectIterator(this.projectList);
	}
	
	
}
