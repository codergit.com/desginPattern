package eleven.codeTwo;

import java.util.ArrayList;

public class ProjectIterator implements IProjectIterater{
	//存放项目的列表
	private ArrayList<IProject> projectList = new ArrayList<IProject>();
	private int currentItem = 0;
	
	
	public ProjectIterator(ArrayList<IProject> projectList) {
		this.projectList = projectList;
	}
	//判断是否还有元素必须实现
	public boolean hasNext() {
		boolean b = true;
		if(this.currentItem >= projectList.size() || this.projectList.get(this.currentItem) == null){
			b = false;
		}
		return b;
	}
	//取得下一个值（注意返回值由Object ---->IProject）
	public IProject next() {
		return ((IProject)this.projectList.get(this.currentItem++));
	}
	//删除一个对象
	public void remove() {
		//暂时不用
	}

}
