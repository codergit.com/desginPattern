package eleven.codeTwo;

import java.util.Iterator;
/**
 * 
 * @author fcs
 * @date 2014-8-18
 * 描述：项目迭代器接口
 * 说明：作者的习惯：如果要实现一个容器或者其他API接口时，我一般都会自己写一个接口继承，然后再 
 * 继承自己写的接口，保证自己的实现类只用实现自己写的接口（接口传递，当然也要实现顶层的接口）
 */
public interface IProjectIterater extends Iterator<Object>{

}
