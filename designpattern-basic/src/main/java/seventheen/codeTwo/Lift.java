package seventheen.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：电梯实现类
 * 说明：每个状态使用switch 和case 进行状态判断
 */
public class Lift implements Ilift {
	private int state;
	public void setState(int state) {
		this.state = state;
	}

	public void run() {
		switch(this.state){
		case OPENING_STATE: break; // do nothing 
		case STOPPING_STATE : 
			this.runWithoutLogic();
			this.setState(RUNNING_STATE);
			break;
		case CLOSING_STATE:
			this.runWithoutLogic();
			this.setState(RUNNING_STATE);
			break ;
		case RUNNING_STATE:
			break; //do nothing
			
		}
	}

	public void stop() {
		switch(this.state){
		case RUNNING_STATE:
			this.stopWithoutLogic();
			this.setState(CLOSING_STATE);
			break;
		case CLOSING_STATE:
			this.stopWithoutLogic();
			this.setState(STOPPING_STATE);
			break;
		case OPENING_STATE:
			
			break;
		case STOPPING_STATE:
		    break;  //do nothing 
		}
	}

	public void open() {
		switch(this.state){
		case RUNNING_STATE:
			break;   //do nothing
		case CLOSING_STATE:
			this.openWithOutLogic();
			this.setState(OPENING_STATE);
			break;
		case OPENING_STATE:
			break;// do nothing 
		case STOPPING_STATE:
			this.openWithOutLogic();
			this.setState(OPENING_STATE);
		    break; 
		}
	}

	public void close() {
		switch(this.state){
		case RUNNING_STATE:
			
			break;
		case CLOSING_STATE:
		
			break;
		case OPENING_STATE:
			this.closeWithoutLogic();
			this.setState(CLOSING_STATE);
			break;
		case STOPPING_STATE:
		    break;  //do nothing 
		}
		
	}
	//纯粹的开关门，不考虑任何实际条件和逻辑
	private void closeWithoutLogic(){
		System.out.println("电梯关门。。。。");
	}
	private void  runWithoutLogic(){
		System.out.println("电梯运行。。。");
	}
	private void openWithOutLogic(){
		System.out.println("打开电梯。。。。");
	}
	private void stopWithoutLogic(){
		System.out.println("停止。。。。");
	}
	
}
