package seventheen.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：电梯状态接口
 * 说明：
 */
public interface Ilift {
  //电梯的四种状态
  public final static int OPENING_STATE = 1;
  public final static int RUNNING_STATE = 2;
  public final static int STOPPING_STATE = 3;
  public final static int CLOSING_STATE = 4;
  public void setState(int state);
  public void run();
  public void stop();
  public void open();
  public void close();
}
