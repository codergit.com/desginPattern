package seventheen.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：场景类
 * 说明：本程序有的问题
 *  1.电梯实现类Lift有点长，方法较多
 *  2.扩展性非常差，不止四个状态，违背开闭原则。
 *  3. 非常规状态无法实现，违反单一原则
 *  4.停止状态下下一步是什么状态，怎么设定的。
 *  这里有一个过渡状态
 */
public class Client {
   public static void main(String[] args) {
	   Ilift  lift = new Lift();
	   lift.setState(Ilift.STOPPING_STATE);
	   lift.open();
	   lift.close();
	   lift.run();
	   lift.stop();
   }
}
