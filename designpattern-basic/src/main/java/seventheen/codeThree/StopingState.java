package seventheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：停止状态实现类
 * 说明：
 */
public class StopingState  extends LiftState {
	//停止状态再运行
	@Override
	public void run() {
		super.context.setLiftState(Context.runningState);
		super.context.getLiftState().run();
	}

	@Override
	public void stop() {
		//电梯是处于停止状态的。
	}
	//停止状态可以打开
	@Override
	public void open() {
		super.context.setLiftState(Context.openningState);
		super.context.getLiftState().open();
		
	}

	@Override
	public void close() {
		 //停止状态本来是关着的
	}

}
