package seventheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：运行状态实现类
 * 说明：
 */
public class RuningState  extends LiftState{

	@Override
	public void run() {
		System.out.println("电梯正在运行。。。。。");
	}
    //新状态设置
	@Override
	public void stop() {
		super.context.setLiftState(Context.stopingState);
		super.context.getLiftState().stop();
	}

	@Override
	public void open() {
       //运行状态肯定不是开着的 		
	}

	@Override
	public void close() {
		//运行状态肯定是关着的
	}

}
