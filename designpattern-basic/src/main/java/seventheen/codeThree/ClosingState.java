package seventheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：关闭状态实现类
 * 说明：
 */
public class ClosingState extends LiftState {

	@Override
	public void run() {
		super.context.setLiftState(Context.runningState);
		super.context.getLiftState().run();
	}
	//电梯门关了，停止
	@Override
	public void stop() {
		super.context.setLiftState(Context.stopingState);
		super.context.getLiftState().stop();
		
	}
	//电梯门关了再打开
	@Override
	public void open() {
		super.context.setLiftState(Context.openningState);
		super.context.getLiftState().open();
	}

	@Override
	public void close() {
		System.out.println("电梯门关了，。。。。。。");
	}

}
