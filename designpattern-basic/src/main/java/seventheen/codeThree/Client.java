package seventheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：场景类模拟
 * 说明：现在各个类的状态是单个的类，符合迪米特法则
 */
public class Client {
   public static void main(String[] args) {
	Context context = new Context();
	context.setLiftState(new ClosingState());
	context.open();
	context.close();
	context.run();
	context.stop();
	   
	   
   }
}
