package seventheen.codeThree;

/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：抽象电梯状态
 * 说明：声明一个受保护的类型Context变量，这个是串联各个状态
 * 的封装类，封装电梯对象的内部状态不被调用者知晓，符合迪米特法则
 * 定义四个具体的实现类，承担的是状态的产生以及状态之间的转换过渡。
 * 
 */
public abstract class LiftState {
    //定义一个环境角色，也就是封装状态的变化引起的功能变化
	protected Context context;
    public  void setContext(Context _context){
    	this.context = _context;
    }
	public abstract void run();
	public abstract void stop();
	public abstract void open();
	public abstract void close();
}
