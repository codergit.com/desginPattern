package seventheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：上下文类
 * 说明：该类是一个环境角色，作用是串联各个状态的过渡，
 * 在LIftState抽象类中我们定义并把这个环境角色聚合进来，并传递到子类，
 * 也就是四个具体的实现类中自己根据环境来决定如何进行状态的过渡。
 */
public class Context {
	//定义出所有的电梯状态
	public final static OpenningState openningState = new OpenningState();
	public final static ClosingState closingState = new ClosingState();
	public final static StopingState stopingState = new StopingState();
	public final static RuningState runningState = new RuningState();
	//定义当前电梯状态
	private LiftState liftState;
	public LiftState getLiftState() {
		return liftState;
	}
	public void setLiftState(LiftState liftState) {
		this.liftState = liftState;
	    this.liftState.setContext(this);

	}
	public void run(){
		this.liftState.run();
	}
	public void stop(){
		this.liftState.stop();
	}
	public void close(){
		this.liftState.close();
	}
	public void open(){
		this.liftState.open();
	}
	

}
