package seventheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：敞门状态
 * 说明：
 */
public class OpenningState   extends LiftState {
    
	@Override
	public void run() {
		//打开状态不可能运行
	}

	@Override
	public void stop() {
		//打开状态一定停止
	}

	@Override
	public void open() {
		//本来就是打开状态的
	}
	//状态修改为关闭状态
	@Override
	public void close() {
		super.context.setLiftState(Context.closingState);
		super.context.getLiftState().close();
		
	}
}
