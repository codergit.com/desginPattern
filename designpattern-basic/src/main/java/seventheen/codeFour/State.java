package seventheen.codeFour;
/**]
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：抽象环境角色
 * 说明：抽象环境中声明一个环境角色，提供各个状态类的自行访问，
 * 并且提供所有状态的抽象行为，由各个实现类实现。
 */
public  abstract class State {
  //定义一个环境角色，提供子类访问
	protected Context context;
	//设置环境角色
	public void setContext(Context _context){
		this.context = _context;
	}
	//行为1
	public abstract void handle1();
	//行为2
	public abstract void handle2();
	
	
	
	
}
