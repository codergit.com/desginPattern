package seventheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：具体环境状态类
 * 说明：继承抽象State类
 */
public class ConcreteState2 extends State{

	@Override
	public void handle1() {
		//设置当前状态为State2
		super.context.setCurrentState(Context.cState1);
		super.context.handle1();
	}

	@Override
	public void handle2() {
		System.out.println("本状态2必须处理的逻辑");

	}

}
