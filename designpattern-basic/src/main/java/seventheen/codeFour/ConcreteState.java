package seventheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：具体环境角色
 * 说明：
 */
public class ConcreteState  extends State{

	@Override
	public void handle1() {
		System.out.println("本状态必须处理的逻辑");
	}

	@Override
	public void handle2() {
		//设置当前状态为State2
		super.context.setCurrentState(Context.cState2);
		super.context.handle2();
	}

}
