package seventheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：场景类
 * 说明：具体环境角色
 */
public class Client {
	public static void main(String[] args) {
		//定义环境角色
		Context context = new Context();
		//初始化状态
		context.setCurrentState(new ConcreteState());
		//行为执行
		context.handle1();
		context.handle2();
		
		
		
	}
}
