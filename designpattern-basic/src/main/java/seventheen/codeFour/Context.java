package seventheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：具体环境角色
 * 说明：两个职责： 处理本状体必须完成的任务，决定是否可以过渡到其他状态。
 */
public class Context {
  //定义状态
	public  final static  State  cState1 = new ConcreteState();
	public  final static  State  cState2 = new ConcreteState2();
	private  State CurrentState;
	//获得当前状态
	public State getCurrentState() {
		return CurrentState;
	}
	//设置当前状态
	public void setCurrentState(State currentState) {
		CurrentState = currentState;
		//切换状态
		this.CurrentState.setContext(this);
	}
	//行为委托
	public void handle1(){
		this.CurrentState.handle1();
	}
	public void handle2(){
		this.CurrentState.handle2();
	}
	
}
