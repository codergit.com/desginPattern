package seventheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：电梯的四种状态
 * 说明：
 */
public interface ILeft {
   public void run();
   public void stop();
   public void open();
   public void close();
   
}
