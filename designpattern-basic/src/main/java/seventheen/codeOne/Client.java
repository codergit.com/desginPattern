package seventheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：场景类
 * 说明：只是简单的模拟一下电梯的运行状态，但是程序还是有一定的缺陷。
 * 电梯有四种状态
 *  1。敞门状态
 *  2.关闭状态
 *  3.运行状态
 *  4.停止状态
 *  可以根据这四个状态的转换进行设定，
 */
public class Client {
     public static void main(String[] args) {
	   ILeft lift = new Lift();
	   lift.open();
	   lift.close();
	   lift.run();
	   lift.stop();
    } 
}
