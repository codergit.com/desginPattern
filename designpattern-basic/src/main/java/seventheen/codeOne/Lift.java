package seventheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-26
 * 描述：电梯实现类
 * 说明：
 */
public class Lift implements  ILeft{

	public void run() {
		System.out.println("电梯正在运行");
	}

	public void stop() {
		System.out.println("电梯停止。。。。");
	}

	public void open() {
		System.out.println("电梯打开了。。。。。");
	}

	public void close() {
		System.out.println("电梯关门。。。。。");
	}
   
}
