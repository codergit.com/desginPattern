package fiftheen.codeTwo;
import java.util.ArrayList;

import com.design.fiftheen.codeOne.BMWModel;
import com.design.fiftheen.codeOne.CarModel;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：宝马车组装者
 * 说明：
 */
public class BMWBuilder extends CarBuilder{
	private BMWModel bmw = new BMWModel();
	@Override
	public void setSequence(ArrayList<String> sequence) {
		this.bmw.setSequence(sequence);
	}

	@Override
	public CarModel getCarModel() {
		return this.bmw;
	}

}
