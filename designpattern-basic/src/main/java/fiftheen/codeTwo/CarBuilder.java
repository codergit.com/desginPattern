package fiftheen.codeTwo;

import java.util.ArrayList;

import com.design.fiftheen.codeOne.CarModel;

/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：抽象汽车组装类
 * 说明：具体的类型的车模由相关的子类完成
 */

public abstract class CarBuilder {
	//组装顺序
	public abstract void setSequence(ArrayList<String> sequence);
	//设置顺序后直接拿到车辆模型。
	public abstract CarModel getCarModel();


}
