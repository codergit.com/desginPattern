package fiftheen.codeTwo;

import java.util.ArrayList;

import com.design.fiftheen.codeOne.BenzModel;

/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：更改后模拟需求的场景类
 * 说明：根据组装后的车辆模型，生产奔驰，宝马一样可以按照这个生产
 *
 * 但是需求是永无止境的，根据上一个原则是开闭原则说的是封装可能出现的变化
 * 因此可以未来可能有A B型的宝马 A B 型的奔驰进行封装。（建造者模式要解决的问题）
 *
 */
public class Client {
	public static void main(String[] args) {
		/**
		 * 按照客户要求的顺序进行制造
		 */
		
		//存放run的顺序
		ArrayList<String> sequence = new ArrayList<String>();
		sequence.add("enginBoom");   //客户要求run的时候发动引擎
		sequence.add("start");
		sequence.add("stop");

		//要一辆奔驰
		BenzBuilder benzBuilder = new BenzBuilder();
		//设置控制顺序
		benzBuilder.setSequence(sequence);
		//制造一辆奔驰
		BenzModel benz  = (BenzModel) benzBuilder.getCarModel();
		benz.run();
		
		
		
	}
}
