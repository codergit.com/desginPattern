package fiftheen.codeTwo;

import java.util.ArrayList;

import com.design.fiftheen.codeOne.BenzModel;
import com.design.fiftheen.codeOne.CarModel;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：奔驰组装者
 * 说明：
 */
public class BenzBuilder extends CarBuilder{
	private BenzModel benz = new BenzModel();
	@Override
	public void setSequence(ArrayList<String> sequence) {
		this.benz.setSequence(sequence);
	}

	@Override
	public CarModel getCarModel() {
		return this.benz;
	}

}
