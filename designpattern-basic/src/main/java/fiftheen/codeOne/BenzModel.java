package fiftheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：奔驰模型代码
 * 说明：
 */
public class BenzModel extends CarModel{

	@Override
	protected void start() {
		System.out.println("奔驰车跑起来是这样的。。。。。");
	}

	@Override
	protected void stop() {
		System.out.println("奔驰车是这样停车的。。。。。。");
	}

	@Override
	protected void alarm() {
		System.out.println("奔驰车的喇叭声是这样的。。。。。。");
	}

	@Override
	protected void engineBoom() {
		System.out.println("奔驰的引擎是这样的。。。。。。。");
	}
	
	
	
}
