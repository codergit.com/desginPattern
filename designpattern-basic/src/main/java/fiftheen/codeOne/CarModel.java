package fiftheen.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：车辆模型的抽象类
 * 说明：这种模式需要一个具体的控制顺序，但是这种顺序方式有很多，因此不能满足变化的
 * 需求。增加一个建造者，按需求进行开发
 */
public  abstract class CarModel {
	//基本方法的执行顺序
	private ArrayList<String> sequence = new ArrayList<String>();
	//模型启动
	protected abstract void start();
	//停止
	protected abstract void stop();
	//喇叭
	protected abstract void alarm();
	//引擎声音
	protected abstract void engineBoom();
	
	final public void run(){
		//循环执行顺序
		for(int i =  0;i< this.sequence.size();i++){
			String actionName = this.sequence.get(i);
			if(actionName.equalsIgnoreCase("start")){
				this.start();
			}else if(actionName.equalsIgnoreCase("alarm")){
				this.alarm();
			}else if(actionName.equalsIgnoreCase("enginBoom")){
				this.engineBoom();
			}else if(actionName.equalsIgnoreCase("stop")){
				this.stop();
			}
		}
	}
	//把传递过来的值传递到类内
	final public void setSequence(ArrayList<String> sequence){
		this.sequence = sequence;
	}
	
}
