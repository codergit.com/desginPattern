package fiftheen.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：模拟需求的场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		/**
		 * 按照客户要求的顺序进行制造
		 */
		BenzModel benz = new BenzModel();
		//存放run的顺序
		ArrayList<String> sequence = new ArrayList<String>();
		sequence.add("enginBoom");   //客户要求run的时候发动引擎
		sequence.add("start");
		sequence.add("stop");
		//把按照要求的顺序赋值给奔驰
		benz.setSequence(sequence);
		benz.run();
		
		
	}
}
