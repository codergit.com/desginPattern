package fiftheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：宝马车模型代码
 * 说明：
 */
public class BMWModel extends CarModel{
	@Override
	protected void start() {
		System.out.println("宝马车跑起来是这样的。。。。。");
	}

	@Override
	protected void stop() {
		System.out.println("宝马车是这样停车的。。。。。。");
	}

	@Override
	protected void alarm() {
		System.out.println("宝马车的喇叭声是这样的。。。。。。");
	}

	@Override
	protected void engineBoom() {
		System.out.println("宝马的引擎是这样的。。。。。。。");
	}
}
