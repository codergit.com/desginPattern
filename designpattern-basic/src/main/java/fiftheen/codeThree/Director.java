package fiftheen.codeThree;

import java.util.ArrayList;

import com.design.fiftheen.codeOne.BMWModel;
import com.design.fiftheen.codeOne.BenzModel;
import com.design.fiftheen.codeTwo.BMWBuilder;
import com.design.fiftheen.codeTwo.BenzBuilder;

/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：导演类
 * 说明：使用this关键字调用类中的成员变量或者方法，这样调用者会更加清晰，
 * 易于维护，super也是
 * 对于ArrayList和HashMap如果定义成类的成员变量，则在方法调用中一定要clear（）
 * 一下防止数据混乱。
 * 
 * 建造者模式核心类
 */
public class Director {
	private ArrayList<String> sequence = new ArrayList<String>();
	private BenzBuilder benzBuilder = new BenzBuilder();
	private BMWBuilder bmwBuilder  = new BMWBuilder();
	/**
	 * A类型的奔驰车模，只有启动和停止两项需求。
	 */
	public BenzModel getABenzModel(){
		//清理场景，这里是一些初级程序员不注意的地方。
		this.sequence.clear();
		this.sequence.add("start");
		this.sequence.add("stop");
		//按照顺序返回一个奔驰车
		this.benzBuilder.setSequence(this.sequence);
		return (BenzModel)this.benzBuilder.getCarModel();
	}
	/**
	 * B类型的奔驰车模，发动引擎，启动，停止
	 */
	public BenzModel getBBenzModel(){
		this.sequence.clear();
		this.sequence.add("start");
		this.sequence.add("stop");
		//按照顺序返回一个奔驰车
		this.benzBuilder.setSequence(this.sequence);
		return (BenzModel)this.benzBuilder.getCarModel();
	}
	
	/**
	 * C型号宝马： 先按喇叭，然后启动，然后停止
	 */
	public BMWModel getCBMWModel(){
		this.sequence.clear();
		this.sequence.add("alarm");
		this.sequence.add("start");
		this.sequence.add("stop");
	    return (BMWModel)this.bmwBuilder.getCarModel();
	}
	/**
	 * D型号宝马： 先启动，奔跑
	 */
	public BMWModel getDBMWModel(){
		this.sequence.clear();
		this.sequence.add("start");
	    return (BMWModel)this.bmwBuilder.getCarModel();
	}
	
	
}
