package fiftheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：场景类
 * 说明：对原有的模型进行了重构，
 * 重构的目的就是：简单，清晰，代码让人能够看懂。
 */
public class Client {
	public static void main(String[] args) {
		Director director = new Director();
		//1万辆A型奔驰
		for(int i =0 ;i< 10000;i++){
			director.getABenzModel().run();
		}
		
		//1万辆c型宝马
		for(int i =0 ;i< 10000;i++){
			director.getCBMWModel().run();
		}	
		
	}
}
