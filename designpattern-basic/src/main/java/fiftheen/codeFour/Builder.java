package fiftheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：抽象建造者
 * 说明：
 */
public abstract class Builder {
	//设置产品的不同部分，以获得不同的产品
	public abstract void setPart();
	//建造产品
	public abstract Product builderProduct();


}
