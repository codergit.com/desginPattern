package fiftheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-23
 * 描述：具体建造者
 * 说明：如果有多个产品类就有几个具体的建造者，而且这些多个产品类具有相同的
 * 接口或者抽象类。
 */
public class ConcreteProduct extends Builder {
	private Product product = new Product();
	@Override
	public void setPart() {
		/**
		 * 产品类内部的逻辑处理
		 */
	}

	//构建一个产品
	@Override
	public Product builderProduct() {
		return product;
	}

}
