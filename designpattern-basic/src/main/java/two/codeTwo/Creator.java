package two.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：抽象工厂类
 * 说明：抽象工厂类负责定义产品对象的产生，
 */
public abstract class Creator {
	/**
	 * 创建一个产品对象，其输入参数类型可以自行设定
	 * 通常为String Enum Class等，也可以为空
	 */
	public abstract <T extends Product> T createProduct(Class<T> c);
}
