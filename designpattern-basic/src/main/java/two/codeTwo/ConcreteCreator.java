package two.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：具体工厂类
 * 说明：具体如何产生一个产品对象是由具体的工厂类实现的
 *
 */
public class ConcreteCreator extends Creator{

	@SuppressWarnings("unchecked")
	public <T extends Product> T createProduct(Class<T> c) {
		Product product= null;
		try{product = (Product)Class.forName(c.getName()).newInstance();
		}catch(Exception e){
			//异常处理
		}
		return (T)product;

		}
}
