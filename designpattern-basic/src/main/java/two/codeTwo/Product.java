package two.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：抽象产品类
 *
 */
public abstract class Product {
	//产品累的公共方法
	public void method1(){
		//业务逻辑处理
	}
	public  abstract void method2();
}
