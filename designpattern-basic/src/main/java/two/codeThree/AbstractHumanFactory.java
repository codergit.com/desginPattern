package two.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：抽象人类创建工厂
 * 说明：这里采用了泛型，通过定义泛型对createHuamn的输入参数产生两层限制：
 * 必须是Class 类型；
 * 必须是Human 的实现类
 */
public abstract class AbstractHumanFactory {
	public abstract <T extends Human> T createHuamn(Class <T> c);
}
