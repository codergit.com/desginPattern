package two.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：简单工厂模式的场景类（女娲）
 * 说明：这是工厂方法模式的一种叫简单工厂模式。（也称为静态工厂模式）
 * 缺点：工厂类的扩展比较困难，不符合开闭原则，不过比较实用。
 */
public class NvWa {
	public static void main(String[] args) {
        System.out.println("--造出第一批人---白人---");
        Human whietMan = HumanFactory.createHuamn(WhiteHuman.class);
        whietMan.getColor();
        whietMan.talk();
        
        System.out.println("\n---造出第二批人---黑人----");
        Human blackMan = HumanFactory.createHuamn(BlackHuman.class);
        blackMan.getColor();
        blackMan.talk();
        
        System.out.println("\n---造出第三批人---黄种人----");
        Human yellowMan = HumanFactory.createHuamn(YellowHuman.class);
        yellowMan.getColor();
        yellowMan.talk();
	}
}
