package two.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：人类总称：人类的接口（共性）
 *
 */
public interface Human {
	//人的皮肤
	public void getColor();
	//人类会说话
	public void talk();
}
