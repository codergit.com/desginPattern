package two.codefour;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：多工厂模式的场景类  场景类（女娲）
 * 说明：
 *      该模式的优点：创建类的职责清晰，而且结构简单，
 *         缺点；影响了可维护性和可扩展性。
 * 应用：在复杂的应用中一般采用多工厂的方法，再增加一个协调类，必变调用者
 * 与各个子工厂交流，协调类的作用是封装子工厂类，对高层模块提供统一的访问接口。
 */
public class NvWa {
	public static void main(String[] args) {
        System.out.println("--造出第一批人---白人---");
        Human whietMan = (new WhitemanFactory()).createHuman();
        whietMan.getColor();
        whietMan.talk();
        
        System.out.println("\n---造出第二批人---黑人----");
        Human blackMan = (new BlackmanFactory()).createHuman();
        blackMan.getColor();
        blackMan.talk();
        
        System.out.println("\n---造出第三批人---黄种人----");
        Human yellowMan = (new YellowmanFactory()).createHuman();
        yellowMan.getColor();
        yellowMan.talk();
	}
}
