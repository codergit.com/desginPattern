package two.codefour;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：多工厂模式的抽象工厂类
 * 说明：每个人种（具体的产品类）都对应了一个创建者，每个创建者都独立负责创建对应的
 * 		产品对象，非常符合单一职责原则，
 * 注意：抽象方法中已经不再需要传递相关参数了，因为每一个具体的工厂都已经非常明确自己的职责：
 * 		创建自己负责的产品类对象。
 */
public abstract class AbstractHumanFactory {
	public abstract Human createHuman();
}
