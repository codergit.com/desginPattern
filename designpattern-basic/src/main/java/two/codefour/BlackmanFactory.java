package two.codefour;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：黑色人种的创建工厂实现
 *
 */
public class BlackmanFactory  extends AbstractHumanFactory{

	@Override
	public Human createHuman() {
		return new BlackHuman();
	}

	

}
