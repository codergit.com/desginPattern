package two.codeFive;

import java.util.HashMap;
import java.util.Map;

import com.design.two.codeTwo.CreateProduct1;
import com.design.two.codeTwo.Product;

/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：延迟初始化
 * 概念：一个对象被消费完毕后，并不立即释放，工厂类保持器初试状态，等待再次被使用。
 * 说明：
 *     1.延迟初始化时工厂方法模式的一个扩展应用。
 *     2.延迟加载框架室可以扩展的，例如限制摸一个产品类的最大实例化数量。
 *     通过map中已有的对象数量来实现。
 *     延迟加载的还可以用在对象初始化比较复杂的情况下，例如硬件访问，涉及多方面的交互。
 *     则可以通过延迟加载降低对象的产生和销毁带来的复杂性。
 * 
 *
 */
public class ProductFactory {
	private static final Map<String,Product> prMap = new HashMap<String,Product>();
	Product product = null;
	public static synchronized Product createProduct(String type) throws Exception{
		Product product  = null;
		if(prMap.containsKey(type)){
			product = prMap.get(type);
		}else{
			if(type.equals("product1")){
				product = new CreateProduct1();
			}else{
				product = new CreateProduct1();
			}
			//同时把对象放入缓存容器中
			prMap.put(type, product);
		}
		return product;

	}



}
