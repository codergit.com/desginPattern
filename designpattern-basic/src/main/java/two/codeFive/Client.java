package two.codeFive;

import com.design.two.codeTwo.ConcreteCreator;
import com.design.two.codeTwo.CreateProduct1;
import com.design.two.codeTwo.Creator;
import com.design.two.codeTwo.Product;

/**
 * 作者：fcs
 * 描述：工厂方法模式的场景类
 * 工厂方法模式定义：定义一个用于创建对象的接口，让子类决定实例化哪一个类，工厂
 * 方法使一个类的实例化延迟到其子类。
 * 该报是工厂方法模式的通用代码
 * 时间：2014-7-30
 */
public class Client {

	public static void main(String[] args) {
		Creator creator = new ConcreteCreator();
		Product product = creator.createProduct(CreateProduct1.class);
		//继续处理业务
	}

}
