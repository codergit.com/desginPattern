package two.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：场景类（女娲）
 *
 */
public class NvWa {
	public static void main(String[] args) {
		AbstractHumanFactory yinyangLu = new HumanFactory();
        System.out.println("--造出第一批人---白人---");
        Human whietMan = yinyangLu.createHuamn(WhiteHuman.class);
        whietMan.getColor();
        whietMan.talk();
        
        System.out.println("\n---造出第二批人---黑人----");
        Human blackMan = yinyangLu.createHuamn(BlackHuman.class);
        blackMan.getColor();
        blackMan.talk();
        
        System.out.println("\n---造出第三批人---黄种人----");
        Human yellowMan = yinyangLu.createHuamn(YellowHuman.class);
        yellowMan.getColor();
        yellowMan.talk();
	}
}
