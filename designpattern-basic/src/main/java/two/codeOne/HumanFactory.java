package two.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：人类创建工厂：继承抽象人类创建工厂（相当于八卦炉）
 *
 */
public class HumanFactory  extends AbstractHumanFactory{

	@SuppressWarnings("unchecked")
	public   <T extends Human> T createHuamn(Class<T> c) {
		//定义一个生产出的人种
		Human human = null;
		try{
			//产生一个人种
			human = (T)Class.forName(c.getName()).newInstance();
			
		}catch(Exception e){
			System.out.println("人种产生错误！！");
		}
		return (T)human;
	}

}
