package twelve.codeTwo;

import java.util.ArrayList;



/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：场景类
 * 说明：通过这样的方式进行人员资源管理图出现了，但是还可以进行优化
 */
public class Client {
	public static void main(String[] args) {
		Branch ceo = compositeCorpTree();
		System.out.println(ceo.getInfo());
	    System.out.println(getTreeInfo(ceo));
	}
	//组装树结构
	public static Branch compositeCorpTree(){
		//产生一个根节点
		Branch root = new Branch("王大麻子","总经理",100000);
		//产生三个部门经理，也就是树枝节点
		Branch developDep = new Branch("马二拐子","研发部经理",20000);
		Branch salesDep = new Branch("赵三驼子","销售部经理",30000);
		Branch financeDep = new Branch("溜达瘸子","财务部经理",5000);
		//产生三个小组
		Branch firstDevGroup  = new Branch("张三","研发一组组长",5000);
		Branch secondDevGroup  = new Branch("李斯","研发二组组长",5000);
		//小兵们
		Leaf a = new Leaf("a","开发人员",2000);
		Leaf b = new Leaf("b","开发人员",2000);
		Leaf c = new Leaf("c","开发人员",2000);
		Leaf d = new Leaf("d","开发人员",2000);
		Leaf e = new Leaf("e","开发人员",2000);
		Leaf f = new Leaf("f","开发人员",2000);
		Leaf g = new Leaf("g","开发人员",2000);
		Leaf h = new Leaf("h","开发人员",2000);
		Leaf j = new Leaf("j","开发人员",2000);
		Leaf k = new Leaf("k","秘书",6000);
		Leaf zhenglaoliu = new Leaf("郑老六","研发部副总",20000);
		//组装整个人员系统,ceo管辖
		root.addSub(salesDep);
		root.addSub(developDep);
		root.addSub(financeDep);
		root.addSub(k);
		
		//定义研发部门下的结构
		developDep.addSub(firstDevGroup);
		developDep.addSub(secondDevGroup);
		developDep.addSub(zhenglaoliu);
		//开发小组
		firstDevGroup.addSub(a);
		firstDevGroup.addSub(b);
		firstDevGroup.addSub(c);
		secondDevGroup.addSub(d);
		secondDevGroup.addSub(e);
		secondDevGroup.addSub(f);
		salesDep.addSub(g);
		salesDep.addSub(h);
		salesDep.addSub(j);
		return root;
	}
	//遍历树信息
	public static String getTreeInfo(Branch root){
		ArrayList<ICorp> subList = root.getSubordinate();
		String info = "";
		for(ICorp s: subList){
			if(s instanceof Leaf){
				 info = info + s.getInfo()+ "\n";
			}else{
				info = info + s.getInfo()+"\n"+getTreeInfo((Branch)s); 
			}
		}
		return info;
	}

}
