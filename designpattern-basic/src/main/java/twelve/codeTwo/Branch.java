package twelve.codeTwo;

import java.util.ArrayList;

public class Branch implements IBranch{
	ArrayList<ICorp> subList = new ArrayList<ICorp>();
	private String name = "";    //树枝节点的名称
	private String position = "";  //职位
	private int salary = 0;        //薪水
	
	public Branch(String name, String position, int salary) {
		this.name = name;
		this.position = position;
		this.salary = salary;
	}

	public String getInfo() {
		String info = "";
		info = "名称"+ this.name;
		info = info + "\t职位 " + this.position;
		info = info + "\t薪水 " + this.salary;
		return info;
	}


	public void addSub(ICorp corp) {
		this.subList.add(corp);
	}

	public ArrayList<ICorp> getSubordinate() {
		return this.subList;
	}

}
