package twelve.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：获取职员信息的接口
 * 说明：是公司所有人员的信息的接口类
 */
public interface ICorp {
	public String getInfo();
}
