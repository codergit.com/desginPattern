package twelve.codeTwo;

public class Leaf  implements ILeaf{
	private String name = "";    //叶子节点的名称
	private String position = "";  //职位
	private int salary = 0;        //薪水
	
	public Leaf(String name, String position, int salary) {
		this.name = name;
		this.position = position;
		this.salary = salary;
	}
	public String getInfo() {
		String info = "";
		info = "名称"+ this.name;
		info = info + "\t职位 " + this.position;
		info = info + "\t薪水 " + this.salary;
		return info;
	}


}
