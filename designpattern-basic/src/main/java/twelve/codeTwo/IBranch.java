package twelve.codeTwo;

import java.util.ArrayList;

public interface IBranch extends ICorp{
	//增加小兵的方法,树叶节点
	public void addSub(ICorp corp);
	//获取下属信息
	public ArrayList<ICorp> getSubordinate();
	//还有一个删除下属信息的，没有用到，不写
}
