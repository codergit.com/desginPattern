package twelve.codeOne;

import java.util.ArrayList;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：树枝节点实现类
 * 说明：
 */
public class Branch implements IBranch{
	//保存根节点下的树枝节点和树叶节点
	private ArrayList subordinateList = new ArrayList();
	private String name = "";    //树枝节点的名称
	private String position = "";  //职位
	private int salary = 0;        //薪水
	
	public Branch(String name, String position, int salary) {
		this.name = name;
		this.position = position;
		this.salary = salary;
	}

	public String getInfo() {
		String info = "";
		info = "名称"+ this.name;
		info = info + "\t职位 " + this.position;
		info = info + "\t薪水 " + this.salary;
		return info;
	}

	public void add(IBranch branch) {
		this.subordinateList.add(branch);
	}

	public void add(ILeaf leaf) {
		this.subordinateList.add(leaf);
	}

	public ArrayList getSubordinateInfo() {
		return this.subordinateList;
	}

}
