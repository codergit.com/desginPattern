package twelve.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：其他有节点分支的节点接口
 * 说明：比如部门
 */
public interface IBranch {
	//获得信息
	public String getInfo();
	//增加数据节点，例如研发部下的研发一组。
	public void add(IBranch branch);
	//增加叶子节点
	public void add(ILeaf leaf);
	
	//获得下属信息
	public ArrayList getSubordinateInfo();
	
	
	
}
