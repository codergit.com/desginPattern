package twelve.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：组合模式的根节点
 * 说明：接口。类型,这个根节点的对家就是总经理
 */
public interface IRoot {
	//得到总经理的信息
	public String getInfo();
	//总经理下面的小兵，是个树枝节点:比如研发部经理
	public void add(IBranch branch);
	//增加树叶节点
	public void add(ILeaf leaf);
	//遍历手下人员的信息
	public ArrayList getSubordinateInfo();
	
	
	
}

