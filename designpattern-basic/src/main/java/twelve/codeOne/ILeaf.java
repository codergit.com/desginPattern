package twelve.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：叶子节点的接口
 * 说明：
 */
public interface ILeaf {
	//获得自己的信息
	public String getInfo();
}
