package twelve.codeOne;

import java.util.ArrayList;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：根节点实现类
 * 说明：通过构造函数传递参数，可以增加子树节点，好叶子节点。
 */
public class Root  implements IRoot{
	//保存根节点下的树枝节点和树叶节点
	private ArrayList suybordinateList = new ArrayList();
	private String name = "";    //根节点的名称
	private String position = "";  //职位
	private int salary = 0;        //薪水

	public Root( String name, String position,
			int salary) {
		this.name = name;
		this.position = position;
		this.salary = salary;
	}

	public String getInfo() {
		String info = "";
		info = "名称"+ this.name;
		info = info + "\t职位 " + this.position;
		info = info + "\t薪水 " + this.salary;
		return info;
	
	}
	//增加树枝节点
	public void add(IBranch branch) {
		this.suybordinateList.add(branch);
	}
	//增加叶子节点
	public void add(ILeaf leaf) {
		this.suybordinateList.add(leaf);
	}
	
	//得到下级信息
	public ArrayList getSubordinateInfo() {
		return this.suybordinateList;
	}

}
