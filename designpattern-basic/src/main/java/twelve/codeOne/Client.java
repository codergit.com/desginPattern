package twelve.codeOne;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：场景类
 * 说明：这是最直接的描述部门之间的关系
 * 但是还有缺陷和可以重构的地方。
 */
public class Client {
	public static void main(String[] args) {
		//产生一个根节点
		IRoot ceo = new Root("王大麻子","总经理",100000);
		//产生三个部门经理，也就是树枝节点
		IBranch developDep = new Branch("马二拐子","研发部经理",20000);
		IBranch salesDep = new Branch("赵三驼子","销售部经理",30000);
		IBranch financeDep = new Branch("溜达瘸子","财务部经理",5000);
		//产生三个小组
		IBranch firstDevGroup  = new Branch("张三","研发一组组长",5000);
		IBranch secondDevGroup  = new Branch("李斯","研发二组组长",5000);
		//小兵们
		ILeaf a = new Leaf("a","开发人员",2000);
		ILeaf b = new Leaf("b","开发人员",2000);
		ILeaf c = new Leaf("c","开发人员",2000);
		ILeaf d = new Leaf("d","开发人员",2000);
		ILeaf e = new Leaf("e","开发人员",2000);
		ILeaf f = new Leaf("f","开发人员",2000);
		ILeaf g = new Leaf("g","开发人员",2000);
		ILeaf h = new Leaf("h","开发人员",2000);
		ILeaf j = new Leaf("j","开发人员",2000);
		ILeaf k = new Leaf("k","秘书",6000);
		ILeaf zhenglaoliu = new Leaf("郑老六","研发部副总",20000);
		//组装整个人员系统,ceo管辖
		ceo.add(salesDep);
		ceo.add(developDep);
		ceo.add(financeDep);
		ceo.add(k);
		
		//定义研发部门下的结构
		developDep.add(firstDevGroup);
		developDep.add(secondDevGroup);
		developDep.add(zhenglaoliu);
		//开发小组
		firstDevGroup.add(a);
		firstDevGroup.add(b);
		firstDevGroup.add(c);
		secondDevGroup.add(d);
		secondDevGroup.add(e);
		secondDevGroup.add(f);
		salesDep.add(g);
		salesDep.add(h);
		salesDep.add(j);
		//打印写完的树状结构
		System.out.println(ceo.getInfo());
		getAllSubordinateInfo( ceo.getSubordinateInfo());
		
	}
	
	public static void getAllSubordinateInfo(ArrayList subList){
		int length = subList.size();
		for (int i = 0; i < length; i++) {
		  Object obj = subList.get(i);
		  if(obj instanceof Leaf){
			  ILeaf  leaf = (Leaf)obj;
			  System.out.println(leaf.getInfo());
		  }else{
			  IBranch branch = (Branch) obj;
			  //递归调用
			  System.out.println(branch.getInfo());
			  getAllSubordinateInfo(branch.getSubordinateInfo());

		  }
		}
	}
}
