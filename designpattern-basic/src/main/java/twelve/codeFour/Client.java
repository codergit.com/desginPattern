package twelve.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：场景类
 * 说明：场景类负责树状结构的建立，并可以通过递归的方式遍历整个树
 * 注意：组合模式违反了依赖倒置原则，但是组合模式还有其他的变形
 */
public class Client {
	public static void main(String[] args) {
		//构件一个根节点
		Composites root = new Composites();
		root.doSomething();
		//构件一个树枝构件
		Composites branch = new Composites();
		//构件一个叶子节点
		Leaf leaf = new Leaf();
		//建立整体
		root.add(branch);
		root.add(leaf);
		
	}
	//通过递归遍历树
	public static void display(Composites root){
		for(Component c: root.getChildren()){
			if(c instanceof Leaf ){
				c.doSomething();
			}else{
				display((Composites)c);
			}
		}
	}
	
	
}
