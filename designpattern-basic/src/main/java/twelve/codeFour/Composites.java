package twelve.codeFour;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：组合模式的重点就在树枝构件
 * 说明：通用代码
 */
public class Composites extends Component{
	//构件容器
	private ArrayList<Component> compList = new ArrayList<Component>();
	//增加一个叶子构件或树枝构件
	public void add(Component component){
		this.compList.add(component);
	}
	//删除一个叶子构件或者树枝构件
	public void remove(Component component){
		this.compList.remove(component);
	}
	//获得分支下的所有叶子节点和树枝构件
	public ArrayList<Component> getChildren(){
		return this.compList;
	}
	
	
}
