package twelve.codeThree;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：场景类构建属性结构，并进行遍历，组装没有变化，
 * 说明：
 */
public class Client {
	public static String  getTreeInfo(Branch root){
		ArrayList<Corp> subList = root.getSubordinte();
		String info = "";
		for(Corp s : subList){
			if(s instanceof Leaf){ //是员工
				info = info + s.getInfo()+"\n";
			}else{   //是个小头目
				info = info + s.getInfo()+ "\n"+  getTreeInfo((Branch) s );
			}
			
		}
		return info;
		
	}
}
