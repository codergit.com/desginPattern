package twelve.codeThree;

import java.util.ArrayList;

public class Branch extends Corp{
	//领导下的领导和小兵
	public ArrayList<Corp> subList = new ArrayList<Corp>();
	//构造函数是必须的
	public Branch(String name, String position, int salary) {
		super(name, position, salary);
	}
	public void addSubordinate(Corp corp){
	    this.subList.add(corp);	
	}
	
	public ArrayList<Corp> getSubordinte(){
		return this.subList;
	}
	
	
	
	
	
}
