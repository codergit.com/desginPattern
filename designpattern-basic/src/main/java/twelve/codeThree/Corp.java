package twelve.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：抽象公司职员类
 * 说明：抽象出一些共性的东西
 */
public  abstract class Corp {
	private String name = "";      //职员名称
	private String position = "";  //职位
	private int salary = 0;        //薪水
	public Corp(String name, String position, int salary) {
		this.name = name;
		this.position = position;
		this.salary = salary;
	}
	//获取员工信息
	public String getInfo() {
		String info = "";
		info = "名称"+ this.name;
		info = info + "\t职位 " + this.position;
		info = info + "\t薪水 " + this.salary;
		return info;
	}



}
