package twelve.codeFive;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：该方法可能出现错误。
 * 说明：
 */
public class Leaf extends Component{
	@Deprecated
	@Override
	public void add(Component component)throws UnsupportedOperationException {
		//空实现
		throw new UnsupportedOperationException();
	}

	@Deprecated
	@Override
	public void remove(Component component) throws UnsupportedOperationException {
		//空实现
		throw new UnsupportedOperationException();
	}
	@Deprecated
	@Override
	public ArrayList<Component> getChildren() throws UnsupportedOperationException {
		//空实现
		throw new UnsupportedOperationException();
	}

}
