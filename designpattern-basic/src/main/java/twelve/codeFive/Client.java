package twelve.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：透明模式，遍历树形结构，不用进行强制类型转换
 * 说明：
 */
public class Client {
	 public static void main(String[] args) {
		
		
	}
	 //递归遍历
	 public static void display(Component root){
		 for(Component c: root.getChildren()){
			 if(c instanceof Leaf){
				 c.doSomething();
			 }else{
				 display(c);
			 }
		 }
	 }
}
