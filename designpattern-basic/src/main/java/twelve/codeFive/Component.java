package twelve.codeFive;

import java.util.ArrayList;

/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：透明模式的抽象构件
 * 说明：抽象构件定义了树枝节点和树叶节点，都必须实现的方法和属性，
 * 这样树枝节点的实现就不需要任何变化
 */
public abstract class Component {
	//个体和整体都有的共享
	public void doSomething(){
		//编写业务逻辑
	}
	//增加一个叶子构件或树枝构件
	public abstract void add(Component component);
	//删除一个叶子构件或树枝构件
	public abstract void remove(Component component);
	//获取分支下的所有叶子构件或者树枝构件
	public abstract ArrayList<Component> getChildren();
 
}

