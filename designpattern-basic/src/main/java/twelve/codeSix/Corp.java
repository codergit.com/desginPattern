package twelve.codeSix;
/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：组合模式  ---- 从下往上遍历由子类查询父类
 * 说明：抽象构件
 */
public class Corp {
	private String name = "";      //职员名称
	private String position = "";  //职位
	private int salary = 0;        //薪水
	private Corp parent = null;
	public Corp(String name, String position, int salary) {
		this.name = name;
		this.position = position;
		this.salary = salary;
	}
	//获取员工信息
	public String getInfo() {
		String info = "";
		info = "名称"+ this.name;
		info = info + "\t职位 " + this.position;
		info = info + "\t薪水 " + this.salary;
		return info;
	}
	
	protected void setParent(Corp _parent){
		this.parent = _parent;
	}
	//得到父节点
	public Corp getParent(){
		return this.parent;
	}
}
