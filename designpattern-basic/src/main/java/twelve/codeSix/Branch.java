package twelve.codeSix;

import java.util.ArrayList;


/**
 * 
 * @author fcs
 * @date 2014-8-19
 * 描述：树枝构件
 * 说明：组合模式的第二种方式遍历
 * 还可以实现同级之间的排序，排位功能。
 */
public class Branch extends Corp{
	//领导下的领导和小兵
	public ArrayList<Corp> subList = new ArrayList<Corp>();
	//构造函数是必须的
	public Branch(String name, String position, int salary) {
		super(name, position, salary);
	}
	public void addSubordinate(Corp corp){
		corp.setParent(this);    //设置父节点，
	    this.subList.add(corp);	
	}
	
	public ArrayList<Corp> getSubordinte(){
		return this.subList;
	}
}
