package one.codeFive;

import com.design.one.IGamePlayer;

/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：代理类的个性化
 * 说明：一个类可以实现多个接口，完成不同的任务的整合，代理不仅可以实现抽象主题接口
 * 也可以实现其他接口完成不同的任务。而且代理的目的是在目标对象方法的基础上做增强
 * 这种增强的本质通常是对目标对象的方法进行拦截和过滤。
 *
 */
public class GamePlayerProxy implements IGamePlayer,IProxy {
	private IGamePlayer gamePlayer = null;
	//通过构造函数传递要对谁进行代练
	public GamePlayerProxy(IGamePlayer _gamePlayer){
		this.gamePlayer = _gamePlayer;
	}
	//代练登录
	public void login(String user, String password) {
		this.gamePlayer.login(user, password);
	}
	//代练杀怪
	public void killBoss() {
		this.gamePlayer.killBoss();
	}
	//代练升级
	public void upGrade() {
		this.gamePlayer.upGrade();
		 this.count() ;
	}
	public void count() {
		System.out.println("升级总费用： 120元");
	}

}
