package one.codeFive;

import com.design.one.IGamePlayer;
import com.design.one.codeOne.GamePlayer;



public class Client {
	public static void main(String[] args) {
		//定义玩家
		IGamePlayer player = new GamePlayer("张三");
		//定义一个代练者
		IGamePlayer proxy= new GamePlayerProxy(player);

		System.out.println("开始时间是： 2014-7-12");
		proxy.login("zhangsan", "password");
		proxy.killBoss();
		proxy.upGrade();
	}
}
