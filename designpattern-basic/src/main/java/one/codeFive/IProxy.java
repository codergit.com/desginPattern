package one.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：代理类的接口类
 *
 */
public interface IProxy {
	//计算费用
	public void count();
}
