package one.codeSix;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import com.design.one.IGamePlayer;
import com.design.one.codeOne.GamePlayer;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：动态代理的场景类
 *
 */
public class Client {
	public static void main(String[] args) {
		// 定义一个代练者
		IGamePlayer player = new GamePlayer("张三");
		
		InvocationHandler handler = new GamePlayIH(player);
		//获得类的class loader
		ClassLoader cl = player.getClass().getClassLoader();
		//动态产生一个代理者
		IGamePlayer proxy = (IGamePlayer)Proxy.newProxyInstance(cl, new Class[]{IGamePlayer.class}, handler);
		
		System.out.println("开始时间是： 2014-7-12");
		//登录
		proxy.login("zhangsan", "password");
		//杀怪
		proxy.killBoss();
		//升级
		proxy.upGrade();
		System.out.println("结束时间是： 2014-7-12");
	}
}
