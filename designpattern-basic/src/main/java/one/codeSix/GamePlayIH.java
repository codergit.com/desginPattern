package one.codeSix;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：动态代理类
 * 说明：
 *  动态代理是在实现阶段不用关心代理谁，二在运行阶段指定代理哪一个对象。
 *  相对来说自己写的代理类的方式就是静态代理。不过动态代理也是代理模式的核心
 * 条件：必须实现InvocationHandler接口由该接口中的Invoke方法执行实际的方法
 *
 */

public class GamePlayIH implements InvocationHandler {
	//被代理者
	Class cls = null;
	//被代理的实例
	Object obj = null;
	//我要代理谁
	public GamePlayIH(Object _obj){
		this.obj = _obj;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
	 *调用被代理的方法
	 *参数说明：
	 *proxy : 代表动态代理对象
	 *method: 代表正在执行的方法
	 *agrs: 代表调用目标方法时传入的实参
	 *
	 */
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
			Object	result = method.invoke(this.obj, args);
			return  result ;
			
		/*	if(method.getName().equalsIgnoreCase("login")){
				System.out.println("有人用我的账号登录");
			}
			return  result ;
			
			* 该段代码体现了AOP的编程思想，它没有使用什么新的技术，
			* 但是它对我们的设计编码有非常大的影响
			* 对于日志，事务，权限等都可以在系统设计阶段不用考虑。
			* 而在设计后通过AOP的方法切过去。
			*
			*
			*/
		}

}
