package one.codeThree;

import com.design.one.IGamePlayer;

/**
 * 
 * @author fcs
 * @date 2014-7-29 描述：普通代理的代理者
 * 
 */
public class GamePlayerProxy implements IGamePlayer {
	private IGamePlayer gamePlayer = null;

	public GamePlayerProxy(String name) {
		try {
			gamePlayer = new GamePlayer(this, name);
		} catch (Exception e) {
			// 异常处理
		}
	}

	// 代练登录
	public void login(String user, String password) {
		this.gamePlayer.login(user, password);
	}

	// 代练杀怪
	public void killBoss() {
		this.gamePlayer.killBoss();
	}

	// 代练升级
	public void upGrade() {
		this.gamePlayer.upGrade();
	}

}
