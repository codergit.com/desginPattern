package one.codeThree;

import com.design.one.IGamePlayer;

/**
 * 
 * @author fcs
 * @date 2014-7-29 描述：代理模式的扩展：普通代理模式
 *       说明：修改了构造函数，传递的是一个代理者名称，即可进行代理。调用者只知道代理存在就行，不用知道代理了谁。
 * 
 */
public class GamePlayer implements IGamePlayer {
	private String name = "";

	public GamePlayer(IGamePlayer _gamePlayer, String _name) throws Exception {
		if (_gamePlayer == null) {
			throw new Exception("不能创建真实角色");
		} else {
			this.name = _name;
		}

	}

	// 登录
	public void login(String user, String password) {
		System.out.println("登录名为： " + user + "的用户名： " + this.name + "登录成功！！1");

	}

	public void killBoss() {
		System.out.println(this.name + "在打怪！！");
	}

	public void upGrade() {
		System.out.println(this.name + " 又升了一级！！");
	}

}
