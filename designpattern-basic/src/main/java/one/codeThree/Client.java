package one.codeThree;

import com.design.one.IGamePlayer;

/**
 * 作者：fcs 
 * 描述：普通代理的场景 
 * 说明：在该模式下，调用者只知道代理而不用知道真实的角色是谁，
 * 屏蔽了真实角色的变更对高层模块的影响。只要你实现了接口所对应的方法，该模式非常适合对扩展性 要求较高的场合。
 * 在实际的项目中，一般都是通过约定来禁止new一个真实的角色，这是一个非常好的方案
 * 
 * 缺陷：普通代理模式具有约束问题： 解决：尽量通过团队内的编程规范约束，因为每个主题类是可被重用的和可维护的，使用技术约束的方式对
 * 系统维护室一种非常不利的因素。 时间：2014-7-29
 */
public class Client {
	public static void main(String[] args) {
		// 定义一个代练者
		IGamePlayer proxy = new GamePlayerProxy("张三");

		System.out.println("开始时间是： 2014-7-12");
		proxy.login("zhangsan", "password");
		proxy.killBoss();
		proxy.upGrade();
		System.out.println("结束时间是： 2014-7-12");
	}

}
