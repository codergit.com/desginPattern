package one.codeTwo;

import com.design.one.IGamePlayer;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：游戏代练者
 *
 */
public class GamePlayerProxy implements IGamePlayer{
	
	private IGamePlayer gamePlayer = null;
	//通过构造函数传递要对谁进行代练
	public GamePlayerProxy(IGamePlayer _gamePlayer){
		this.gamePlayer = _gamePlayer;
	}
	//代练登录
	public void login(String user, String password) {
		this.gamePlayer.login(user, password);
	}
	//代练杀怪
	public void killBoss() {
		this.gamePlayer.killBoss();
	}
	//代练升级
	public void upGrade() {
		this.gamePlayer.upGrade();
	}

}
