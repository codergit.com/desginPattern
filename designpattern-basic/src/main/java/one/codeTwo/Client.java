package one.codeTwo;

import com.design.one.IGamePlayer;
import com.design.one.codeOne.GamePlayer;

/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：对第一版的改进
 * 说明：增加一个代练者，通过代练者实现
 *
 */
public class Client {
	
	public static void main(String[] args) {
		//定义玩家
		IGamePlayer player = new GamePlayer("张三");
		//定义一个代练者
		IGamePlayer proxy= new GamePlayerProxy(player);

		System.out.println("开始时间是： 2014-7-12");
		proxy.login("zhangsan", "password");
		proxy.killBoss();
		proxy.upGrade();
		System.out.println("结束时间是： 2014-7-12");
	}
	
}
