package one.codeOne;

import com.design.one.IGamePlayer;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：游戏者（代理模式的第一个版本）
 *
 */
public class GamePlayer implements IGamePlayer{
	private String name = "";
	//构造方法传递名称
	public GamePlayer(String _name){
		this.name = _name;
	}
	//登录
	public void login(String user, String password) {
	   	System.out.println("登录名为： "+user+"的用户名： "+this.name+"登录成功！！1");
	
	}
	
	public void killBoss() {
		System.out.println(this.name+"在打怪！！");
	}

	public void upGrade() {
		System.out.println(this.name+" 又升了一级！！");
	}

}
