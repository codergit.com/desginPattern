package one.codeOne;

import com.design.one.IGamePlayer;

/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：场景类
 *
 */
public class Client {
	public static void main(String[] args) {
		//定义玩家
		IGamePlayer player = new GamePlayer("张三");
		System.out.println("开始时间是： 2014-7-12");
		player.login("zhangsan", "password");
		player.killBoss();
		player.upGrade();
		System.out.println("结束时间是： 2014-7-12");

	}
}
