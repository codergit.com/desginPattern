package one.codeFour;
/**
 * 作者：fcs
 * 描述：强制代理的场景类
 * 说明：强制代理:就是要从真实角色查找到代理角色，不允许直接访问真实角色。高层模块只要调用getProxy
 *      就可以访问真实角色的所有方法，不需要产生一个代理出来，代理的管理已经由
 *      真实的角色自己完成。
 * 时间：2014-7-29
 */
public class Client {
	public static void main(String[] args) {
		//定义玩家
		IGamePlayer player = new GamePlayer("张三");
		//定义一个代练者
		IGamePlayer proxy= player.getProxy();

		System.out.println("开始时间是： 2014-7-12");
		proxy.login("zhangsan", "password");
		proxy.killBoss();
		proxy.upGrade();
		System.out.println("结束时间是： 2014-7-12");
	}

}
