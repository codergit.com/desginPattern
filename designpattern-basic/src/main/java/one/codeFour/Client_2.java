package one.codeFour;
 
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：直接访问代理类
 * 说明：这个代理角色不是真实角色指定的对象，这个代理对象时自己new出来的。
 *      必须找到指定代理才行
 *
 */
public class Client_2 {
	public static void main(String[] args) {
		//定义玩家
		IGamePlayer player = new GamePlayer("张三");
		//定义一个代练者
		IGamePlayer proxy= new GamePlayerProxy(player);

		System.out.println("开始时间是： 2014-7-12");
		proxy.login("zhangsan", "password");
		proxy.killBoss();
		proxy.upGrade();
		System.out.println("结束时间是： 2014-7-12");
	}
}
