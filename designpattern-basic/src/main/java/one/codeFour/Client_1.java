package one.codeFour;

/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：直接访问真实角色场景类
 *
 */
public class Client_1 {
  public static void main(String[] args) {
	//定义玩家
		IGamePlayer player = new GamePlayer("张三");
		System.out.println("开始时间是： 2014-7-12");
		player.login("zhangsan", "password");
		player.killBoss();
		player.upGrade();
		System.out.println("结束时间是： 2014-7-12");
}
}
