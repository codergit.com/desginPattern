package one.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：强制代理的接口类
 *
 */
public interface IGamePlayer {
	//登录游戏
	public void login(String user, String password);
	//杀怪
	public void killBoss();
	//升级
	public void upGrade();
	//每个人都可以找一下自己的代理
	public IGamePlayer getProxy();
	
	
}
