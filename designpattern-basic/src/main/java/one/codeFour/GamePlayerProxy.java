package one.codeFour;



public class GamePlayerProxy implements IGamePlayer {
	private IGamePlayer gamePlayer = null;
	//构造函数传递用户名
	public GamePlayerProxy(IGamePlayer _gamePlayer) {
		this.gamePlayer =_gamePlayer;
	}

	// 代练登录
	public void login(String user, String password) {
		this.gamePlayer.login(user, password);
	}

	// 代练杀怪
	public void killBoss() {
		this.gamePlayer.killBoss();
	}

	// 代练升级
	public void upGrade() {
		this.gamePlayer.upGrade();
	}

	public IGamePlayer getProxy() {
		return this;
	}


}
