package one.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：重新定义了一个接口，多声明了一个得到代理的方法
 * 说明：增加了一个私有的方法，检测是否是自己指定的代理，是指定的代理则允许访问，否则不允许
 *
 */
public class GamePlayer  implements IGamePlayer{
	private String name = "";
	//我的代理是谁
	private IGamePlayer proxy = null;
	
	public GamePlayer(String _name){
		this.name = _name;
	}
	
	
	public void login(String user, String password) {
		if(this.isProxy() ){
		   	System.out.println("登录名为： "+user+"的用户名： "+this.name+"登录成功！！1");
		}else{
			System.out.println("清使用指定的代理访问");
		}
	}

	public void killBoss() {
		if(this.isProxy()){
			System.out.println(this.name+"在杀怪");
		}else{
			System.out.println("清使用指定的代理访问");

		}
	}

	public void upGrade() {
		if(this.isProxy()){
			System.out.println(this.name+"  又升了一级");
		}else{
			System.out.println("清使用指定的代理访问");
		}		
	}

	//找到自己的代理
	public IGamePlayer getProxy() {
		this.proxy = new GamePlayerProxy(this);
		return this.proxy;
	}
	//校验是否是代理访问
	private boolean isProxy(){
		if(this.proxy == null){
			return false;
		}else{
			return true;
		}
	}

}
