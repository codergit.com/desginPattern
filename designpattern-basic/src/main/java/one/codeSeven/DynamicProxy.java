package one.codeSeven;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：动态代理类，这里实现了一个简单的横切面编程
 * 说明：它是一个通用类，不具有业务意义。可以再次产生一个实现类，就有意义了
 *
 */
public class DynamicProxy<T> {
	@SuppressWarnings("unchecked")
	public static <T> T newProxyInstance(ClassLoader loader,Class<?> []interfaces,InvocationHandler handler){
		//寻找joinPoint连接点，AOP框架使用元数据定义
		if(true){
			//执行一个前置通知
			(new BeforeAdvice()).exec();
		}
		//执行目标，并返回结果
		return (T)Proxy.newProxyInstance(loader, interfaces, handler);
	}
}
