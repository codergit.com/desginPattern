package one.codeSeven;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：真实主题类
 *
 */
public class RealSubject implements Subject{
	//业务操作实现
	public void doSomething(String str) {
		System.out.println("do something ---->" +str);		
	}

}
