package one.codeSeven;

import java.lang.reflect.InvocationHandler;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：具体业务的动态代理类
 * 说明;拓展后，高层模块对代理的访问会更加简单
 *
 */
public class SubjectDynamicProxy extends DynamicProxy {
	public static <T> T newProxyInstance(Subject subject) {
		// 获得ClassLoader
		ClassLoader loader = subject.getClass().getClassLoader();
		// 获得接口数组
		Class<?>[] classes = subject.getClass().getInterfaces();
		// 获得handler
		InvocationHandler handler = new MyInvocationHandler(subject);

		return newProxyInstance(loader, classes, handler);

	}
}
