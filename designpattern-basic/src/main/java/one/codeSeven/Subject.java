package one.codeSeven;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：抽象主题
 *
 */
public interface Subject {
	//业务操作
	public void doSomething(String str);
	//可以有多个逻辑处理方法
}
