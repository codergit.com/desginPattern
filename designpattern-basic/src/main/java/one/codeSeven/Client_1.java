package one.codeSeven;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：场景类
 * 说明：该包下的代理是一个通用的代理框架，可以在此基础上拓展自己的AOP框架
 * 一个通用的代理只要有一个接口，一个实现类，就可以使用该代理。完成代理的所有功效。
 *
 */
public class Client_1 {
	public static void main(String[] args) {
		//定义一个主题
		Subject subject= new RealSubject();
		
		//定义主题的代理
		Subject proxy = SubjectDynamicProxy.newProxyInstance(subject);
		
		//代理的行为
		proxy.doSomething("终于结束了。。。");
	}
}
