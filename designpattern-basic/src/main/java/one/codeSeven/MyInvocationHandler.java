package one.codeSeven;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：动态代理的handler(处理)类
 * 说明：所有通过动态代理实现的方法都通过invoke方法调用。
 *
 */
public class MyInvocationHandler implements InvocationHandler{
	//被代理的对象
	private Object trget = null;
	//通过构造函数传递一个对象
	public MyInvocationHandler(Object object){
		this.trget  = object;
	}
	//代理方法
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		return method.invoke(this.trget, args);
	}

}
