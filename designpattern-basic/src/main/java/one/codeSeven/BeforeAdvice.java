package one.codeSeven;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：通知接口实现类
 *
 */
public class BeforeAdvice implements IAdvice {

	public void exec() {
		System.out.println("我是前置通知我被执行了");		
	}

}
