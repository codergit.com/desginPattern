package one.codeSeven;
/**
 * 
 * @author fcs
 * @date 2014-7-29
 * 描述：通知接口
 *
 */
public interface IAdvice {
	//通知只有一个方法，执行即可
	public void exec();
}
