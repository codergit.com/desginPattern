package eighttheen.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-27
 * 描述：场景类
 * 说明：
 */
public class Client {
    public static void main(String[] args) {
		//现代化的邮局
    	ModernPostOffice2 hellRoadPostOffice = new ModernPostOffice2();
    	//只要把信的内容和收信人的地址即可，邮局自动完成一系列的工作。
    	String address = "Happy road NO. 666 ,God Province ,Heaven";
    	String context = "Hello, it's me.....";
    	//发送
    	hellRoadPostOffice.sendLetter(context, address);
    
    
    }
}
