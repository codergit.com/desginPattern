package eighttheen.codeThree;

import com.design.eighttheen.codeOne.ILetterProcess;
import com.design.eighttheen.codeOne.LetterProcessImp;
/**
 * @author fcs
 * @date 2014-8-27
 * 描述：扩展后的现代化邮局
 * 说明：不改变子系统对外暴漏的接口，方法，只改变内部的处理逻辑，
 * 其他兄弟模块的调用产生了不同的结果，这就是门面模式
 */
public class ModernPostOffice2 {
	 private ILetterProcess  letterProcess = new LetterProcessImp();
	 private Police letterPolice = new Police();
	 //写信封装投递，一体化
     public void sendLetter(String context,String address){
    	 letterProcess.writeContext(context);
    	 letterProcess.fillEnvelope(address);
    	 //警察检查信件
    	 letterPolice.checkLetter(letterProcess);
    	 letterProcess.letterIntotoEnvelope();
         letterProcess.sendLetter();
     }
}
