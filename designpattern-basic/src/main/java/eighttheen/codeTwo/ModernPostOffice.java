package eighttheen.codeTwo;

import com.design.eighttheen.codeOne.ILetterProcess;
import com.design.eighttheen.codeOne.LetterProcessImp;
/**
 * 
 * @author fcs
 * @date 2014-8-27
 * 描述：负责对一个比较复杂的信件处理过程的封装，然后高层模块只要和它有交互就行。
 * 说明：
 */
public class ModernPostOffice {
     private ILetterProcess  letterProcess = new LetterProcessImp();
     //写信封装投递，一体化
     public void sendLetter(String context,String address){
    	 letterProcess.writeContext(context);
    	 letterProcess.fillEnvelope(address);
    	 letterProcess.letterIntotoEnvelope();
         letterProcess.sendLetter();
     }
}
