package eighttheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：封装类
 * 说明：产生一个业务规则的方法，并且其作用域是在子系统中，
 */
public class Context {
   //委托处理
	private ClassA a = new ClassA();
	private ClassB b = new ClassB();
	public void complexMethod(){
		this.a.doSomethingA();
		this.b.doSomething();
	}
}
