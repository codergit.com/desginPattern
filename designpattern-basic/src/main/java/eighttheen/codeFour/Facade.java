package eighttheen.codeFour;

public class Facade {
    //被委托的对象
	public ClassA a = new ClassA();
	public ClassB b = new ClassB();
	public ClassC c = new ClassC();
	//提供给外部的访问方法
	public void methodA(){
		this.a.doSomethingA();
	}
	public void methodB(){
		this.b.doSomething();
	}
	public void methodC(){
		//在业务C的方法里调用业务 B的方法
		//这在设计中非常糟糕的事。
		//违反了单一职责原则，破坏了系统的封装，应该建立一个封装类
		//再将该对象传给门面类
		//this.b.doSomething();
		this.c.doSomething();
	}
	
	
}
