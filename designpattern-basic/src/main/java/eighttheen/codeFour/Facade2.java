package eighttheen.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：增加门面
 * 说明：委托给已经存在的门面对象，处理，避免修改代码，
 * 保持相同的代码只写一遍的规则。
 */
public class Facade2 {
     private Facade facade = new Facade();
     public void getMethod(){
    	 facade.a.doSomethingA();
     }

}
