package eighttheen.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-27
 * 描述：写信的过程接口
 * 说明：
 */
public interface ILetterProcess {
	//写内容
	public void writeContext(String context);
	//写信封
	public void fillEnvelope(String address);
	//放入袋中
	public  void letterIntotoEnvelope();
	//邮递
	public void sendLetter();
}
