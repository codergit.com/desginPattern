package twenty.codeTwo;

/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：改进后的场景类
 * 说明：改进后的场景类违反了迪米特法则
 * 将备忘录类，封装成一个管理类，管理这个备忘录。
 */
public class Client {
   public static void main(String[] args) {
	 //声明猪脚
   	Boy boy = new Boy();
   	//初始化当前状态
   	boy.setState("心情很好。。。");
   	System.out.println("=====男孩现在的状态=====");
   	System.out.println(boy.getState());
   	//记录下当前的状态
   	Memento   mem = boy.createMeemento();
   	//男孩去追女孩，状态改变
   	
   	boy.changeState();
   	System.out.println("\n男孩追女孩后的状态");
   	System.out.println(boy.getState());
   	//追女孩失败，恢复原状
   	boy.restoreMemento(mem);
   	System.out.println("\n=====男孩恢复后的状态。。。。");
        System.out.println(boy.getState());	
   	
   }
}
