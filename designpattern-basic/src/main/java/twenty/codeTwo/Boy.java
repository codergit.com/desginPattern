package twenty.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：改进后的封装Boy类
 * 说明：
 */
public class Boy {
	    private  String state = "";
	    //改变状态
	    public void changeState(){
	    	 this.state = "心情可能不是很好。。。。";
	    }
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}  
		//保留一个备份
		public Memento createMeemento(){
			return new Memento(this.state);
		}
		//恢复一个备份
		public void restoreMemento(Memento _memento){
			this.setState(_memento.getState());
		}
		
}
