package twenty.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：场景类
 * 说明：这只是简单的备忘录模式
 */
public class Client {
     public static void main(String[] args) {
		
    	 //定义发起人
    	 Originator originator = new Originator();
		 //定义出备忘录管理员
    	 Caretaker caretaker = new Caretaker();
		 //创建一个备忘录
    	 caretaker.setMemento(originator.createMento());
		 //恢复一个备忘录
    	 originator.restoreMemento(caretaker.getMemento());
    	 
		
		
		
		
	}
}
