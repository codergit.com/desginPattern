package twenty.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：备忘录通用类图源码
 * 说明：发起人角色
 */
public class Originator {
     private String state = "";

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	//创建一个备忘录
	public Memento createMento(){
		 return new Memento(this.state);
	}
	//恢复一个备忘录
	public void restoreMemento(Memento _Memento){
		this.setState(_Memento.getState());
	}
    	 
}
