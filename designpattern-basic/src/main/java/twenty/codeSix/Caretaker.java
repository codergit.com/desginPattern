package twenty.codeSix;


/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：备忘录管理者
 * 说明：就是增加一个了备忘录的javaBean
 */
public class Caretaker {
    private Memento  memento;

	public Memento getMemento() {
		return memento;
	}
	public void setMemento(Memento memento) {
		this.memento = memento;
	}
}
