package twenty.codeSix;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：备忘录角色
 * 说明：
 */
public class Memento {
   //接收HashMap作为状态
	private HashMap<String,Object>  stateMap;
	//接受一个对象，建立一个备份
	public Memento(HashMap<String,Object> map){
		this.stateMap = map;
	}
	public HashMap<String, Object> getStateMap() {
		return stateMap;
	}
	public void setStateMap(HashMap<String, Object> stateMap) {
		this.stateMap = stateMap;
	}
	
}
