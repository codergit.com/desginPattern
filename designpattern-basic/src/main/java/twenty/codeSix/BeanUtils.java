package twenty.codeSix;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：BeanUtils工具类
 * 说明：该类在项目中 经常用到，也可以使用工具继承
 */
public class BeanUtils {
    //把Bean的所有属性及数值放入到HashMap中。
	public static HashMap<String,Object>  backupProp(Object bean){	
		HashMap<String,Object> result = new HashMap<String,Object>();
		try {
			//获得Bean描述
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
		    //获得属性描述
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
		    //遍历所有属性
			for(PropertyDescriptor  des: descriptors){
				//属性名称
				String fieldName = des.getName();
				//读取属性的方法
				Method  getter = des.getReadMethod();
				//读取属性
					Object fieldValue = getter.invoke(bean, new Object[]{});
				if(! fieldName.equalsIgnoreCase("class")){
					result.put(fieldName, fieldValue);
					
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	//把HashMap的值返回到Bean中
	public static void restoreProp(Object bean,HashMap<String,Object> propMap){
		   try {
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
			PropertyDescriptor [] descriptor = beanInfo.getPropertyDescriptors();
			//遍历所有属性
			for(PropertyDescriptor des: descriptor){
				//属性名称
				String fieldName = des.getName();
				//如果有这个属性
				if(propMap.containsKey(fieldName)){
					//写属性的方法
					Method setter = des.getWriteMethod();
					setter.invoke(bean ,new Object[]{propMap.get(fieldName)});
					
				}
			}		   
		   } catch (Exception e) {
			e.printStackTrace();
		    System.out.println("shit......");
		   
		   }
		 
	}
	
	
	
}
