package twenty.codeEight;
/**
 * 
 * @author fcs
 * @date 2014-8-30
 * 描述：备忘录管理者
 * 说明：使用双接口设计，一个是业务的正常接口，实现必要的业务逻辑，叫做宽
 * 接口，另外一个接口是空接口，什么方法都没有，其目的是提供给子系统外的模块访问，比如容器都系，这个
 * 叫窄接口，由于窄接口中没有提供任何操纵数据的方法，因此相对来说比较安全。
 */
public class Caretaker {
     //备忘录对象
	private IMemento memento;

	public IMemento getMemento() {
		return memento;
	}
	public void setMemento(IMemento memento) {
		this.memento = memento;
	}
}
