package twenty.codeSeven;

import java.util.HashMap;

import com.design.twenty.codeSix.Memento;


/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：多备份的备忘录管理者
 * 说明：把容纳备忘录的容器修改为Map类型即可。
 */
public class Caretaker {
    //容纳备忘录的容器
	private HashMap<String,Memento> memMap = new HashMap<String,Memento>();
	
	public Memento getMemento(String idx) {
		return memMap.get(idx);
	}
	public void setMemento(String idx,Memento memento) {
        this.memMap.put(idx,memento);
	
	}
}
