package twenty.codeSeven;

import com.design.twenty.codeSix.Originator;
/**\
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：多备份下的备忘录模式
 * 说明：内存溢出问题，该备份一旦装入内存，没有任何销毁的意向，这是非常危险的，
 * 因此在系统设计时，要严格限定备忘录的创建，建议增加map的上限，否则系统很容易产生内存溢出情况。
 * 
 */
public class Client {
    public static void main(String[] args) {
		//定义发起人
    	Originator ori = new Originator();
	    //定义出备忘录管理员
    	Caretaker  caretaker = new Caretaker();
    	//创建两个备忘录
    	caretaker.setMemento("001", ori.createMento());
        caretaker.setMemento("002", ori.createMento());
        //恢复一个指定标记的备忘录
       ori.restoreMemento(caretaker.getMemento("001"));
       
    
    }
}
