package twenty.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：场景类
 * 说明：
 */
public class Client {
    public static void main(String[] args) {
	   //定义发起人
    	Originator originator = new Originator();
       //建立初试状态	   
    	originator.setState("初始状态。。。。。");
    	System.out.println("初试状态是： "+originator.getState());
        //建立备份
    	originator.createMento();
    	//修改状态
    	originator.setState("修改后的状态。。。。");
        System.out.println("修改后的状态："+originator.getState());
    	//恢复原有状态
        originator.restoreMemento();
        System.out.println("恢复后状态： "+originator.getState());
    }
}
