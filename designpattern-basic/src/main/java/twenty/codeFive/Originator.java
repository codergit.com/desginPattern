package twenty.codeFive;

import com.design.twenty.codeFour.Memento;

/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：融合备忘录的发起人角色，发起人自主备份和恢复
 * 
 * 说明：增加了clone方法，产生了一个备份对象，需要使用的时候在还愿。
 */
public class Originator implements Cloneable{
	//内部状态
	   private String state = "";
       private Originator backUp;
		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}
		//创建一个备忘录
		public Memento createMento(){
			 return new Memento(this.state);
		}
		//恢复一个备忘录
		public void restoreMemento(){
			//在运行恢复前英国进行断言，放在空指针
			this.setState(this.backUp.getState());
		}
		
		//克隆当前对象
		@Override
		public Originator clone(){
			try {
				return (Originator) super.clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
}
