package twenty.codeFive;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：备忘录管理员角色
 * 说明：备忘录角色转换成了发起人角色
 */
public class Caretaker {
    //发起人角色
	private Originator originator;
	public Originator getOriginator() {
		return originator;
	}
	public void setOriginator(Originator originator) {
		this.originator = originator;
	}
	
}
