package twenty.codeOne;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：场景类
 * 说明boy类封装的不够好
 */
public class Client {
    public static void main(String[] args) {
		//声明猪脚
    	Boy boy = new Boy();
    	//初始化当前状态
    	boy.setState("心情很好。。。");
    	System.out.println("=====男孩现在的状态=====");
    	System.out.println(boy.getState());
    	Boy backUp = new Boy();
    	backUp.setState(boy.getState());
    	boy.changeState();
    	System.out.println("\n男孩追女孩后的状态");
    	System.out.println(boy.getState());
    	//追女孩失败，恢复原状
    	boy.setState(backUp.getState());
    	System.out.println("\n=====男孩恢复后的状态。。。。");
         System.out.println(boy.getState());	
    	
    	
	}
}
