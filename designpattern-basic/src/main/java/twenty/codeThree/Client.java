package twenty.codeThree;

import com.design.twenty.codeTwo.Boy;
import com.design.twenty.codeTwo.Memento;
/**
 * 
 * @author fcs
 * @date 2014-8-29
 * 描述：改进后的场景类
 * 说明：这个就是备忘录模式，需要创建备忘录的时候，就创建一个备忘录，
 * 然后丢给备忘录管理者进行管理
 */
public class Client {
	 public static void main(String[] args) {
		 //声明猪脚
	   	Boy boy = new Boy();
	   	//声明出备忘录的管理者
	   	Caretaker  caretaker = new Caretaker();
	   	
	   	//初始化当前状态
	   	boy.setState("心情很好。。。");
	   	System.out.println("=====男孩现在的状态=====");
	   	System.out.println(boy.getState());
	   	//记录下当前的状态
	    caretaker.setMemento( boy.createMeemento());
	   	//男孩去追女孩，状态改变
	   	
	   	boy.changeState();
	   	System.out.println("\n男孩追女孩后的状态");
	   	System.out.println(boy.getState());
	   	//追女孩失败，恢复原状
	    boy.restoreMemento(caretaker.getMemento());
	   	System.out.println("\n=====男孩恢复后的状态。。。。");
	        System.out.println(boy.getState());	
	   	
	   }
}
