package three;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：每个抽象类都有两个实现类，分别实现公共点的最细节，最具体的事物。
 * 具体实现肤色语言性别的定义
 *
 */
public class MaleBlackMan extends AbstractYellowMan{

	public void getSex() {
	   System.out.println("黑色男性");	
	}
	
}
