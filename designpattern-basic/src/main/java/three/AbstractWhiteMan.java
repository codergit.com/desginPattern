package three;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：白色人种
 * 说明：负责人种的抽象属性定义。
 *
 */
public abstract class AbstractWhiteMan implements Human {

	public void getColor() {
		System.out.println("白色人种的皮肤颜色是白色的");		
	}

	public void talk() {
		System.out.println("白色人种会说话，一般都是单字节");
	}

	
}
