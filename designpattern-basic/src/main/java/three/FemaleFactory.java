package three;

/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：生产女性的八卦炉
 *
 */
public class FemaleFactory implements HuamnFactory{

	public Human createYellowHuamn() {
		return new FemaleYellowWoman();
	}

	public Human createWhiteHuman() {
		return new FemaleWhiteWoman();
	}

	public Human createBlackHuman() {
		return new FemaleBlackWoman();
	}

}
