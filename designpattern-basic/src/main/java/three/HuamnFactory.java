package three;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * 描述：制造人类的抽象接口
 *
 */
public interface HuamnFactory {
	//制造一个黄色人种
	public Human createYellowHuamn();
	//制造一个白色人种
	public Human createWhiteHuman();
	//制造一个黑色人种
	public Human createBlackHuman();
	
}
