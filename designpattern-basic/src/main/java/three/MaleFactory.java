package three;
/**
 * 
 * @author fcs
 * @date 2014-7-30
 * �������������Եİ���¯
 *
 */
public class MaleFactory implements HuamnFactory{

	public Human createYellowHuamn() {
		return new MaleYellowMan();
	}

	public Human createWhiteHuman() {
		return new MaleWhiteMan();
	}

	public Human createBlackHuman() {
		return new MaleBlackMan();
	}

}
