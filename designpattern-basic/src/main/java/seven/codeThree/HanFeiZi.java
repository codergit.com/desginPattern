package seven.codeThree;

import java.util.ArrayList;
import java.util.Iterator;

import com.design.seven.codeONe.IHanFeiZi;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：被观察者实现类
 * 说明：实现了两个接口，一个是通用的观察者接口，一个是有自己特定活动的观察者
 */
public class HanFeiZi implements Observable ,IHanFeiZi{
	//定义变长数组，存放所有被观察者
	private ArrayList<Observer> observerList = new ArrayList<Observer>();
	//吃饭
	public void haveBreakfast() {
		System.out.println("韩非子吃饭了。。。");
		this.notifyObserver("韩非子在吃饭");
	
	}

	//娱乐
	public void haveFun() {
		System.out.println("韩非子在娱乐。。。");
		this.notifyObserver("韩非子在娱乐");
	}
	
	//增加观察者
	public void addObserver(Observer observer) {
		this.observerList.add(observer);
	}
	//删除观察者
	public void deleteObserver(Observer observer) {
		this.observerList.remove(observer);
	}

	//通知所有观察者
	public void notifyObserver(String context) {
		for(Observer observer:observerList){
			observer.update(context);
		}
	}

	

}
