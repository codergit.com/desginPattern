package seven.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：另外一位观察者，
 * 说明：实现抽象观察者接口
 */
public class LiuSi implements Observer{

	public void update(String context) {
		System.out.println("刘思：观察到韩非子活动，开始动作。。。。");
		this.happy(context);
		System.out.println("刘思：乐死了。。\n");
	}
	private void happy(String context){
		System.out.println("刘思：因为"+context+"---变得快乐。。。。");
	}

}
