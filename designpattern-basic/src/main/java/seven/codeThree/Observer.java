package seven.codeThree;

/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：观察者接口
 * 说明：
 */
public interface Observer {
	
	public void update(String context);
}
