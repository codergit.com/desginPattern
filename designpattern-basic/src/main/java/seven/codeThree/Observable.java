package seven.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：被观察者接口
 * 说明：这是一个通用的被观察者接口，所有的被观察者都可以实现这个接口
 */
public interface Observable {
	//增加一个观察者
	public void addObserver(Observer observer);
	//删除一个观察者
	public  void deleteObserver(Observer observer);
	//通知观察者，被观察者有什么活动。
	public void notifyObserver(String context);
}
