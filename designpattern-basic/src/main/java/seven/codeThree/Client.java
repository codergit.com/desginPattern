package seven.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：场景类
 * 说明：结果正确，符合开闭原则，实现了类间的解耦，
 * 如果在想拓展观察者的话，继续实现Observer接口即可，要修改一下场景类了。
 */
public class Client {
	public static void main(String[] args) {
		Observer lisi = new LiSi();
		Observer wangsi = new WangSi();
		Observer liuSi  = new LiuSi();
		//定义韩非子
		HanFeiZi han = new HanFeiZi();
		//加入观察者
		han.addObserver(liuSi);
		han.addObserver(wangsi);
		han.addObserver(lisi);
		//韩非子的活动。。。
		han.haveBreakfast();
		
		
		
	}
}
