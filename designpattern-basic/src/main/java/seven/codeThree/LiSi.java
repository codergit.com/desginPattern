package seven.codeThree;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：具体的观察者
 * 说明：实现抽象的观察者接口
 */
public class LiSi implements Observer{

	public void update(String context) {
		System.out.println("观察到韩非子有活动，开始向老板汇报。。。。");		
		this.reportToQinShiHuang(context);
	
	}
	//汇报秦始皇
	public void reportToQinShiHuang(String reportContext){
		System.out.println("李斯报告：秦老板，韩非子有活动了。。"+reportContext);
	}

}
