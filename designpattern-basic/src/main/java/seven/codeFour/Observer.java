package seven.codeFour;
/**]
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：抽象观察者
 * 说明一般是一个接口，每一个实现该接口的实现类都是具体的观察者。
 */
public interface Observer {
	//更新方法
	public void update();
}
