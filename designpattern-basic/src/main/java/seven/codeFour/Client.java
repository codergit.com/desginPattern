package seven.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：场景类
 * 说明：
 */
public class Client {
	public static void main(String[] args) {
		//创建一个被观察者
		ConcreteSubject subject = new ConcreteSubject();
		//定义一个一个观察者
		Observer obs = new ConcreteObserver();
		//观察者观察被观察者
		subject.addObserver(obs);
		//观察者活动了，。
		subject.doSomething();
	}
}
