package seven.codeFour;

import java.util.Vector;

/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：被观察者
 * 说明：观察者模式的通用代码.
 * 职责是定义谁能够观察，谁不能够观察。
 * ArrayList是线程异步，不安全，
 * Vector是线程同步，安全
 */
public abstract class Subject {
	//定义一个观察者数组
	private Vector<Observer> observerVector = new Vector<Observer>();
	//增加一个观察者
	public void addObserver(Observer o){
		this.observerVector.add(o);
	}
	//删除一个观察者
	public void deleteObserver(Observer observer){
		this.observerVector.remove(observer);
		
	}
	//通知所有观察者
	public void notifyObservers(){
		for(Observer observer:observerVector){
			observer.update();
		}
	}
}
