package seven.codeFour;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：具体的观察者
 * 说明：
 */
public class ConcreteObserver implements Observer{

	public void update() {
		System.out.println("接收消息，并进行处理。。。。");
	}

}
