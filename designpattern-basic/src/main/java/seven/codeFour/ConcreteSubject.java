package seven.codeFour;

import java.util.Vector;

/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：具体的被观察者
 * 说明：观察者的通用代码，这是一个纯净的观察者，在具体的项目中该类有狠多的
 * 变种。
 */
public class ConcreteSubject  extends Subject{
	public void doSomething(){
		/**
		 * do something 
		 */
		super.notifyObservers();
	}
	
}
