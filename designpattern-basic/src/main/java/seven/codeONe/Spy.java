package seven.codeONe;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：间谍类
 * 说明：继承Thread类，并且无限循环监视活动，
 * 但是这种方式会损耗CPU和内存资源。真实的项目中不允许有while（true）,否则极易出现zh
 * 种问题。
 */
public class Spy extends Thread{
	private HanFeiZi hanFeizi;
	private LiSi      liSi;
	private String type;

	public Spy(HanFeiZi hanFeizi, LiSi liSi, String type) {
		super();
		this.hanFeizi = hanFeizi;
		this.liSi = liSi;
		this.type = type;
	}	
	//无限循环进行监控
	public void run(){
		while(true){
			if(this.type.equals("breakfast")){
				if(this.hanFeizi.isHavingBreakfast()){
					this.liSi.update("韩非子在吃饭。。");
					//重置状态继续监视
					this.hanFeizi.setHavingBreakfast(false);
				}
			}else{
				if(this.hanFeizi.isHavingFun()){
					this.liSi.update("韩非子在娱乐。");
					this.hanFeizi.setHavingFun(false);
				}
			}
		}
	}
}
