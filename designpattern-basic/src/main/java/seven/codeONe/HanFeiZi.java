package seven.codeONe;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：具体被观察者
 * 说明：韩非子活动
 */
public class HanFeiZi implements IHanFeiZi{
	private boolean isHavingBreakfast = false;
	private boolean isHavingFun = false;
	public void haveBreakfast() {
		System.out.println("韩非子：开始吃饭了。。。。");
		this.isHavingBreakfast = true;
	}

	public void haveFun() {
		System.out.println("韩非子：开始娱乐了。。。。。");
		this.isHavingFun = true;
	}

	public boolean isHavingBreakfast() {
		return isHavingBreakfast;
	}

	public void setHavingBreakfast(boolean isHavingBreakfast) {
		this.isHavingBreakfast = isHavingBreakfast;
	}

	public boolean isHavingFun() {
		return isHavingFun;
	}

	public void setHavingFun(boolean isHavingFun) {
		this.isHavingFun = isHavingFun;
	}
	

}
