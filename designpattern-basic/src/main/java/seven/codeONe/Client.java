package seven.codeONe;
//场景类
public class Client {
	public static void main(String[] args) {
		//定义李斯和韩非子
		LiSi liSi = new LiSi();
		HanFeiZi hanFeiZi = new HanFeiZi();
		Spy watchBreakfast = new Spy(hanFeiZi,liSi,"breakfast");
		watchBreakfast.start();
		
		Spy watchfun = new Spy(hanFeiZi,liSi,"fun");
		watchfun.start();
		try {
			Thread.sleep(1000);  //主线程等待一秒后再往下执行
			hanFeiZi.haveBreakfast();
			Thread.sleep(1000);
			hanFeiZi.haveFun();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}  
	
	
	}
}
