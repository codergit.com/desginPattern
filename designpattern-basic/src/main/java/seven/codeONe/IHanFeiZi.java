package seven.codeONe;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：被观察者接口
 * 说明：
 */
public interface IHanFeiZi {
	//吃饭
	public void haveBreakfast();
	//娱乐
	public void haveFun();
	
}
