package seven.codeTwo;

import com.design.seven.codeONe.IHanFeiZi;
import com.design.seven.codeONe.ILiSi;
import com.design.seven.codeONe.LiSi;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：通过聚集方式的观察者
 * 说明：在韩非子的类中加了LiSi类，相当于聚集
 * 在每个方法中调用LiSi.update()方法，完成李斯观察韩非子的职责。
 */
public class HanFeiZi implements IHanFeiZi{
	private ILiSi liSi = new LiSi();
	public void haveBreakfast() {
		System.out.println("韩非子： 开始吃饭了。。。");
		this.liSi.update("韩非子在吃饭。。。");
	}
	//娱乐
	public void haveFun() {
		System.out.println("韩非子：开始娱乐了。。。。");
		this.liSi.update("韩非子在娱乐");
	}

}
