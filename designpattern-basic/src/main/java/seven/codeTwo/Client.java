package seven.codeTwo;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：通过聚集方式的场景类
 * 说明：李斯没有出现在场景类中。但是实际情况不符合，因此要再进行修改
 * 因此要再次升级
 */
public class Client {
	public static void main(String[] args) {
		HanFeiZi hanFeiZi  = new HanFeiZi();
		hanFeiZi.haveBreakfast();
		hanFeiZi.haveFun();
	}
}
