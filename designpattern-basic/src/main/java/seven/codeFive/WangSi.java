package seven.codeFive;

import java.util.Observable;
import java.util.Observer;

/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：另外一位观察者，
 * 说明：实现抽象观察者接口
 */
public class WangSi implements Observer{

	//被观察者的活动后。。。
	private void cry(String context){
		System.out.println("王思：因为"+context+"---所以我悲伤啊。。。。");
	}
	public void update(Observable o, Object arg) {
		System.out.println("王思：观察到韩非子的活动，自己开始活动了。。。。");
		this.cry(arg.toString());
		System.out.println("王思哭死了\n");		
	}

}
