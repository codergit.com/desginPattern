package seven.codeFive;

import java.util.Observable;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：观察者
 * 
 * 说明：这是利用  Java的API中的观察者类Observerable和Observer接口实现观察者模式
 */
public class Observerable  extends  Observable{
	public void notifyObserver(String context){
		
		System.out.println(context);
	}
}
