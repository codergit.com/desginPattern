package seven.codeFive;

import java.util.Observable;
import java.util.Observer;

/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：具体的观察者
 * 说明：实现java 的API观察者接口  。
 */
public class LiSi implements Observer{

	
	//汇报秦始皇
	public void reportToQinShiHuang(String reportContext){
		System.out.println("李斯报告：秦老板，韩非子有活动了。。"+reportContext);
	}
	public void update(Observable o, Object arg) {
		System.out.println("观察到韩非子有活动，开始向老板汇报。。。。");		
		this.reportToQinShiHuang(arg.toString());
		System.out.println("李斯汇报完毕\n");
	}

}
