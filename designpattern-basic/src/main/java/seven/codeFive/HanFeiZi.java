package seven.codeFive;

import com.design.seven.codeONe.IHanFeiZi;
/**
 * 
 * @author fcs
 * @date 2014-8-11
 * 描述：被观察者实现类继承 Observerable实现自己的业务接口
 * 说明：有问题。。。。
 */
public class HanFeiZi extends Observerable implements IHanFeiZi{
	
	//吃饭
	public void haveBreakfast() {
		System.out.println("韩非子吃饭了。。。");
		//通知所有的观察者
		super.setChanged();
		super.notifyObservers("韩非子在吃饭"); 
	
	}

	//娱乐
	public void haveFun() {
		System.out.println("韩非子在娱乐。。。");
		super.setChanged();
		super.notifyObservers("韩非子在娱乐"); // 通知所有观察者
		super.notifyObserver("韩非子在娱乐");  //这里调用的是Observerable
		//自定义的观察者里的方法，而加了s的则是调用了JavaAPI中的方法。
	}



	

}
