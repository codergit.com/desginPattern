package ninetheen.codeOne;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：减法解释器
 * 说明：解析器的开发工作完成后，对解析器进行封装。，
 */
public class SubExpression extends SymbolExpression{

	public SubExpression(Expression _left, Expression _right) {
		super(_left, _right);
	}
	//左右两个表达式相减
	public int interpreter(HashMap<String ,Integer> var){
		return super.left.interpreter(var)- super.right.interpreter(var);
	}
}
