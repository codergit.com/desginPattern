package ninetheen.codeOne;

import java.util.HashMap;
import java.util.Stack;

/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：解析器封装类
 * 说明：
 */
public class Calculator {
   //定义表达式
	private Expression expression;
	public Calculator(String expStr){
		//定义一个栈，安排运算的先后顺序
		Stack<Expression> stack = new Stack<Expression>();
		//表达式拆分为字符数组
	    char [] charArray = expStr.toCharArray();
	    //运算
	    Expression left = null;
	    Expression right = null;
	    for(int i =0;i< charArray.length;i++){
	    	switch(charArray[i]){
	    	   case '+':
	    		   left = stack.pop();
	    		   right = new VarExpression(String.valueOf(charArray[i++]));
	    		   stack.push(new AddExpression(left, right));
	    		   break;
	    	   case '-':   
	    		   
	    		   left = stack.pop();
	    		   right = new VarExpression(String.valueOf(charArray[i++]));
	    		   stack.push(new AddExpression(left, right));
	    		   break;
	    		   //公式中的变量
	    		   default:
	    			   stack.push(new VarExpression(String.valueOf(charArray[i])));
	    		   
	    		   
	    		   
	    		   
	    	}
	    	//抛出运算结果。
	    	this.expression = stack.pop();
	    }

	}
	public int run(HashMap<String,Integer> var){
		return this.expression.interpreter(var);
	}
}
