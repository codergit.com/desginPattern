package ninetheen.codeOne;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：抽象表达式类
 * 说明：
 */
public  abstract class Expression {
	//解析公式和数值，其中var中的key值是公式中的参数，value值是具体的数值，
    public abstract int interpreter(HashMap<String,Integer> var);
}
