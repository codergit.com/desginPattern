package ninetheen.codeOne;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：抽象运算符号解析器
 * 说明：
 */
public class SymbolExpression extends Expression{
	protected Expression left;
	protected Expression right;
	//所有的公式解析都应该只关心自己左右两个表达式的结果。
	public SymbolExpression(Expression _left,Expression _right){
		this.left = _left;
		this.right = _right;
	}
	@Override
	public int interpreter(HashMap<String, Integer> var) {
		return 0;
	}

}
