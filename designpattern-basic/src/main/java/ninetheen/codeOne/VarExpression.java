package ninetheen.codeOne;

import java.util.HashMap;

/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：变量解析器
 * 说明：
 */
public class VarExpression extends Expression{
    private String key;
    public VarExpression(String _key){
    	this.key = _key;
    }
    //从map中取值
	@Override
	public int interpreter(HashMap<String, Integer> var) {
		return var.get(this.key);
	}

}
