package ninetheen.codeTwo;

import javax.naming.Context;

/**
 * 
 * @author fcs
 * @date 2014-8-28
 * 描述：抽象表达式
 * 说明：是生成语法集合的关键，通过递归调用进行解析
 */
public abstract class Expression {
      //每个表达式必须有一个解析任务
	public abstract Object interpreter(Context context);
}
